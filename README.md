# 介绍

​	基于spring Boot 和Vue3开发的平台，该仓库为平台后端部分。本平台采用市面上的主流技术，方面快速入手，平台功能齐全，编码规范，配合接口文档，方便二次开发，目前详细文档正在编写中。

# 特色功能

- 部门/岗位/角色/用户管理	
- 菜单/系统字典/用户字典/参数管理
- 精确到按钮的权限设置
- 丰富的日志采集系统
- 实时动态的缓存更新
- 接口次数计费，全局接口限流
- 自动化部署
- 统一文件存储
- 在线接口调试，实时接口文档
- 外部API统一管理，在线调试，实时切换数据源
- 提供后端代码生成器，一键生成后端基础代码
- 使用主流技术，二次开发方便

# 软件架构

| 名称            | 版本                 | 说明                  |
| --------------- | -------------------- | --------------------- |
| spring Boot     | 2.3.12.RELEASE       | 基础框架              |
| Spring Security | 2.3.12.RELEASE       | 权限框架              |
| MyBatis Plus    | 3.4.0                | 数据库映射框架        |
| Minio           | 2021-06-17T00-10-46Z | 分布式文件存储        |
| MySql           | 5.7.35               | 数据库                |
| Redis           | 6.2.6                | 缓存                  |
| canal           | 1.1.5                | 数据同步              |
| XXL-JOB         | 2.3.0                | 定时任务              |
| jjwt            | 0.9.1                | JWT创建和验证的Java库 |
| knife4j         | 2.0.9                | 接口文档              |
| hutool          | 5.7.19               | 工具集                |

# 功能截图
 **演示地址：http://123.249.2.1** 
 **账户密码：admin/3179927046、test/123456** 

**登录**
![输入图片说明](doc/jpg/image100.png)

**移动端登录**
登录页面已适配移动端
![输入图片说明](doc/jpg/image101.png)

**首页**
![输入图片说明](doc/jpg/p1.png)


**系统管理**

![系统管理](doc/jpg/image.png)

**用户管理**
![输入图片说明](doc/jpg/image103.png)

**接口配置**
![输入图片说明](doc/jpg/image2.png)

**接口配额**
![输入图片说明](doc/jpg/image6.png)

**修改示例**
![输入图片说明](doc/jpg/image4.png)

**分配菜单**
![输入图片说明](doc/jpg/image8.png)

**日志监控**
![输入图片说明](doc/jpg/image5.png)

**API管理**
![输入图片说明](doc/jpg/image7.png)

**接口文档**
![输入图片说明](doc/jpg/image104.png)
**ELK日志监控页面**
![输入图片说明](doc/jpg/elk1.png)
**ELK异常日志详情**
![输入图片说明](doc/jpg/elk2.png)

**在线消息提示**

![输入图片说明](doc/jpg/adadasd.png)
![输入图片说明](doc/jpg/SVLSQ0%251V4CWD~%7DILEXN5KS.png)



# 安装教程

- 前端仓库地址：https://gitee.com/T520/tao-development-platform-web
- 本仓库为后端
- 本项目可以采用jenkins + docker + nginx进行自动化部署
- 本项目克隆下来后，需要更改mysql，redis和canal地址及密码

## 开发环境
- IDEA
- JDK8
- MySql 5.7.35
- Redis 5.+
- maven 3.+

## 资源官网
- DockerHup:https://hub.docker.com
- redis:https://redis.io
- xxl-job:https://www.xuxueli.com/xxl-job
- canal:https://github.com/alibaba/canal
- MySql:https://www.mysql.com/cn
- minio: http://www.minio.org.cn
- Elasticsearch: https://www.elastic.co/cn/elasticsearch

## 普通安装

  本平台的第三方依赖有mysql(必须)、redis（必须）、minio(必须)、xxl-job（可选）、canal（可选），请自行查看官网，或者百度安装。

## docker方式安装

  后端仓库文档中有docker-compose文件，可以一键安装除canal外的全部服务
# 平台部分流程架构图
## 架构图
![输入图片说明](doc/jpg/%E6%9E%B6%E6%9E%84%E5%9B%BE2.jpg)

## 访问流程图
![输入图片说明](doc/jpg/%E6%B5%81%E7%A8%8B01.jpg)