FROM openjdk:8

#创建名录
RUN ["mkdir","-p","/app"]

#引用变量 ( ARG仅在构建 Docker镜像期间可用 )
ARG JAR_FILE

ENV TZ="Asia/Shanghai"
#设置时区为上海
RUN /bin/cp /usr/share/zoneinfo/${TZ} /etc/localtime && echo ${TZ} >/etc/timezone

WORKDIR /app

#指定需要暴露的端口
EXPOSE 9000

#预先设置日志挂载路径
VOLUME /app/logs

#复制jar包进容器
COPY target/${JAR_FILE} app.jar

#启动命令
ENTRYPOINT ["java","-jar","app.jar"]

#cmd参数，可以设置启动环境变量
CMD ["--spring.profiles.active=test"]