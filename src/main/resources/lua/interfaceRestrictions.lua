--[获得剩余次数]
local remainingNumber  = redis.call("ZSCORE",KEYS[1],KEYS[2])
--[如果没有这个key，就新建,返回-2]
if  not remainingNumber  then
    redis.call("ZADD",KEYS[1],0,KEYS[2])
    return -2
end
if tonumber(remainingNumber) >= 1 then
    local n =  redis.call("ZINCRBY",KEYS[1],-1,KEYS[2])
    return tonumber(n)
else
    return -1
end



