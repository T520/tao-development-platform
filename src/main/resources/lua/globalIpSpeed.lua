--[[局部变量]]
local value  = redis.call("GET",KEYS[1])
--[[如果key不存在]]
if not value then
--[[设置key,过期时间为1秒]]
    redis.call("SETEX",KEYS[1],'1','1')
    return 1
end
--[[如果key存在,就需要判断当前是否超过速率]]
if tonumber(value) > (tonumber(ARGV[1]) - 1) then
    return 0
else
--[[自增1]]
    redis.call("incr",KEYS[1])
    return 1
end