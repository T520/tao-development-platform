package com.tang.module.system.search;

import com.tang.module.system.entity.Dept;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;

/**
 * 部门表(Dept)实体搜索对象
 *
 * @author tang
 * @since 2022-02-16 15:18:45
 */
@ApiModel(value = "DeptSearch搜索对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class DeptSearch extends Dept {
    
}
