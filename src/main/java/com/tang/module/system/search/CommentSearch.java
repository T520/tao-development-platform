package com.tang.module.system.search;

import com.tang.module.system.entity.Comment;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;

/**
 * 评论表(Comment)实体搜索对象
 *
 * @author tang
 * @since 2022-03-20 21:34:00
 */
@ApiModel(value = "CommentSearch搜索对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class CommentSearch extends Comment {
    
}
