package com.tang.module.system.search;

import com.tang.module.system.entity.User;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;

/**
 * 用户表(User)实体搜索对象
 *
 * @author tang
 * @since 2022-02-16 15:18:45
 */
@ApiModel(value = "UserSearch搜索对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class UserSearch extends User {
    
}
