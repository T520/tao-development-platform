package com.tang.module.system.search;

import com.tang.module.system.entity.LogInterface;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;

/**
 * 接口日志表(LogInterface)实体搜索对象
 *
 * @author tang
 * @since 2022-02-16 15:18:45
 */
@ApiModel(value = "LogInterfaceSearch搜索对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class LogInterfaceSearch extends LogInterface {
    
}
