package com.tang.module.system.search;

import com.tang.module.system.entity.Post;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;

/**
 * 岗位表(Post)实体搜索对象
 *
 * @author tang
 * @since 2022-02-16 15:18:45
 */
@ApiModel(value = "PostSearch搜索对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class PostSearch extends Post {
    
}
