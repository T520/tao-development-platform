package com.tang.module.system.search;

import com.tang.module.system.entity.LogApi;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;

/**
 * 接口日志表(LogApi)实体搜索对象
 *
 * @author tang
 * @since 2022-02-16 15:18:45
 */
@ApiModel(value = "LogApiSearch搜索对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class LogApiSearch extends LogApi {
    
}
