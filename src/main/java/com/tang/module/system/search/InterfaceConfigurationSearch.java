package com.tang.module.system.search;

import com.tang.module.system.entity.InterfaceConfiguration;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;

/**
 * 业务接口配置表(InterfaceConfiguration)实体搜索对象
 *
 * @author tang
 * @since 2022-02-16 15:18:45
 */
@ApiModel(value = "InterfaceConfigurationSearch搜索对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class InterfaceConfigurationSearch extends InterfaceConfiguration {
    
}
