package com.tang.module.system.search;

import com.tang.module.system.entity.Dict;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;

/**
 * 字典表(Dict)实体搜索对象
 *
 * @author tang
 * @since 2022-02-16 15:18:45
 */
@ApiModel(value = "DictSearch搜索对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class DictSearch extends Dict {
    
}
