package com.tang.module.system.search;

import com.tang.module.system.entity.RoleScope;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;

/**
 * 角色数据权限关联表(RoleScope)实体搜索对象
 *
 * @author tang
 * @since 2022-02-16 15:18:45
 */
@ApiModel(value = "RoleScopeSearch搜索对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class RoleScopeSearch extends RoleScope {
    
}
