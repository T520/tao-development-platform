package com.tang.module.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import com.tang.module.system.entity.LogError;

/**
 * 错误日志表(LogError)数据库访问层
 *
 * @author tang
 * @since 2022-01-20 10:34:00
 */
@Repository
public interface LogErrorMapper extends BaseMapper<LogError> {

}
