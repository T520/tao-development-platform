package com.tang.module.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.tang.module.system.entity.InterfaceLimit;

import java.util.List;

/**
 * 接口访问次数
(InterfaceLimit)数据库访问层
 *
 * @author tang
 * @since 2022-01-21 16:28:34
 */
@Repository
public interface InterfaceLimitMapper extends BaseMapper<InterfaceLimit> {


    /**
     * 总访问次数自增1,剩余访问次数减少1
     *
     * @param uid           用户id
     * @param interfaceType 接口类型
     */
    void updateTimes(@Param("uid") Long uid, @Param("interfaceType") Integer interfaceType);

    /**
     * 获得全部没有初始化的用户和接口类型
     * @return 配置列表
     */
    List<InterfaceLimit> getNotInit();

    /**
     * 更新访问次数
     * @param userAccount 用户账户
     * @param number 次数
     * @param interfaceType 接口类型
     * @return
     */
    void addNumber(@Param("userAccount") String userAccount,@Param("number") Long number,@Param("interfaceType") String interfaceType);

}