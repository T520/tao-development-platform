package com.tang.module.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import com.tang.module.system.entity.RoleInterface;

/**
 * 角色数据权限关联表(RoleInterface)数据库访问层
 *
 * @author tang
 * @since 2022-03-18 12:11:56
 */
@Repository
public interface RoleInterfaceMapper extends BaseMapper<RoleInterface> {

}
