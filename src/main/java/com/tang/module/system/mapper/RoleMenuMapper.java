package com.tang.module.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import com.tang.module.system.entity.RoleMenu;

/**
 * 角色菜单关联表(RoleMenu)数据库访问层
 *
 * @author tang
 * @since 2022-01-20 17:29:39
 */
@Repository
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {

}
