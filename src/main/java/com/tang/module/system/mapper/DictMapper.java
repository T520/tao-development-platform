package com.tang.module.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import com.tang.module.system.entity.Dict;

/**
 * 字典表(Dict)数据库访问层
 *
 * @author tang
 * @since 2022-01-20 11:49:22
 */
@Repository
public interface DictMapper extends BaseMapper<Dict> {

}
