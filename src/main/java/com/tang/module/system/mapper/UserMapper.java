package com.tang.module.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tang.module.system.vo.UserVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;
import com.tang.module.system.entity.User;

/**
 * 用户表(User)数据库访问层
 *
 * @author tang
 * @since 2022-01-12 18:04:55
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

    /**
     * 通过用户ID获取用户名
     * @param id 用户id
     * @return 用户名
     */
    User byIdGetUserName(@Param("id") String id);


}
