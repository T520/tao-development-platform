package com.tang.module.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import com.tang.module.system.entity.Post;

/**
 * 岗位表(Post)数据库访问层
 *
 * @author tang
 * @since 2022-01-14 16:14:56
 */
@Repository
public interface PostMapper extends BaseMapper<Post> {

}
