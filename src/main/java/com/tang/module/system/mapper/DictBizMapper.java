package com.tang.module.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import com.tang.module.system.entity.DictBiz;

/**
 * 业务字典表(DictBiz)数据库访问层
 *
 * @author tang
 * @since 2022-01-20 11:49:23
 */
@Repository
public interface DictBizMapper extends BaseMapper<DictBiz> {

}
