package com.tang.module.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import com.tang.module.system.entity.Dept;

/**
 * 部门表(Dept)数据库访问层
 *
 * @author tang
 * @since 2022-01-14 16:14:56
 */
@Repository
public interface DeptMapper extends BaseMapper<Dept> {

}
