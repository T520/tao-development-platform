package com.tang.module.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import com.tang.module.system.entity.Comment;

/**
 * 评论表(Comment)数据库访问层
 *
 * @author tang
 * @since 2022-03-20 21:34:00
 */
@Repository
public interface CommentMapper extends BaseMapper<Comment> {

}
