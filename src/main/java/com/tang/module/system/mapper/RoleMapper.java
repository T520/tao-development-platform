package com.tang.module.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.tang.module.system.entity.Role;

import java.util.List;

/**
 * 角色表(Role)数据库访问层
 *
 * @author tang
 * @since 2022-01-13 10:31:42
 */
@Repository
public interface RoleMapper extends BaseMapper<Role> {

    /**
     * 根据接口ID查询出访问该接口需要的角色
     * @param interfaceId 接口ID
     * @return 角色集合
     */
    List<Role> byInterfaceId(@Param("interfaceId") Long interfaceId);


}
