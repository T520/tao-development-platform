package com.tang.module.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import com.tang.module.system.entity.InterfaceConfiguration;

import java.util.List;

/**
 * 业务接口配置表(InterfaceConfiguration)数据库访问层
 *
 * @author tang
 * @since 2022-01-21 12:59:21
 */
@Repository
public interface InterfaceConfigurationMapper extends BaseMapper<InterfaceConfiguration> {

    /**
     * 获取全部生效的接口配置和对应的角色
     * @return
     */
    List<InterfaceConfiguration> getAllOrRole();


}
