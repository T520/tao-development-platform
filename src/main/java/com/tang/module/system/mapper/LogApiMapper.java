package com.tang.module.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tang.common.resources.Kv;
import lombok.Data;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.tang.module.system.entity.LogApi;

import java.util.Date;
import java.util.List;

/**
 * 接口日志表(LogApi)数据库访问层
 *
 * @author tang
 * @since 2022-01-18 18:08:15
 */
@Repository
public interface LogApiMapper extends BaseMapper<LogApi> {


    /**
     * 按日统计日志
     * @param date 统计开始日期
     * @return
     */
    List<Kv<String,Integer>> byDay(@Param("date") Date date);

}
