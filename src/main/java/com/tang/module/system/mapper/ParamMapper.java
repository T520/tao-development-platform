package com.tang.module.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import com.tang.module.system.entity.Param;

/**
 * 参数表(Param)数据库访问层
 *
 * @author tang
 * @since 2022-01-13 12:47:13
 */
@Repository
public interface ParamMapper extends BaseMapper<Param> {

}
