package com.tang.module.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tang.common.resources.Kv;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.tang.module.system.entity.LogInterface;

import java.util.Date;
import java.util.List;

/**
 * 接口日志表(LogInterface)数据库访问层
 *
 * @author tang
 * @since 2022-01-21 14:04:09
 */
@Repository
public interface LogInterfaceMapper extends BaseMapper<LogInterface> {

    /**
     * 按日统计日志
     * @param date 统计开始日期
     * @return
     */
    List<Kv<String,Integer>> byDay(@Param("date") Date date);

}
