package com.tang.module.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.tang.module.system.entity.Menu;

import java.util.List;

/**
 * 菜单表(Menu)数据库访问层
 *
 * @author tang
 * @since 2022-01-20 17:29:39
 */
@Repository
public interface MenuMapper extends BaseMapper<Menu> {


    /**
     * 获取全部按钮
     * @return
     */
    List<Menu> buttonAll();

    /**
     * 获取角色拥有的按钮
     * @param roles 用户的角色ID集合
     * @return
     */
    List<Menu> buttonRole(@Param("roles") List<Long> roles);

    /**
     * 获得角色的全部菜单（包括按钮）
     * @param roleIds 角色id集合
     * @return
     */
    List<Menu> getRoleMenu(@Param("roleIds") List<Long> roleIds);

    /**
     * 获得角色的全部菜单（不包括按钮）
     * @param roleIds 角色id集合
     * @return
     */
    List<Menu> getRoleMenuNotButton(@Param("roleIds") List<Long> roleIds);

    /**
     * 获得三级菜单的顶父负菜单的名称
     * @param id 当三级菜单的id
     * @return 菜单名称
     */
    String getParentName(Long id);
}
