package com.tang.module.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tang.module.system.entity.InterfaceConfiguration;
import com.tang.module.system.entity.Role;

import java.util.List;

/**
 * 业务接口配置表(InterfaceConfiguration)表服务接口
 *
 * @author tang
 * @since 2022-01-21 12:59:21
 */
public interface InterfaceConfigurationService extends IService<InterfaceConfiguration> {

    /**
     * 获得接口所需角色
     * @param id 接口id
     * @return 角色列表
     */
    List<Role> getRole(Long id);

    /**
     * 更新接口对应的角色
     * @param id 接口ID
     * @param rid 角色ID集合
     * @return 是否成功
     */
    boolean updateRole(Long id,List<Long> rid);

    /**
     * 刷新缓存
     * @return 是否成功
     */
    boolean cacheRefresh();

}
