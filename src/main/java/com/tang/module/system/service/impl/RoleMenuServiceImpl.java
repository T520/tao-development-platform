package com.tang.module.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tang.module.system.mapper.RoleMenuMapper;
import com.tang.module.system.entity.RoleMenu;
import com.tang.module.system.service.RoleMenuService;
import org.springframework.stereotype.Service;

/**
 * 角色菜单关联表(RoleMenu)表服务实现类
 *
 * @author tang
 * @since 2022-01-20 17:29:39
 */
@Service("roleMenuService")
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements RoleMenuService {

}
