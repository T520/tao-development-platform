package com.tang.module.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tang.module.system.mapper.LogInterfaceMapper;
import com.tang.module.system.entity.LogInterface;
import com.tang.module.system.service.LogInterfaceService;
import org.springframework.stereotype.Service;

/**
 * 接口日志表(LogInterface)表服务实现类
 *
 * @author tang
 * @since 2022-01-21 14:04:09
 */
@Service("logInterfaceService")
public class LogInterfaceServiceImpl extends ServiceImpl<LogInterfaceMapper, LogInterface> implements LogInterfaceService {

}
