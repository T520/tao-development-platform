package com.tang.module.system.service;

import com.tang.module.system.entity.NameAndValue;

import java.util.LinkedList;
import java.util.Map;

/**
 * 统计业务层，收集各种数据进行分析
 * @author tang
 * @date 2022/3/14 16:34
 */
public interface StatisticsService {

    /**
     * 按天统计日志情况
     * 每个图表数据都是一个map,全部图表数据组成一个大map
     *
     * @return
     */
    Map<String,Map<String, Object>>  byDay();

    /**
     * 首页统计
     * @return 记录数目
     */
    Map<String, String> recordStatistics();

    /**
     * 访问来源统计
     * @return key：城市名称，value：访问次数
     */
    LinkedList<NameAndValue> accessSource();

}
