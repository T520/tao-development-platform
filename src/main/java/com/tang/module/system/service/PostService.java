package com.tang.module.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tang.module.system.entity.Post;

/**
 * 岗位表(Post)表服务接口
 *
 * @author tang
 * @since 2022-01-14 16:14:56
 */
public interface PostService extends IService<Post> {

}
