package com.tang.module.system.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tang.common.constant.cahce.BusinessCacheConstant;
import com.tang.common.constant.ParamName;
import com.tang.common.constant.SafetyConstant;
import com.tang.common.exception.GlobalException;
import com.tang.common.exception.SecurityException;
import com.tang.common.properties.TaoSystemProperties;
import com.tang.common.resources.MinioTemplate;
import com.tang.common.utils.JwtTokenUtil;
import com.tang.common.utils.cache.ParamCache;
import com.tang.module.system.entity.TaoFile;
import com.tang.module.system.mapper.RoleMapper;
import com.tang.module.system.mapper.UserMapper;
import com.tang.module.system.entity.User;
import com.tang.module.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;


import java.util.ArrayList;
import java.util.List;

/**
 * 用户表(User)表服务实现类
 *
 * @author tang
 * @since 2022-01-12 18:04:55
 */
@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private StringRedisTemplate  stringRedisTemplate;

    @Autowired
    private TaoSystemProperties taoSystemProperties;

    @Autowired
    private MinioTemplate minioTemplate;

    @Autowired
    private TransactionTemplate transactionTemplate;


    @Override
    public User userDetails(String username) {
        User user = userMapper.selectOne(Wrappers.<User>lambdaQuery().eq(User::getAccount, username));
        if (user == null){
            return null;
        }
        List<String> roleIds = StrUtil.splitTrim(user.getRoleId(), ",");
        //设置角色
        user.setRoleList(new ArrayList<>());
        user.getRoleList().addAll(roleMapper.selectBatchIds(roleIds));
        return user;
    }

    @Override
    public String login(String username, String password) {
        User user = this.userDetails(username);
        if (user == null || ! passwordEncoder.matches(password,user.getPassword())){
            throw new SecurityException("用户名或密码错误");
        }
        if (! user.isEnabled()){
            throw new SecurityException("当前账户为启用，请联系运营管理员");
        }
        //设置Security登录信息
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new
                UsernamePasswordAuthenticationToken(user,null,user.getAuthorities());
        //获得当前请求
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        //设置当前请求
        usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(attributes.getRequest()));
        //在上下文环境中设置该用户的信息和权限
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
        //获得令牌有效时长
        String param = ParamCache.getValue(ParamName.TOKEN_TIME);
        //生成token
        String jwt = JwtTokenUtil.createJWT(SafetyConstant.SALT, Long.valueOf(param) * 60 * 1000, username);
        //存储在redis中
        if (taoSystemProperties.isRedisToken()) {
            stringRedisTemplate.opsForValue().set(BusinessCacheConstant.TOKEN_PREFIX + user.getUsername(),jwt);
        }
        return jwt;
    }

    @Override
    public Boolean resetPassword(String id, String newPassword) {
        //密码加密
        String encode = passwordEncoder.encode(newPassword);
        //更新
        return userMapper.update(new User(), Wrappers.<User>lambdaUpdate().
                set(User::getPassword,encode).
                eq(User::getId, id)) == 1;
    }


    @Override
    public Boolean updatePassword(String oldPassword, String newPassword) {
        //获得当前用户登录信息
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null){
            throw new GlobalException("无法获取当前认证信息");
        }
        //获得当前用户详细信息
        User user = (User) authentication.getPrincipal();
        if (user == null){
            throw new GlobalException("无法获取当前用户信息");
        }
        if (!passwordEncoder.matches(oldPassword,user.getPassword())){
            throw new GlobalException("输入的原密码不正确");
        }
        //修改密码
        return userMapper.update(new User(),Wrappers.<User>lambdaUpdate()
                .set(User::getPassword,passwordEncoder.encode(newPassword))
                .eq(User::getId,user.getId())) == 1;
    }

    @Override
    public String avatarUpdate(MultipartFile file, Long id) {
        //调用minio上传该文件
        TaoFile taoFile = minioTemplate.putFile(file);
        //使用编程事务
        transactionTemplate.execute(transactionStatus ->{
            //更新数据
            userMapper.update(new User(),Wrappers.<User>lambdaUpdate().
                    set(User::getAvatar,taoFile.getLink()).
                    eq(User::getId,id));
            return Boolean.TRUE;
        });
        return taoFile.getLink();
    }
}
