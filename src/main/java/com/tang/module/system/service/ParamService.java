package com.tang.module.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tang.module.system.entity.Param;

/**
 * 参数表(Param)表服务接口
 *
 * @author tang
 * @since 2022-01-13 12:47:13
 */
public interface ParamService extends IService<Param> {

}
