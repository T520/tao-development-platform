package com.tang.module.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tang.module.system.entity.RoleMenu;

/**
 * 角色菜单关联表(RoleMenu)表服务接口
 *
 * @author tang
 * @since 2022-01-20 17:29:39
 */
public interface RoleMenuService extends IService<RoleMenu> {

}
