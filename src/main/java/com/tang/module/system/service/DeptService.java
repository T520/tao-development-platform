package com.tang.module.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tang.module.system.entity.Dept;
import com.tang.module.system.vo.DeptVo;

import java.util.List;

/**
 * 部门表(Dept)表服务接口
 *
 * @author tang
 * @since 2022-01-14 16:14:56
 */
public interface DeptService extends IService<Dept> {

    /**
     * 构建树结构
     * @param deptCategory 部门类型
     * @return 树形数据
     */
    List<DeptVo> tree(Integer deptCategory);

    /**
     * 创建树
     * @param fatherId 父ID
     * @param dataList 全部数据
     * @return
     */
    List<DeptVo> createTree(Long fatherId,List<DeptVo> dataList);


}
