package com.tang.module.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tang.module.system.entity.Dict;
import com.tang.module.system.entity.DictBiz;

import java.util.List;
import java.util.Map;

/**
 * 业务字典表(DictBiz)表服务接口
 *
 * @author tang
 * @since 2022-01-20 11:49:23
 */
public interface DictBizService extends IService<DictBiz> {

    /**
     * 获取字典列表
     * @param codeList 字典code集合
     * @return 字典map集合，key为字典code，value为字典列表
     */
    public Map<String, List<DictBiz>> list(List<String> codeList);


    /**
     * 构建字典树
     * @return 树结构
     */
    public List<DictBiz> tree();

    /**
     * 构建树
     * @param fid 父id
     * @param dictBizList 数据集合
     * @return 树结构
     */
    public List<DictBiz> createTree(Long fid, List<DictBiz> dictBizList);
}
