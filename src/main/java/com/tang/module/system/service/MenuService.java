package com.tang.module.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tang.module.system.entity.Menu;
import com.tang.module.system.entity.User;
import com.tang.module.system.vo.MenuVo;

import java.lang.reflect.Method;
import java.util.List;

/**
 * 菜单表(Menu)表服务接口
 *
 * @author tang
 * @since 2022-01-20 17:29:39
 */
public interface MenuService extends IService<Menu> {

    /**
     * 获得改用户的全部按钮
     * @param user 用户
     * @return 按钮集合
     */
    public List<Menu> button(User user);

    /**
     * 构建菜单树
     * @param category 菜单类别
     * @return 树结构
     */
    public List<Menu> tree(String category);

    /**
     * 构建菜单树,提供给登录接口使用
     * @return 树结构
     */
    public List<Menu> loginTree();

    /**
     * 构建树
     * @param fid 父id
     * @param menuList 数据集合
     * @return 树结构
     */
    public List<Menu> createTree(Long fid,List<Menu> menuList);


    /**
     * 构建完成菜单树
     * @param status 状态
     * @return 树结构
     */
    public List<MenuVo> treeVo(Integer status);

    /**
     * 构建树
     * @param fid 父id
     * @param menuList 数据集合
     * @return 树结构
     */
    public List<MenuVo> createTreeVo(Long fid,List<MenuVo> menuList);

    /**
     * 获取对应层数的菜单
     * @param level 层数
     * @return 菜单集合
     */
    public List<Menu> parentMenuLevel(Integer level);

    /**
     * 获取对应菜单的父菜单，格式为1,2,3
     * @param category 当前菜单类型
     * @param parentId 父ID
     * @param id 菜单ID
     * @return 父菜单字符串
     */
    public String parentMenuCommaString(Integer category,Long parentId,Long id);


    /**
     * 通过角色ID获得该角色拥有的菜单ID
     * @param id 角色ID
     * @return 菜单ID
     */
    public List<String> roleGetMenu(Long id);

}
