package com.tang.module.system.service.impl;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tang.common.resources.Kv;
import com.tang.module.system.mapper.LogApiMapper;
import com.tang.module.system.entity.LogApi;
import com.tang.module.system.service.LogApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 接口日志表(LogApi)表服务实现类
 *
 * @author tang
 * @since 2022-01-18 18:08:15
 */
@Service("logApiService")
public class LogApiServiceImpl extends ServiceImpl<LogApiMapper, LogApi> implements LogApiService {

}
