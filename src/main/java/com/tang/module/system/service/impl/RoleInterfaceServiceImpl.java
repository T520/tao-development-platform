package com.tang.module.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tang.module.system.mapper.RoleInterfaceMapper;
import com.tang.module.system.entity.RoleInterface;
import com.tang.module.system.service.RoleInterfaceService;
import org.springframework.stereotype.Service;

/**
 * 角色数据权限关联表(RoleInterface)表服务实现类
 *
 * @author tang
 * @since 2022-03-18 12:11:56
 */
@Service("roleInterfaceService")
public class RoleInterfaceServiceImpl extends ServiceImpl<RoleInterfaceMapper, RoleInterface> implements RoleInterfaceService {

}
