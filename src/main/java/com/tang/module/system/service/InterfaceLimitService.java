package com.tang.module.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tang.module.system.entity.InterfaceLimit;

/**
 * 接口访问次数
(InterfaceLimit)表服务接口
 *
 * @author tang
 * @since 2022-01-21 16:28:34
 */
public interface InterfaceLimitService extends IService<InterfaceLimit> {

    /**
     *  清空访问次数
     * @param userAccount 用户账号
     * @param interfaceType 接口类型
     * @return
     */
    public Boolean empty(String userAccount, String interfaceType);

    /**
     * 新用户初始化
     * @return
     */
    public Boolean initNumber();

    /**
     * 新增访问次数
     * @param userAccount 用户账号
     * @param interfaceType  接口类型
     * @param number 次数
     * @return
     */
    public Boolean addNumber(String userAccount,Long number,String interfaceType);


}
