package com.tang.module.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tang.module.system.entity.Dict;

import java.util.List;
import java.util.Map;

/**
 * 字典表(Dict)表服务接口
 *
 * @author tang
 * @since 2022-01-20 11:49:22
 */
public interface DictService extends IService<Dict> {

    /**
     * 获取字典列表
     * @param codeList 字典code集合
     * @return 字典map集合，key为字典code，value为字典列表
     */
    public Map<String, List<Dict>> list(List<String> codeList);

    /**
     * 构建字典树
     * @return 树结构
     */
    public List<Dict> tree();

    /**
     * 构建树
     * @param fid 父id
     * @param dictList 数据集合
     * @return 树结构
     */
    public List<Dict> createTree(Long fid, List<Dict> dictList);

}
