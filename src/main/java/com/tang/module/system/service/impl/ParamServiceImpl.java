package com.tang.module.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tang.module.system.mapper.ParamMapper;
import com.tang.module.system.entity.Param;
import com.tang.module.system.service.ParamService;
import org.springframework.stereotype.Service;

/**
 * 参数表(Param)表服务实现类
 *
 * @author tang
 * @since 2022-01-13 12:47:13
 */
@Service("paramService")
public class ParamServiceImpl extends ServiceImpl<ParamMapper, Param> implements ParamService {

}
