package com.tang.module.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tang.module.system.entity.RoleInterface;

/**
 * 角色数据权限关联表(RoleInterface)表服务接口
 *
 * @author tang
 * @since 2022-03-18 12:11:56
 */
public interface RoleInterfaceService extends IService<RoleInterface> {

}
