package com.tang.module.system.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.tang.module.system.entity.Menu;
import com.tang.module.system.mapper.MenuMapper;
import com.tang.module.system.service.SearchService;
import com.tang.module.system.vo.MenuSearchVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author tang
 * @date 2022/4/20 16:11
 */
@Service
public class SearchServiceImpl implements SearchService {

    @Resource
    private MenuMapper menuMapper;


    @Override
    public List<MenuSearchVo> menuKeywordSearch(String keyword) {
        //开始模糊搜索,这里剔除了按钮类型和没有路由的菜单
        List<Menu> menuList = menuMapper.selectList(Wrappers.<Menu>lambdaQuery().
                eq(Menu::getCategory, 1).
                eq(Menu::getStatus,1).
                isNotNull(Menu::getPath).
                ne(Menu::getPath,"").
                like(Menu::getName, keyword));
        List<MenuSearchVo> menuSearchVoList = new ArrayList<>();
        //遍历全部菜单，查询出父菜单
        for (Menu menu : menuList) {
            MenuSearchVo menuSearchVo = new MenuSearchVo();
            menuSearchVo.setPath(menu.getPath());
            //查询出父菜单
            Menu parentMenu = menuMapper.selectOne(Wrappers.<Menu>lambdaQuery().select(Menu::getName).eq(Menu::getId, menu.getParentId()));
            //如果当前为二级菜单
            if (menu.getParentId() == 0){
                menuSearchVo.setPathList(Arrays.asList(parentMenu.getName(),menu.getName()));
            }else {
                //获得顶级菜单的名称
                String parentName = menuMapper.getParentName(menu.getId());
                menuSearchVo.setPathList(Arrays.asList(parentName,parentMenu.getName(),menu.getName()));
            }
            menuSearchVoList.add(menuSearchVo);
        }
        return menuSearchVoList;
    }
}
