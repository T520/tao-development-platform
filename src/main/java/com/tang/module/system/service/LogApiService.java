package com.tang.module.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tang.common.resources.Kv;
import com.tang.module.system.entity.LogApi;

import java.util.List;
import java.util.Map;

/**
 * 接口日志表(LogApi)表服务接口
 *
 * @author tang
 * @since 2022-01-18 18:08:15
 */
public interface LogApiService extends IService<LogApi> {





}
