package com.tang.module.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tang.module.system.entity.LogError;

/**
 * 错误日志表(LogError)表服务接口
 *
 * @author tang
 * @since 2022-01-20 10:34:00
 */
public interface LogErrorService extends IService<LogError> {

}
