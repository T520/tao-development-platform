package com.tang.module.system.service.impl;

import cn.hutool.core.text.StrSplitter;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tang.module.system.entity.RoleMenu;
import com.tang.module.system.mapper.RoleMapper;
import com.tang.module.system.entity.Role;
import com.tang.module.system.mapper.RoleMenuMapper;
import com.tang.module.system.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 角色表(Role)表服务实现类
 *
 * @author tang
 * @since 2022-01-13 10:31:42
 */
@Service("roleService")
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Resource
    private RoleMapper roleMapper;

    @Resource
    private RoleMenuMapper roleMenuMapper;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateRoleMenu(String menuIds, String roleId) {
        //获得菜单ID集合
        List<String> menuList = StrSplitter.split(menuIds, ",", true, true);
        //先删除全部关联关系
        roleMenuMapper.delete(Wrappers.<RoleMenu>lambdaQuery().eq(RoleMenu::getRoleId,roleId));
        //循环新增数据
        for (String s : menuList) {
            RoleMenu roleMenu = new RoleMenu();
            roleMenu.setMenuId(Long.valueOf(s));
            roleMenu.setRoleId(Long.valueOf(roleId));
            roleMenuMapper.insert(roleMenu);
        }
        return Boolean.TRUE;
    }
}
