package com.tang.module.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tang.module.system.entity.Role;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 角色表(Role)表服务接口
 *
 * @author tang
 * @since 2022-01-13 10:31:42
 */
public interface RoleService extends IService<Role> {

    /**
     * 更新角色拥有的菜单
     * @param menuIds 菜单Id字符串集合（，分割）
     * @param roleId 角色ID
     * @return 是否成功
     */
    Boolean  updateRoleMenu(String menuIds,String roleId);

}
