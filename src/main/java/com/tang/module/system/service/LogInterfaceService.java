package com.tang.module.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tang.module.system.entity.LogInterface;

/**
 * 接口日志表(LogInterface)表服务接口
 *
 * @author tang
 * @since 2022-01-21 14:04:09
 */
public interface LogInterfaceService extends IService<LogInterface> {


}
