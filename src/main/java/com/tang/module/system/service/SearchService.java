package com.tang.module.system.service;

import com.tang.module.system.entity.Menu;
import com.tang.module.system.vo.MenuSearchVo;

import java.util.List;

/**
 * 系统搜索服务接口
 * @author tang
 * @date 2022/4/20 16:11
 */
public interface SearchService {

    /**
     * 菜单关键字搜索
     * @param keyword 关键字
     * @return 命中的数据
     */
    List<MenuSearchVo> menuKeywordSearch(String keyword);

}
