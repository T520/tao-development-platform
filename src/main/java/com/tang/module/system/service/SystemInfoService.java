package com.tang.module.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tang.common.resources.TaoPage;

/**
 * 系统信息接口
 * @author tang
 * @date 2022/1/24 10:57
 */
public interface SystemInfoService {


    /**
     * 获取在线用户列表
     * @param taoPage 分页参数
     * @return page
     */
    Page<Object> onlineUsers(TaoPage taoPage);

    /**
     * 强制用户下线
     * @param account 账户名
     * @return 是否成功
     */
    Boolean offline(String account);

}
