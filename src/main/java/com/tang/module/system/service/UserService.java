package com.tang.module.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tang.module.system.entity.User;
import org.springframework.web.multipart.MultipartFile;


/**
 * 用户表(User)表服务接口
 *
 * @author tang
 * @since 2022-01-12 18:04:55
 */
public interface UserService extends IService<User> {


    /**
     * 根据用户名获取用户详细信息
     * @param username 用户名（登录账号）
     * @return user
     */
    User userDetails(String username);

    /**
     * 登录接口，验证用户名和密码，返回token
     * @param username 登录用户名
     * @param password 登录密码
     * @return token令牌
     */
    String login(String username,String password);

    /**
     * 管理员重置密码
     * @param id 用户id
     * @param newPassword 新密码
     * @return 是否成功
     */
    Boolean resetPassword(String id, String newPassword);

    /**
     * 用户修改密码
     * @param oldPassword 老密码
     * @param newPassword 新密码
     * @return 是否成功
     */
    Boolean updatePassword(String oldPassword,String newPassword);



    /**
     * 上传头像
     * @param file 头像文件
     * @param id 用户id
     * @return 新头像链接
     */
    String avatarUpdate(MultipartFile file, Long id);
}
