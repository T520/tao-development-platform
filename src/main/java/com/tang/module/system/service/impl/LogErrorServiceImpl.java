package com.tang.module.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tang.module.system.mapper.LogErrorMapper;
import com.tang.module.system.entity.LogError;
import com.tang.module.system.service.LogErrorService;
import org.springframework.stereotype.Service;

/**
 * 错误日志表(LogError)表服务实现类
 *
 * @author tang
 * @since 2022-01-20 10:34:00
 */
@Service("logErrorService")
public class LogErrorServiceImpl extends ServiceImpl<LogErrorMapper, LogError> implements LogErrorService {

}
