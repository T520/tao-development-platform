package com.tang.module.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tang.module.system.mapper.PostMapper;
import com.tang.module.system.entity.Post;
import com.tang.module.system.service.PostService;
import org.springframework.stereotype.Service;

/**
 * 岗位表(Post)表服务实现类
 *
 * @author tang
 * @since 2022-01-14 16:14:56
 */
@Service("postService")
public class PostServiceImpl extends ServiceImpl<PostMapper, Post> implements PostService {

}
