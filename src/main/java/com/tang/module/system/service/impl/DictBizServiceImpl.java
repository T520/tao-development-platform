package com.tang.module.system.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tang.module.system.entity.Dict;
import com.tang.module.system.mapper.DictBizMapper;
import com.tang.module.system.entity.DictBiz;
import com.tang.module.system.service.DictBizService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 业务字典表(DictBiz)表服务实现类
 *
 * @author tang
 * @since 2022-01-20 11:49:23
 */
@Service("dictBizService")
public class DictBizServiceImpl extends ServiceImpl<DictBizMapper, DictBiz> implements DictBizService {


    @Autowired
    private DictBizMapper dictBizMapper;

    @Override
    public Map<String, List<DictBiz>> list(List<String> codeList) {
        Map<String, List<DictBiz>> map = new HashMap<>();
        for (String s : codeList) {
            List<DictBiz> dicts = dictBizMapper.selectList(Wrappers.<DictBiz>lambdaQuery()
                    .select(DictBiz::getDictKey, DictBiz::getDictValue)
                    .eq(DictBiz::getCode, s)
                    .ne(DictBiz::getDictKey, 0)
                    .orderByAsc(DictBiz::getSort));
            map.put(s,dicts);
        }
        return map;
    }

    @Override
    public List<DictBiz> tree() {
        List<DictBiz> dictBizList = dictBizMapper.selectList(null);
        return createTree(0L,dictBizList);
    }

    @Override
    public List<DictBiz> createTree(Long fid, List<DictBiz> dictBizList) {
        List<DictBiz> dictBizs = new ArrayList<>();
        //遍历寻找子节点
        for (DictBiz dictBiz : dictBizList) {
            if (dictBiz.getParentId().equals(fid)){
                dictBizs.add(dictBiz);
                //设置子字典
                dictBiz.setChildren(createTree(dictBiz.getId(),dictBizList));
            }
        }
        return dictBizs;
    }
}
