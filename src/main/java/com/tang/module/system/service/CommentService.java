package com.tang.module.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tang.module.system.entity.Comment;

/**
 * 评论表(Comment)表服务接口
 *
 * @author tang
 * @since 2022-03-20 21:34:00
 */
public interface CommentService extends IService<Comment> {

}
