package com.tang.module.system.entity;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;

/**
 * 角色表(Role)实体类
 *
 * @author tang
 * @since 2022-01-13 10:31:42
 */
@Data
@ApiModel(value = "tao_role 实体类",description = "tao_role 实体类")
@TableName("tao_role")
public class Role implements Serializable {
    private static final long serialVersionUID = -11631111765338592L;
    
    /**
    * 主键
    */    
    @TableId(
        value = "id",
        type = IdType.ASSIGN_ID
    )
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "主键")
    private Long id;
    
    /**
    * 角色名
    */    
    @TableField("role_name")
    @ApiModelProperty(value = "角色名")
    private String roleName;
    
    /**
    * 排序
    */    
    @TableField("sort")
    @ApiModelProperty(value = "排序")
    private Integer sort;
    
    /**
    * 角色别名
    */    
    @TableField("role_alias")
    @ApiModelProperty(value = "角色别名")
    private String roleAlias;
    
    /**
    * 是否已删除
    */    
    @TableField("is_deleted")
    @ApiModelProperty(value = "是否已删除")
    private Integer isDeleted;

}
