package com.tang.module.system.entity;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.tang.module.system.vo.DeptVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;

/**
 * 部门表(Dept)实体类
 *
 * @author tang
 * @since 2022-01-14 16:14:56
 */
@Data
@ApiModel(value = "tao_dept 实体类",description = "tao_dept 实体类")
@TableName("tao_dept")
public class Dept  implements Serializable {
    private static final long serialVersionUID = -34243409456871171L;

    /**
    * 主键
    */
    @TableId(
        value = "id",
        type = IdType.ASSIGN_ID
    )
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "主键")
    private Long id;
    
    /**
    * 父主键
    */    
    @TableField("parent_id")
    @ApiModelProperty(value = "父主键")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentId;
    
    /**
    * 祖级列表
    */    
    @TableField("path")
    @ApiModelProperty(value = "祖级列表")
    private String path;
    
    /**
    * 部门类型
    */    
    @TableField("dept_category")
    @ApiModelProperty(value = "部门类型")
    private Integer deptCategory;
    
    /**
    * 部门名
    */    
    @TableField("dept_name")
    @ApiModelProperty(value = "部门名")
    private String deptName;
    
    /**
    * 部门全称
    */    
    @TableField("full_name")
    @ApiModelProperty(value = "部门全称")
    private String fullName;
    
    /**
    * 排序
    */    
    @TableField("sort")
    @ApiModelProperty(value = "排序")
    private Integer sort;
    
    /**
    * 备注
    */    
    @TableField("remark")
    @ApiModelProperty(value = "备注")
    private String remark;
    
    /**
    * 是否已删除
    */    
    @TableField("is_deleted")
    @ApiModelProperty(value = "是否已删除")
    private Integer isDeleted;

    /**
     * 子数据
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "孩子")
    private List<DeptVo> children;
    


}
