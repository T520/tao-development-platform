package com.tang.module.system.entity;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import lombok.experimental.Accessors;

/**
 * 角色数据权限关联表(RoleInterface)实体类
 *
 * @author tang
 * @since 2022-03-18 12:11:56
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "tao_role_interface 实体类",description = "tao_role_interface 实体类")
@TableName("tao_role_interface")
public class RoleInterface implements Serializable {
    private static final long serialVersionUID = -36525663506099702L;
    
    /**
    * 主键
    */    
    @TableId(
        value = "id",
        type = IdType.ASSIGN_ID
    )
     @ApiModelProperty(value = "主键")
    private Long id;
    
    /**
    * 权限id
    */    
    @TableField("interface_id")
    @ApiModelProperty(value = "权限id")
    private Long interfaceId;
    
    /**
    * 角色id
    */    
    @TableField("role_id")
    @ApiModelProperty(value = "角色id")
    private Long roleId;
    

     
}
