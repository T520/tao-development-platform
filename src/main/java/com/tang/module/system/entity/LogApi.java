package com.tang.module.system.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 接口日志表(LogApi)实体类
 *
 * @author tang
 * @since 2022-01-19 11:25:44
 */
@Data
@ApiModel(value = "tao_log_api 实体类",description = "tao_log_api 实体类")
@TableName("tao_log_api")
public class LogApi implements Serializable {
    private static final long serialVersionUID = 608838165309736213L;
    
    /**
    * 编号
    */    
    @TableId(
        value = "id",
        type = IdType.ASSIGN_ID
    )
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "编号")
    private Long id;
    
    /**
    * 服务器名
    */    
    @TableField("server_host")
    @ApiModelProperty(value = "服务器名")
    private String serverHost;
    
    /**
    * 服务器IP地址
    */    
    @TableField("server_ip")
    @ApiModelProperty(value = "服务器IP地址")
    private String serverIp;
    
    /**
    * 服务器环境
    */    
    @TableField("env")
    @ApiModelProperty(value = "服务器环境")
    private String env;
    
    /**
    * 日志类型
    */    
    @TableField(value = "type")
    @ApiModelProperty(value = "日志类型")
    private String type;
    
    /**
    * 日志标题
    */    
    @TableField(value = "title",condition = SqlCondition.LIKE)
    @ApiModelProperty(value = "日志标题")
    private String title;
    
    /**
    * 操作方式
    */    
    @TableField("method")
    @ApiModelProperty(value = "操作方式")
    private String method;
    
    /**
    * 请求URI
    */    
    @TableField("request_uri")
    @ApiModelProperty(value = "请求URI")
    private String requestUri;
    
    /**
    * 操作系统
    */    
    @TableField("system")
    @ApiModelProperty(value = "操作系统")
    private String system;
    
    /**
    * 移动端
    */    
    @TableField("mobile_terminal")
    @ApiModelProperty(value = "移动端")
    private Boolean mobileTerminal;
    
    /**
    * 浏览器
    */    
    @TableField("browser")
    @ApiModelProperty(value = "浏览器")
    private String browser;
    
    /**
    * 操作IP地址
    */    
    @TableField("remote_ip")
    @ApiModelProperty(value = "操作IP地址")
    private String remoteIp;
    
    /**
    * 方法类
    */    
    @TableField("method_class")
    @ApiModelProperty(value = "方法类")
    private String methodClass;
    
    /**
    * 方法名
    */    
    @TableField("method_name")
    @ApiModelProperty(value = "方法名")
    private String methodName;
    
    /**
    * 操作提交的数据
    */    
    @TableField("params")
    @ApiModelProperty(value = "操作提交的数据")
    private String params;
    
    /**
    * 执行时间
    */    
    @TableField("time")
    @ApiModelProperty(value = "执行时间")
    private String time;
    
    /**
    * 创建者
    */    
    @TableField("create_by")
    @ApiModelProperty(value = "创建者")
    private String createBy;
    
    /**
    * 创建时间
    */
    @DateTimeFormat(
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            locale = "zh", timezone="GMT+8"
    )
    @TableField("create_time")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
    
    /**
    * 用户运营商
    */    
    @TableField("isp")
    @ApiModelProperty(value = "用户运营商")
    private String isp;
    
    /**
    * 用户所在地址
    */    
    @TableField("address")
    @ApiModelProperty(value = "用户所在地址")
    private String address;
    
    /**
    * 异常信息
    */    
    @TableField("exception_msg")
    @ApiModelProperty(value = "异常信息")
    private String exceptionMsg;
    
    /**
    * 访问是否成功
    */    
    @TableField("success")
    @ApiModelProperty(value = "访问是否成功")
    private Boolean success;
    

     
}
