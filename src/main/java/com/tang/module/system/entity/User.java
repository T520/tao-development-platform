package com.tang.module.system.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tang.common.resources.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * 用户表(User)实体类
 *
 * @author tang
 * @since 2022-01-12 18:04:55
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "tao_user 实体类",description = "tao_user 实体类")
@TableName("tao_user")
@Accessors(chain = true)
@JsonIgnoreProperties({"password","authorities","enabled"})
public class User extends BaseEntity implements UserDetails {
    private static final long serialVersionUID = -67520042302560291L;

    
    /**
    * 租户ID
    */    
    @TableField("tenant_id")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;
    
    /**
    * 用户编号
    */    
    @TableField("code")
    @ApiModelProperty(value = "用户编号")
    private String code;
    
    /**
    * 用户平台
    */    
    @TableField("user_type")
    @ApiModelProperty(value = "用户平台")
    private Integer userType;
    
    /**
    * 账号
    */    
    @TableField("account")
    @ApiModelProperty(value = "账号")
    private String account;
    
    /**
    * 密码
    */    
    @TableField("password")
    @JsonIgnore
    @ApiModelProperty(value = "密码")
    private String password;
    
    /**
    * 昵称
    */    
    @TableField("name")
    @ApiModelProperty(value = "昵称")
    private String name;
    
    /**
    * 真名
    */    
    @TableField("real_name")
    @ApiModelProperty(value = "真名")
    private String realName;
    
    /**
    * 头像
    */    
    @TableField("avatar")
    @ApiModelProperty(value = "头像")
    private String avatar;
    
    /**
    * 邮箱
    */    
    @TableField("email")
    @ApiModelProperty(value = "邮箱")
    private String email;
    
    /**
    * 手机
    */    
    @TableField("phone")
    @ApiModelProperty(value = "手机")
    private String phone;
    
    /**
    * 生日
    */
    @DateTimeFormat(
            pattern = "yyyy-MM-dd"
    )
    @JsonFormat(
            pattern = "yyyy-MM-dd",
            locale = "zh", timezone="GMT+8"
    )
    @TableField("birthday")
    @ApiModelProperty(value = "生日")
    private Date birthday;
    
    /**
    * 性别
    */    
    @TableField("sex")
    @ApiModelProperty(value = "性别")
    private Integer sex;
    
    /**
    * 角色id
    */    
    @TableField("role_id")
    @ApiModelProperty(value = "角色id")
    private String roleId;
    
    /**
    * 部门id
    */    
    @TableField("dept_id")
    @ApiModelProperty(value = "部门id")
    private String deptId;
    
    /**
    * 岗位id
    */    
    @TableField("post_id")
    @ApiModelProperty(value = "岗位id")
    private String postId;

    /**
     * 角色列表
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "角色集合")
    private List<Role> roleList;

/*    @TableField(exist = false)
    @ApiModelProperty(value = "角色名称集合")
    private GrantedAuthority[] authorities;*/

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roleList.stream().map(role -> new SimpleGrantedAuthority(role.getRoleName())).collect(Collectors.toList());
    }

    @Override
    public String getUsername() {
        return account;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return status == 1;
    }
}
