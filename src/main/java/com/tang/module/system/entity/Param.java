package com.tang.module.system.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.SqlCondition;
import com.tang.common.resources.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.EqualsAndHashCode;

/**
 * 参数表(Param)实体类
 *
 * @author tang
 * @since 2022-01-13 12:47:13
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "tao_param 实体类",description = "tao_param 实体类")
@TableName("tao_param")
public class Param extends BaseEntity  {
    private static final long serialVersionUID = -45955450489511666L;
    
    /**
    * 参数名
    */    
    @TableField(value = "param_name",condition = SqlCondition.LIKE)
    @ApiModelProperty(value = "参数名")
    private String paramName;
    
    /**
    * 参数键
    */    
    @TableField(value = "param_key",condition = SqlCondition.LIKE)
    @ApiModelProperty(value = "参数键")
    private String paramKey;
    
    /**
    * 参数值
    */    
    @TableField("param_value")
    @ApiModelProperty(value = "参数值")
    private String paramValue;
    
    /**
    * 备注
    */    
    @TableField("remark")
    @ApiModelProperty(value = "备注")
    private String remark;


     
}
