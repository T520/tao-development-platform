package com.tang.module.system.entity;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;

/**
 * 角色数据权限关联表(RoleScope)实体类
 *
 * @author tang
 * @since 2022-01-13 11:38:44
 */
@Data
@ApiModel(value = "tao_role_scope 实体类",description = "tao_role_scope 实体类")
@TableName("tao_role_scope")
public class RoleScope implements Serializable {
    private static final long serialVersionUID = -15289372813846968L;
    
    /**
    * 主键
    */    
    @TableId(
        value = "id",
        type = IdType.ASSIGN_ID
    )
    @JsonSerialize(using = ToStringSerializer.class)
     @ApiModelProperty(value = "主键")
    private Long id;
    
    /**
    * 权限id
    */    
    @TableField("scope_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "权限id")
    private Long scopeId;
    
    /**
    * 角色id
    */    
    @TableField("role_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "角色id")
    private Long roleId;
    

     
}
