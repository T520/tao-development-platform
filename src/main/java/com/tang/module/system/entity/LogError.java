package com.tang.module.system.entity;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import lombok.experimental.Accessors;

/**
 * 错误日志表(LogError)实体类
 *
 * @author tang
 * @since 2022-01-20 10:34:00
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "tao_log_error 实体类",description = "tao_log_error 实体类")
@TableName("tao_log_error")
public class LogError implements Serializable {
    private static final long serialVersionUID = 934823564873986588L;
    
    /**
    * 编号
    */    
    @TableId(
        value = "id",
        type = IdType.ASSIGN_ID
    )
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "编号")
    private Long id;
    
    /**
    * 服务器名
    */    
    @TableField("server_host")
    @ApiModelProperty(value = "服务器名")
    private String serverHost;
    
    /**
    * 服务器IP地址
    */    
    @TableField("server_ip")
    @ApiModelProperty(value = "服务器IP地址")
    private String serverIp;
    
    /**
    * 系统环境
    */    
    @TableField("env")
    @ApiModelProperty(value = "系统环境")
    private String env;
    
    /**
    * 操作方式
    */    
    @TableField("method")
    @ApiModelProperty(value = "操作方式")
    private String method;
    
    /**
    * 请求URI
    */    
    @TableField("request_uri")
    @ApiModelProperty(value = "请求URI")
    private String requestUri;
    
    /**
    * 操作系统
    */    
    @TableField("system")
    @ApiModelProperty(value = "操作系统")
    private String system;
    
    /**
    * 运营商
    */    
    @TableField("isp")
    @ApiModelProperty(value = "运营商")
    private String isp;
    
    /**
    * 移动端
    */    
    @TableField("mobile_terminal")
    @ApiModelProperty(value = "移动端")
    private Boolean mobileTerminal;
    
    /**
    * 用户所在地址
    */    
    @TableField("address")
    @ApiModelProperty(value = "用户所在地址")
    private String address;
    
    /**
    * 浏览器
    */    
    @TableField("browser")
    @ApiModelProperty(value = "浏览器")
    private String browser;
    
    /**
    * 堆栈
    */    
    @TableField("stack_trace")
    @ApiModelProperty(value = "堆栈")
    private String stackTrace;
    
    /**
    * 异常名
    */    
    @TableField("exception_name")
    @ApiModelProperty(value = "异常名")
    private String exceptionName;
    
    /**
    * 异常信息
    */    
    @TableField("message")
    @ApiModelProperty(value = "异常信息")
    private String message;
    
    /**
    * 错误行数
    */    
    @TableField("line_number")
    @ApiModelProperty(value = "错误行数")
    private Integer lineNumber;
    
    /**
    * 操作IP地址
    */    
    @TableField("remote_ip")
    @ApiModelProperty(value = "操作IP地址")
    private String remoteIp;
    
    /**
    * 方法类
    */    
    @TableField("method_class")
    @ApiModelProperty(value = "方法类")
    private String methodClass;
    
    /**
    * 文件名
    */    
    @TableField("file_name")
    @ApiModelProperty(value = "文件名")
    private String fileName;
    
    /**
    * 方法名
    */    
    @TableField("method_name")
    @ApiModelProperty(value = "方法名")
    private String methodName;
    
    /**
    * 操作提交的数据
    */    
    @TableField("params")
    @ApiModelProperty(value = "操作提交的数据")
    private String params;
    
    /**
    * 创建者
    */    
    @TableField("create_by")
    @ApiModelProperty(value = "创建者")
    private String createBy;
    
    /**
    * 创建时间
    */    
    @TableField("create_time")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
    

     
}
