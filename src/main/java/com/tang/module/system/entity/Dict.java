package com.tang.module.system.entity;

import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 字典表(Dict)实体类
 *
 * @author tang
 * @since 2022-01-20 11:49:22
 */
@Data
@ApiModel(value = "tao_dict 实体类",description = "tao_dict 实体类")
@TableName("tao_dict")
public class Dict implements Serializable {
    private static final long serialVersionUID = -85966343026967809L;
    
    /**
    * 主键
    */    
    @TableId(
        value = "id",
        type = IdType.ASSIGN_ID
    )
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "主键")
    private Long id;
    
    /**
    * 父主键
    */    
    @TableField("parent_id")
    @ApiModelProperty(value = "父主键")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentId;
    
    /**
    * 字典码
    */    
    @TableField("code")
    @ApiModelProperty(value = "字典码")
    private String code;
    
    /**
    * 字典键
    */    
    @TableField(value = "dict_key")
    @ApiModelProperty(value = "字典值")
    private String dictKey;
    
    /**
    * 字典值
    */    
    @TableField(value = "dict_value",condition = SqlCondition.LIKE)
    @ApiModelProperty(value = "字典名称")
    private String dictValue;
    
    /**
    * 排序
    */    
    @TableField("sort")
    @ApiModelProperty(value = "排序")
    private Integer sort;
    
    /**
    * 字典备注
    */    
    @TableField("remark")
    @ApiModelProperty(value = "字典备注")
    private String remark;
    
    /**
    * 是否已封存
    */    
    @TableField("is_sealed")
    @ApiModelProperty(value = "是否已封存")
    private Integer isSealed;
    
    /**
    * 是否已删除
    */    
    @TableField("is_deleted")
    @ApiModelProperty(value = "是否已删除")
    private Integer isDeleted;

    /**
     * 子字典列表
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "子字典列表")
    private List<Dict> children;
     
}
