package com.tang.module.system.entity;


import java.util.List;

import com.baomidou.mybatisplus.annotation.*;
import com.tang.common.resources.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 接口配置表(InterfaceConfiguration)实体类
 *
 * @author tang
 * @since 2022-03-18 11:13:54
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "tao_interface_configuration 实体类",description = "tao_interface_configuration 实体类")
@TableName("tao_interface_configuration")
public class InterfaceConfiguration extends BaseEntity {

    /**
    * 接口名称
    */    
    @TableField(value = "interface_name",condition = SqlCondition.LIKE)
    @ApiModelProperty(value = "接口名称")
    private String interfaceName;
    
    /**
    * 接口标记
    */    
    @TableField(value = "sign",condition = SqlCondition.LIKE)
    @ApiModelProperty(value = "接口标记")
    private String sign;
    
    /**
    * 接口类型，数据字典（接口类型）
    */    
    @TableField("interface_type")
    @ApiModelProperty(value = "接口类型，数据字典（接口类型）")
    private Long interfaceType;
    
    /**
    * 是否开启接口访问限制
    */    
    @TableField("open_limit")
    @ApiModelProperty(value = "是否开启接口访问限制")
    private Boolean openLimit;

    
    /**
    * 是否需要开启接口权限
    */    
    @TableField("open_auth")
    @ApiModelProperty(value = "是否需要开启接口权限")
    private Boolean openAuth;

    /**
     * 请求方法
     */
    @TableField("method")
    @ApiModelProperty(value = "请求方法")
    private Long method;

    /**
     * 角色集合
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "接口权限")
    private List<Role> roleList;


}
