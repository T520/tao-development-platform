package com.tang.module.system.entity;

import java.io.Serializable;

import com.tang.common.resources.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.EqualsAndHashCode;

/**
 * 参数表(ApiOpenConfig)实体类
 *
 * @author tang
 * @since 2022-02-15 16:06:20
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "tao_api_open_config 实体类",description = "tao_api_open_config 实体类")
@TableName("tao_api_open_config")
public class ApiOpenConfig extends BaseEntity {
    private static final long serialVersionUID = 492172324662981685L;

    
    /**
    * 接口名称
    */    
    @TableField("name")
    @ApiModelProperty(value = "接口名称")
    private String name;
    
    /**
    * 启用的机构id
    */    
    @TableField("mechanism_id")
    @ApiModelProperty(value = "启用的机构id")
    private String mechanismId;
    
    /**
    * 启用的机构简称
    */    
    @TableField("mechanism_name")
    @ApiModelProperty(value = "启用的机构简称")
    private String mechanismName;
    
    /**
    * 是否有请求头
    */    
    @TableField("is_head")
    @ApiModelProperty(value = "是否有请求头")
    private Boolean isHead;
    
    /**
    * 请求头参数模板
    */    
    @TableField("head_template")
    @ApiModelProperty(value = "请求头参数模板")
    private String headTemplate;
    
    /**
    * 是否有url请求参数
    */    
    @TableField("is_url")
    @ApiModelProperty(value = "是否有url请求参数")
    private Boolean isUrl;
    
    /**
    * url请求参数模板
    */    
    @TableField("url_template")
    @ApiModelProperty(value = "url请求参数模板")
    private String urlTemplate;
    
    /**
    * 备注
    */    
    @TableField("remark")
    @ApiModelProperty(value = "备注")
    private String remark;
    
    /**
    * 请求url
    */    
    @TableField("url")
    @ApiModelProperty(value = "请求url")
    private String url;
    
    /**
    * 接口编码
    */    
    @TableField("api_code")
    @ApiModelProperty(value = "接口编码")
    private String apiCode;
    
    /**
    * 生效的class路径
    */    
    @TableField("class_path")
    @ApiModelProperty(value = "生效的class路径")
    private String classPath;
    

     
}
