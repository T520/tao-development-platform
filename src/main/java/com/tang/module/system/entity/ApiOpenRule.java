package com.tang.module.system.entity;

import java.io.Serializable;

import com.tang.common.resources.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.EqualsAndHashCode;

/**
 * 开放API规则(ApiOpenRule)实体类
 *
 * @author tang
 * @since 2022-02-15 10:44:33
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "tao_api_open_rule 实体类",description = "tao_api_open_rule 实体类")
@TableName("tao_api_open_rule")
public class ApiOpenRule extends BaseEntity  {
    private static final long serialVersionUID = 136168169676542332L;
    
    /**
    * 参数名称
    */    
    @TableField("name")
    @ApiModelProperty(value = "参数名称")
    private String name;
    
    /**
    * 机构id
    */    
    @TableField("mechanism_id")
    @ApiModelProperty(value = "机构id")
    private String mechanismId;

    
    /**
    * 参数值
    */    
    @TableField("value")
    @ApiModelProperty(value = "参数值")
    private String value;
    
    /**
    * 备注
    */    
    @TableField("remark")
    @ApiModelProperty(value = "备注")
    private String remark;
    
    /**
    * 接口编码
    */    
    @TableField("api_code")
    @ApiModelProperty(value = "接口编码")
    private String apiCode;
    
    /**
    * 参数编码
    */    
    @TableField("rule_code")
    @ApiModelProperty(value = "参数编码")
    private String ruleCode;
    

     
}
