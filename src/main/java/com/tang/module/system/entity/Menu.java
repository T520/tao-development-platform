package com.tang.module.system.entity;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.tang.module.system.vo.MenuVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import lombok.experimental.Accessors;

/**
 * 菜单表(Menu)实体类
 *
 * @author tang
 * @since 2022-01-20 17:35:52
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "tao_menu 实体类",description = "tao_menu 实体类")
@TableName("tao_menu")
public class Menu implements Serializable {
    private static final long serialVersionUID = -53425913192331404L;

    /**
    * 主键
    */
    @TableId(
        value = "id",
        type = IdType.ASSIGN_ID
    )
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "主键")
    private Long id;

    /**
    * 父级菜单
    */
    @TableField("parent_id")
    @ApiModelProperty(value = "父级菜单")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long parentId;

    /**
    * 菜单编号
    */
    @TableField("code")
    @ApiModelProperty(value = "菜单编号")
    private String code;

    /**
    * 菜单名称
    */
    @TableField("name")
    @ApiModelProperty(value = "菜单名称")
    private String name;

    /**
    * 菜单别名
    */
    @TableField("permission_id")
    @ApiModelProperty(value = "菜单别名")
    private String permissionId;

    /**
    * 请求地址
    */
    @TableField("path")
    @ApiModelProperty(value = "请求地址")
    private String path;

    /**
    * 菜单图标
    */
    @TableField("icon")
    @ApiModelProperty(value = "菜单图标")
    private String icon;

    /**
    * 排序
    */
    @TableField("sort")
    @ApiModelProperty(value = "排序")
    private Integer sort;

    /**
    * 菜单类型
    */
    @TableField("category")
    @ApiModelProperty(value = "菜单类型")
    private Integer category;

    /**
    * 操作按钮类型
    */
    @TableField("action")
    @ApiModelProperty(value = "操作按钮类型")
    private Integer action;

    /**
    * 是否打开新页面
    */
    @TableField("is_open")
    @ApiModelProperty(value = "是否打开新页面")
    private Boolean isOpen;

    /**
    * 备注
    */
    @TableField("remark")
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
    * 是否已删除
    */
    @TableField("is_deleted")
    @ApiModelProperty(value = "是否已删除")
    private Integer isDeleted;

    /**
     * 状态
     */
    @TableField("status")
    @ApiModelProperty(value = "状态")
    private Integer status;

    /**
     *  子菜单
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "孩子")
    private List<Menu> children;
     
}
