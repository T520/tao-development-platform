package com.tang.module.system.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.tang.common.resources.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.EqualsAndHashCode;

/**
 * 参数表(ApiOpenMechanism)实体类
 *
 * @author tang
 * @since 2022-02-14 17:34:28
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "tao_api_open_mechanism 实体类",description = "tao_api_open_mechanism 实体类")
@TableName("tao_api_open_mechanism")
@JsonInclude(content = JsonInclude.Include.NON_EMPTY)
public class ApiOpenMechanism extends BaseEntity{
    private static final long serialVersionUID = 913719211129264601L;

    
    /**
    * 简称
    */    
    @TableField("name")
    @ApiModelProperty(value = "简称")
    private String name;
    
    /**
    * 企业名称
    */    
    @TableField("enterprise_name")
    @ApiModelProperty(value = "企业名称")
    private String enterpriseName;
    
    /**
    * 统一社会信用代码
    */    
    @TableField("credit_code")
    @ApiModelProperty(value = "统一社会信用代码")
    private String creditCode;
    
    /**
    * 备注
    */    
    @TableField("remark")
    @ApiModelProperty(value = "备注")
    private String remark;
    
    /**
    * 机构编码
    */    
    @TableField("mechanism_code")
    @ApiModelProperty(value = "机构编码")
    private String  mechanismCode;
    

     
}
