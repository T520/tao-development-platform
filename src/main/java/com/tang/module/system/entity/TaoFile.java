/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package com.tang.module.system.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 文件上传实体
 * @author tang
 * @date 2022/1/20 14:26
 */
@Data
@ApiModel(value = "文件上传结果")
public class TaoFile {

	/**
	 * 文件地址
	 */
	@ApiModelProperty(value = "文件访问地址")
	private String link;
	/**
	 * 域名地址 ip+port+桶
	 */
	@ApiModelProperty(value = "文件域名地址")
	private String domain;
	/**
	 * 文件名 文件夹+随机文件名
	 */
	@ApiModelProperty(value = "文件名称")
	private String name;
	/**
	 * 初始文件名 原生文件名
	 */
	@ApiModelProperty(value = "文件原始名称")
	private String originalName;
	/**
	 * 附件表ID
	 */
	@ApiModelProperty(value = "附件表ID")
	private Long attachId;
}
