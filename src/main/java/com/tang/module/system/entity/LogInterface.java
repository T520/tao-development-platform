package com.tang.module.system.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;

/**
 *业务接口日志表(LogInterface)实体类
 *
 * @author tang
 * @since 2022-01-21 14:04:09
 */
@Data
@ApiModel(value = "tao_log_interface 实体类",description = "tao_log_interface 实体类")
@TableName("tao_log_interface")
public class LogInterface implements Serializable {
    private static final long serialVersionUID = -70808846276293647L;
    
    /**
    * id
    */    
    @TableId(
        value = "id",
        type = IdType.ASSIGN_ID
    )
     @ApiModelProperty(value = "id")
    private Long id;
    
    /**
    * 接口类型
    */    
    @TableField("type")
    @ApiModelProperty(value = "接口类型")
    private Integer type;
    
    /**
    * 接口名称
    */    
    @TableField("name")
    @ApiModelProperty(value = "接口名称")
    private String name;
    
    /**
    * 日志内容
    */    
    @TableField("log")
    @ApiModelProperty(value = "日志内容")
    private String log;
    
    /**
    * 操作系统
    */    
    @TableField("system")
    @ApiModelProperty(value = "操作系统")
    private String system;
    
    /**
    * 移动端
    */    
    @TableField("mobile_terminal")
    @ApiModelProperty(value = "移动端")
    private Boolean mobileTerminal;
    
    /**
    * 用户运营商
    */    
    @TableField("isp")
    @ApiModelProperty(value = "用户运营商")
    private String isp;
    
    /**
    * 用户所在地址
    */    
    @TableField("address")
    @ApiModelProperty(value = "用户所在地址")
    private String address;
    
    /**
    * 浏览器
    */    
    @TableField("browser")
    @ApiModelProperty(value = "浏览器")
    private String browser;
    
    /**
    * 操作IP地址
    */    
    @TableField("remote_ip")
    @ApiModelProperty(value = "操作IP地址")
    private String remoteIp;
    
    /**
    * 用户昵称
    */    
    @TableField("user_nickname")
    @ApiModelProperty(value = "用户昵称")
    private String userNickname;
    
    /**
    * 用户账户
    */    
    @TableField("user_account")
    @ApiModelProperty(value = "用户账户")
    private String userAccount;
    
    /**
    * 用户id
    */    
    @TableField("user_id")
    @ApiModelProperty(value = "用户id")
    private Long userId;
    
    /**
    * 创建时间
    */    
    @TableField("create_time")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @TableField(exist = false)
    @ApiModelProperty(value = "参数")
    private Map<String, String[]> parameter;

    @TableField(exist = false)
    @ApiModelProperty(value = "请求标识")
    private String urlTag;

    @TableField(exist = false)
    @ApiModelProperty(value = "用户代理")
    private String userAgent;

     
}
