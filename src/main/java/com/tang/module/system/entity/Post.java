package com.tang.module.system.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.SqlCondition;
import com.tang.common.resources.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.EqualsAndHashCode;

/**
 * 岗位表(Post)实体类
 *
 * @author tang
 * @since 2022-01-14 16:14:56
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "tao_post 实体类",description = "tao_post 实体类")
@TableName("tao_post")
public class Post extends BaseEntity  {
    private static final long serialVersionUID = 561880309071368986L;

    /**
    * 岗位类型
    */    
    @TableField("category")
    @ApiModelProperty(value = "岗位类型")
    private Integer category;
    
    /**
    * 岗位编号
    */    
    @TableField("post_code")
    @ApiModelProperty(value = "岗位编号")
    private String postCode;
    
    /**
    * 岗位名称
    */    
    @TableField(value = "post_name",condition = SqlCondition.LIKE)
    @ApiModelProperty(value = "岗位名称")
    private String postName;
    
    /**
    * 岗位排序
    */    
    @TableField("sort")
    @ApiModelProperty(value = "岗位排序")
    private Integer sort;
    
    /**
    * 岗位描述
    */    
    @TableField("remark")
    @ApiModelProperty(value = "岗位描述")
    private String remark;

    

     
}
