package com.tang.module.system.entity;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 评论表(Comment)实体类
 *
 * @author tang
 * @since 2022-03-20 21:34:00
 */
@Data
@ApiModel(value = "tao_comment 实体类",description = "tao_comment 实体类")
@TableName("tao_comment")
public class Comment implements Serializable {
    private static final long serialVersionUID = 892047358218752550L;
    
    /**
    * 主键
    */    
    @TableId(
        value = "id",
        type = IdType.ASSIGN_ID
    )
    @ApiModelProperty(value = "主键")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    
    /**
    * 消息
    */    
    @TableField("content")
    @ApiModelProperty(value = "消息")
    private String content;
    
    /**
    * 创建人
    */    
    @TableField("create_user")
    @ApiModelProperty(value = "创建人")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long createUser;
    
    /**
    * 创建时间
    */    
    @TableField("create_time")
    @ApiModelProperty(value = "创建时间")
    @DateTimeFormat(
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            locale = "zh", timezone="GMT+8"
    )
    private Date createTime;
    
    /**
    * 是否已删除
    */    
    @TableField("is_deleted")
    @ApiModelProperty(value = "是否已删除")
    private Integer isDeleted;
    

     
}
