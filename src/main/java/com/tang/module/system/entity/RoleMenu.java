package com.tang.module.system.entity;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;

/**
 * 角色菜单关联表(RoleMenu)实体类
 *
 * @author tang
 * @since 2022-01-20 17:29:39
 */
@Data
@ApiModel(value = "tao_role_menu 实体类",description = "tao_role_menu 实体类")
@TableName("tao_role_menu")
public class RoleMenu implements Serializable {
    private static final long serialVersionUID = -57265926650087990L;
    
    /**
    * 主键
    */    
    @TableId(
        value = "id",
        type = IdType.ASSIGN_ID
    )
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "主键")
    private Long id;
    
    /**
    * 菜单id
    */    
    @TableField("menu_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "菜单id")
    private Long menuId;
    
    /**
    * 角色id
    */    
    @TableField("role_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "角色id")
    private Long roleId;
    

     
}
