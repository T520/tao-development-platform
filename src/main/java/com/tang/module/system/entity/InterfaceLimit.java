package com.tang.module.system.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.SqlCondition;
import com.tang.common.resources.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.EqualsAndHashCode;

/**
 * 接口访问次数
(InterfaceLimit)实体类
 *
 * @author tang
 * @since 2022-01-21 16:28:34
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "tao_interface_limit 实体类",description = "tao_interface_limit 实体类")
@TableName("tao_interface_limit")
public class InterfaceLimit extends BaseEntity {
    private static final long serialVersionUID = -73655433321761813L;
    
    /**
    * 接口类型
    */    
    @TableField("interface_type")
    @ApiModelProperty(value = "接口类型")
    private String interfaceType;
    
    /**
    * 用户账户
    */    
    @TableField(value = "user_account",condition = SqlCondition.LIKE)
    @ApiModelProperty(value = "用户账户")
    private String userAccount;
    
    /**
    * 关联用户id
    */    
    @TableField("user_id")
    @ApiModelProperty(value = "关联用户id")
    private Long userId;
    
    /**
    * 关联部门id
    */    
    @TableField("dept_id")
    @ApiModelProperty(value = "关联部门id")
    private Long deptId;
    
    /**
    * 关联岗位id
    */    
    @TableField("post_id")
    @ApiModelProperty(value = "关联岗位id")
    private Long postId;
    
    /**
    * 剩余访问次数
    */    
    @TableField("remaining_times")
    @ApiModelProperty(value = "剩余访问次数")
    private Long remainingTimes;
    
    /**
    * 已访问次数
    */    
    @TableField("to_access")
    @ApiModelProperty(value = "已访问次数")
    private Long toAccess;
    

     
}
