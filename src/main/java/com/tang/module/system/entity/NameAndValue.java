package com.tang.module.system.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 键值对实体
 *
 * @author tang
 * @since 2022-07-05 17:35:52
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NameAndValue {

        /**
        *  名称
        */
        private String name;

        /**
        * 值
        */
        private Long value;
}