package com.tang.module.system.vo;

import com.tang.module.system.entity.LogInterface;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;



/**
 * 接口日志表(LogInterface)vo对象实体对象
 *
 * @author tang
 * @since 2022-01-21 14:04:09
 */
@ApiModel(value = "LogInterfaceVo对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class LogInterfaceVo extends LogInterface {
    
}
