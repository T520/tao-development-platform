package com.tang.module.system.vo;

import com.tang.module.system.entity.Dict;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;



/**
 * 字典表(Dict)vo对象实体对象
 *
 * @author tang
 * @since 2022-01-20 11:49:23
 */
@ApiModel(value = "DictVo对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class DictVo extends Dict {
    
}
