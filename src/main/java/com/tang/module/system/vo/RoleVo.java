package com.tang.module.system.vo;

import com.tang.module.system.entity.Role;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;



/**
 * 角色表(Role)vo对象实体对象
 *
 * @author tang
 * @since 2022-01-13 10:31:42
 */
@ApiModel(value = "RoleVo对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class RoleVo extends Role {
    
}
