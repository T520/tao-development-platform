package com.tang.module.system.vo;

import com.tang.module.system.entity.LogError;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;



/**
 * 错误日志表(LogError)vo对象实体对象
 *
 * @author tang
 * @since 2022-01-20 10:34:00
 */
@ApiModel(value = "LogErrorVo对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class LogErrorVo extends LogError {
    
}
