package com.tang.module.system.vo;

import com.tang.module.system.entity.InterfaceConfiguration;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Data;



/**
 * 业务接口配置表(InterfaceConfiguration)vo对象实体对象
 *
 * @author tang
 * @since 2022-01-21 12:59:21
 */
@ApiModel(value = "InterfaceConfigurationVo对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class InterfaceConfigurationVo extends InterfaceConfiguration {

    @ApiModelProperty(value = "接口类型名称")
    private String interfaceTypeName;

    @ApiModelProperty(value = "请求方式名称")
    private String methodName;
    
}
