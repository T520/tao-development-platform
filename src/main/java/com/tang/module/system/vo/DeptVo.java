package com.tang.module.system.vo;

import com.tang.module.system.entity.Dept;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Data;



/**
 * 部门表(Dept)vo对象实体对象
 *
 * @author tang
 * @since 2022-01-14 16:14:56
 */
@ApiModel(value = "DeptVo对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class DeptVo extends Dept {

    @ApiModelProperty(value = "部门类型名称")
    private String deptCategoryName;
    
}
