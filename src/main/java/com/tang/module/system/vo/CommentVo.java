package com.tang.module.system.vo;

import com.tang.module.system.entity.Comment;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Data;



/**
 * 评论表(Comment)vo对象实体对象
 *
 * @author tang
 * @since 2022-03-20 21:34:00
 */
@ApiModel(value = "CommentVo对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class CommentVo extends Comment {

    /**
     * 创建人姓名
     */
    @ApiModelProperty("创建人姓名")
    private String createUserName;

    /**
     * 创建人头像url
     */
    @ApiModelProperty("创建人头像url")
    private String avatarLink;

}
