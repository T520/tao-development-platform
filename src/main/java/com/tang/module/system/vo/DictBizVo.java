package com.tang.module.system.vo;

import com.tang.module.system.entity.DictBiz;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;



/**
 * 业务字典表(DictBiz)vo对象实体对象
 *
 * @author tang
 * @since 2022-01-20 11:49:23
 */
@ApiModel(value = "DictBizVo对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class DictBizVo extends DictBiz {
    
}
