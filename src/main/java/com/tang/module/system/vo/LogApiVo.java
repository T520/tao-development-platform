package com.tang.module.system.vo;

import com.tang.module.system.entity.LogApi;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Data;



/**
 * 接口日志表(LogApi)vo对象实体对象
 *
 * @author tang
 * @since 2022-01-18 18:08:15
 */
@ApiModel(value = "LogApiVo对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class LogApiVo extends LogApi {


    /**
     * 日志类型名称
     */
    @ApiModelProperty(value = "日志类型名称")
    private String typeName;

    
}
