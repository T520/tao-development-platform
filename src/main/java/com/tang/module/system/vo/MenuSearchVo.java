package com.tang.module.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 菜单搜索Vo实体，用于前端展示菜单搜索结果
 * @author tang
 * @date 2022/4/20 16:24
 */
@Data
@ApiModel(value = "菜单搜索Vo实体")
public class MenuSearchVo {

    /**
     * 命中菜单路由
     */
    @ApiModelProperty("命中菜单路由")
    private String path;

    /**
     * 命中菜单的路径
     * 例如：系统管理，菜单管路
     */
    @ApiModelProperty(value = "命中菜单的路径")
    private List<String> pathList;


}
