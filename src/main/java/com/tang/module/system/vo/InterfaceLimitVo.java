package com.tang.module.system.vo;

import com.tang.module.system.entity.InterfaceLimit;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Data;



/**
 * 接口访问次数
(InterfaceLimit)vo对象实体对象
 *
 * @author tang
 * @since 2022-01-21 16:28:34
 */
@ApiModel(value = "InterfaceLimitVo对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class InterfaceLimitVo extends InterfaceLimit {

    @ApiModelProperty(value = "接口类型名称")
    private String interfaceTypeName;

}
