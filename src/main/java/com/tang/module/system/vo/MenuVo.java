package com.tang.module.system.vo;

import com.tang.module.system.entity.Menu;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Data;

import java.util.List;


/**
 * 菜单表(Menu)vo对象实体对象
 *
 * @author tang
 * @since 2022-01-20 17:29:39
 */
@ApiModel(value = "MenuVo对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class MenuVo extends Menu {

    @ApiModelProperty(value = "操作按钮类型名称")
    private String actionName;

    @ApiModelProperty(value = "菜单类型名称")
    private String categoryName;

    @ApiModelProperty(value = "vo子菜单")
    private List<MenuVo> childrenVo;

    
}
