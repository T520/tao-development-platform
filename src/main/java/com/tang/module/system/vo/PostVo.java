package com.tang.module.system.vo;

import com.tang.module.system.entity.Post;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;



/**
 * 岗位表(Post)vo对象实体对象
 *
 * @author tang
 * @since 2022-01-14 16:14:56
 */
@ApiModel(value = "PostVo对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class PostVo extends Post {
    
}
