package com.tang.module.system.vo;

import com.tang.module.system.entity.RoleScope;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;



/**
 * 角色数据权限关联表(RoleScope)vo对象实体对象
 *
 * @author tang
 * @since 2022-01-13 11:38:44
 */
@ApiModel(value = "RoleScopeVo对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class RoleScopeVo extends RoleScope {
    
}
