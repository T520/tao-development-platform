package com.tang.module.system.vo;

import com.tang.module.system.entity.RoleMenu;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;



/**
 * 角色菜单关联表(RoleMenu)vo对象实体对象
 *
 * @author tang
 * @since 2022-01-20 17:29:39
 */
@ApiModel(value = "RoleMenuVo对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class RoleMenuVo extends RoleMenu {
    
}
