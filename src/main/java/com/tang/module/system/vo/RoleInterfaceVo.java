package com.tang.module.system.vo;

import com.tang.module.system.entity.RoleInterface;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;



/**
 * 角色数据权限关联表(RoleInterface)vo对象实体对象
 *
 * @author tang
 * @since 2022-03-18 12:11:56
 */
@ApiModel(value = "RoleInterfaceVo对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class RoleInterfaceVo extends RoleInterface {
    
}
