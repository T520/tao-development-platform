package com.tang.module.system.vo;

import com.tang.module.system.entity.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Data;

import java.util.List;


/**
 * 用户表(User)vo对象实体对象
 *
 * @author tang
 * @since 2022-01-12 18:04:55
 */
@ApiModel(value = "UserVo对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class UserVo extends User {

    /**
     * 操作IP
     */
    @ApiModelProperty(value = "操作IP")
    private String ip;

    /**
     * 操作地址
     */
    @ApiModelProperty(value = "操作地址")
    private String address;

    /**
     * 操作运营商
     */
    @ApiModelProperty(value = "操作运营商")
    private String isp;

    /**
     * 角色名称集合
     */
    @ApiModelProperty(value = "角色名称集合")
    private List<String> ruleNameList;

    /**
     * 岗位名称集合
     */
    @ApiModelProperty(value = "岗位名称集合")
    private List<String> postNameList;

    /**
     * 部门名称集合
     */
    @ApiModelProperty(value = "部门名称集合")
    private List<String> deptNameList;

    
}
