package com.tang.module.system.vo;

import com.tang.module.system.entity.Param;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;



/**
 * 参数表(Param)vo对象实体对象
 *
 * @author tang
 * @since 2022-01-13 12:47:13
 */
@ApiModel(value = "ParamVo对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class ParamVo extends Param {
    
}
