package com.tang.module.system.wrapper;

import com.tang.common.constant.DictConstant;
import com.tang.common.resources.BaseEntityWrapper;
import com.tang.common.utils.cache.DictCache;
import com.tang.module.system.entity.Dept;
import com.tang.module.system.vo.DeptVo;
import org.springframework.beans.BeanUtils;

/**
 * 部门表(Dept)数据库转换层
 *
 * @author tang
 * @since 2022-01-14 16:14:56
 */
public class DeptWrapper extends BaseEntityWrapper<Dept,DeptVo> {

    public static  DeptWrapper build(){
        return new DeptWrapper();
    }


    @Override
    public  DeptVo wrapper(Dept dept){
      DeptVo deptVo = new DeptVo();
      //属性赋值
      BeanUtils.copyProperties(dept,deptVo);
      //设置类型名称
      deptVo.setDeptCategoryName(DictCache.getValue(DictConstant.DEPT_TYPE,deptVo.getDeptCategory()));
      return deptVo;
    }
}
