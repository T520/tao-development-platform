package com.tang.module.system.wrapper;


import com.tang.module.system.entity.LogError;
import com.tang.module.system.vo.LogErrorVo;
import org.springframework.beans.BeanUtils;

/**
 * 错误日志表(LogError)数据库转换层
 *
 * @author tang
 * @since 2022-01-20 10:34:00
 */
public class LogErrorWrapper {
    
    public static LogErrorVo wrapper(LogError logError){
      LogErrorVo logErrorVo = new LogErrorVo();
      //属性赋值
      BeanUtils.copyProperties(logError,logErrorVo);
      return logErrorVo;
    }
}
