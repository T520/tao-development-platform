package com.tang.module.system.wrapper;



import com.tang.module.system.entity.LogInterface;
import com.tang.module.system.vo.LogInterfaceVo;
import org.springframework.beans.BeanUtils;

/**
 * 接口日志表(LogInterface)数据库转换层
 *
 * @author tang
 * @since 2022-01-21 14:04:09
 */
public class LogInterfaceWrapper {
    
    public static LogInterfaceVo wrapper(LogInterface logInterface){
      LogInterfaceVo logInterfaceVo = new LogInterfaceVo();
      //属性赋值
      BeanUtils.copyProperties(logInterface,logInterfaceVo);
      return logInterfaceVo;
    }
}
