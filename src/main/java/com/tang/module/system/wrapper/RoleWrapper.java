package com.tang.module.system.wrapper;

import com.tang.module.system.entity.Role;
import com.tang.module.system.vo.RoleVo;
import org.springframework.beans.BeanUtils;

/**
 * 角色表(Role)数据库转换层
 *
 * @author tang
 * @since 2022-01-13 10:31:42
 */
public class RoleWrapper {
    
    public static RoleVo wrapper(Role role){
      RoleVo roleVo = new RoleVo();
      //属性赋值
      BeanUtils.copyProperties(role,roleVo);
      return roleVo;
    }
}
