package com.tang.module.system.wrapper;

import com.tang.module.system.entity.RoleScope;
import com.tang.module.system.vo.RoleScopeVo;
import org.springframework.beans.BeanUtils;

/**
 * 角色数据权限关联表(RoleScope)数据库转换层
 *
 * @author tang
 * @since 2022-01-13 11:38:44
 */
public class RoleScopeWrapper {
    
    public static RoleScopeVo wrapper(RoleScope roleScope){
      RoleScopeVo roleScopeVo = new RoleScopeVo();
      //属性赋值
      BeanUtils.copyProperties(roleScope,roleScopeVo);
      return roleScopeVo;
    }
}
