package com.tang.module.system.wrapper;



import com.tang.common.constant.DictConstant;
import com.tang.common.resources.BaseEntityWrapper;
import com.tang.common.utils.cache.DictCache;
import com.tang.module.system.entity.InterfaceLimit;
import com.tang.module.system.vo.InterfaceLimitVo;
import org.springframework.beans.BeanUtils;

/**
 * 接口访问次数
(InterfaceLimit)数据库转换层
 *
 * @author tang
 * @since 2022-01-21 16:28:34
 */
public class InterfaceLimitWrapper extends BaseEntityWrapper<InterfaceLimit,InterfaceLimitVo> {


    public static InterfaceLimitWrapper build(){
        return new InterfaceLimitWrapper();
    }


    @Override
    public  InterfaceLimitVo wrapper(InterfaceLimit interfaceLimit){
      InterfaceLimitVo interfaceLimitVo = new InterfaceLimitVo();
      //属性赋值
      BeanUtils.copyProperties(interfaceLimit,interfaceLimitVo);
      interfaceLimitVo.setInterfaceTypeName(DictCache.getValue(DictConstant.INTERFACE_TYPE, Integer.valueOf(interfaceLimitVo.getInterfaceType())));
      return interfaceLimitVo;
    }
}
