package com.tang.module.system.wrapper;



import com.tang.common.utils.cache.UserCache;
import com.tang.module.system.entity.Comment;
import com.tang.common.resources.BaseEntityWrapper;
import com.tang.module.system.vo.CommentVo;
import org.springframework.beans.BeanUtils;

/**
 * 评论表(Comment)数据库转换层
 *
 * @author tang
 * @since 2022-03-20 21:34:00
 */
public class CommentWrapper extends BaseEntityWrapper<Comment,CommentVo> {
    
    public static  CommentWrapper build(){
        return new CommentWrapper();
    }

    @Override
    public  CommentVo wrapper(Comment comment){
      CommentVo commentVo = new CommentVo();
      //属性赋值
      BeanUtils.copyProperties(comment,commentVo);
      commentVo.setCreateUserName(UserCache.getUserName(comment.getCreateUser()));
      return commentVo;
    }
}
