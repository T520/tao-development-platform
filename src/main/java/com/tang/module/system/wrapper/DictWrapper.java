package com.tang.module.system.wrapper;

import com.tang.module.system.entity.Dict;
import com.tang.module.system.vo.DictVo;
import org.springframework.beans.BeanUtils;

/**
 * 字典表(Dict)数据库转换层
 *
 * @author tang
 * @since 2022-01-20 11:49:23
 */
public class DictWrapper {
    
    public static DictVo wrapper(Dict dict){
      DictVo dictVo = new DictVo();
      //属性赋值
      BeanUtils.copyProperties(dict,dictVo);
      return dictVo;
    }
}
