package com.tang.module.system.wrapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tang.common.resources.BaseEntityWrapper;
import com.tang.common.utils.cache.UserCache;
import com.tang.module.system.entity.User;
import com.tang.module.system.mapper.UserMapper;
import com.tang.module.system.vo.UserVo;
import org.springframework.beans.BeanUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 用户表(User)数据库转换层
 *
 * @author tang
 * @since 2022-01-12 18:04:55
 */
public class UserWrapper extends BaseEntityWrapper<User,UserVo> {


    public static UserWrapper build() {
        return new UserWrapper();
    }

    @Override
    public  UserVo wrapper(User user){
      UserVo userVo = new UserVo();
      //属性赋值
      BeanUtils.copyProperties(user,userVo);
      List<String> roleList = new ArrayList<>();
      List<String> deptList = new ArrayList<>();
      List<String> postList = new ArrayList<>();
      //填充角色
      if (! StringUtils.isEmpty(userVo.getRoleId())){
          String[] strings = userVo.getRoleId().split(",");
          for (String string : strings) {
              roleList.add(UserCache.getRoleName(Long.valueOf(string)));
          }
      }
      //填充部门
      if (! StringUtils.isEmpty(userVo.getDeptId())){
          String[] strings = userVo.getDeptId().split(",");
          for (String string : strings) {
              deptList.add(UserCache.getDeptName(Long.valueOf(string)));
          }
      }
      //添加岗位
      if (! StringUtils.isEmpty(userVo.getPostId())){
          String[] strings = userVo.getPostId().split(",");
          for (String string : strings) {
              postList.add(UserCache.getPostName(Long.valueOf(string)));
          }
      }
      userVo.setRuleNameList(roleList);
      userVo.setDeptNameList(deptList);
      userVo.setPostNameList(postList);
      return userVo;
    }

}
