package com.tang.module.system.wrapper;




import com.tang.module.system.entity.RoleMenu;
import com.tang.module.system.vo.RoleMenuVo;
import org.springframework.beans.BeanUtils;

/**
 * 角色菜单关联表(RoleMenu)数据库转换层
 *
 * @author tang
 * @since 2022-01-20 17:29:39
 */
public class RoleMenuWrapper {
    
    public static RoleMenuVo wrapper(RoleMenu roleMenu){
      RoleMenuVo roleMenuVo = new RoleMenuVo();
      //属性赋值
      BeanUtils.copyProperties(roleMenu,roleMenuVo);
      return roleMenuVo;
    }
}
