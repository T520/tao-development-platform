package com.tang.module.system.wrapper;

import com.tang.module.system.entity.DictBiz;
import com.tang.module.system.vo.DictBizVo;
import org.springframework.beans.BeanUtils;

/**
 * 业务字典表(DictBiz)数据库转换层
 *
 * @author tang
 * @since 2022-01-20 11:49:23
 */
public class DictBizWrapper {
    
    public static DictBizVo wrapper(DictBiz dictBiz){
      DictBizVo dictBizVo = new DictBizVo();
      //属性赋值
      BeanUtils.copyProperties(dictBiz,dictBizVo);
      return dictBizVo;
    }
}
