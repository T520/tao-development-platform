package com.tang.module.system.wrapper;

import com.tang.common.constant.DictConstant;
import com.tang.common.resources.BaseEntityWrapper;
import com.tang.common.utils.cache.DictCache;
import com.tang.module.system.entity.InterfaceConfiguration;
import com.tang.module.system.vo.InterfaceConfigurationVo;
import org.springframework.beans.BeanUtils;

/**
 * 业务接口配置表(InterfaceConfiguration)数据库转换层
 *
 * @author tang
 * @since 2022-01-21 12:59:21
 */
public class InterfaceConfigurationWrapper extends BaseEntityWrapper<InterfaceConfiguration,InterfaceConfigurationVo> {


    public static InterfaceConfigurationWrapper build(){
        return new InterfaceConfigurationWrapper();
    }


    @Override
    public  InterfaceConfigurationVo wrapper(InterfaceConfiguration interfaceConfiguration){
      InterfaceConfigurationVo interfaceConfigurationVo = new InterfaceConfigurationVo();
      //属性赋值
      BeanUtils.copyProperties(interfaceConfiguration,interfaceConfigurationVo);
      interfaceConfigurationVo.setInterfaceTypeName(DictCache.getValue(DictConstant.INTERFACE_TYPE, Math.toIntExact(interfaceConfiguration.getInterfaceType())));
      interfaceConfigurationVo.setMethodName(DictCache.getValue(DictConstant.HTTP_TYPE, Math.toIntExact(interfaceConfiguration.getMethod())));
      return interfaceConfigurationVo;
    }
}
