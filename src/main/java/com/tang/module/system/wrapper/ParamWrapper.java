package com.tang.module.system.wrapper;

import com.tang.module.system.entity.Param;
import com.tang.module.system.vo.ParamVo;
import org.springframework.beans.BeanUtils;

/**
 * 参数表(Param)数据库转换层
 *
 * @author tang
 * @since 2022-01-13 12:47:13
 */
public class ParamWrapper {
    
    public static ParamVo wrapper(Param param){
      ParamVo paramVo = new ParamVo();
      //属性赋值
      BeanUtils.copyProperties(param,paramVo);
      return paramVo;
    }
}
