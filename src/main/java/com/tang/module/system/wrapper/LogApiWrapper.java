package com.tang.module.system.wrapper;


import com.tang.common.constant.DictConstant;
import com.tang.common.resources.BaseEntityWrapper;
import com.tang.common.utils.cache.DictCache;
import com.tang.module.system.entity.LogApi;
import com.tang.module.system.vo.LogApiVo;
import org.springframework.beans.BeanUtils;

/**
 * 接口日志表(LogApi)数据库转换层
 *
 * @author tang
 * @since 2022-01-18 18:08:15
 */
public class LogApiWrapper extends BaseEntityWrapper<LogApi,LogApiVo> {

    public static LogApiWrapper build() {
        return new LogApiWrapper();
    }


    @Override
    public  LogApiVo wrapper(LogApi logApi){
      LogApiVo logApiVo = new LogApiVo();
      //属性赋值
      BeanUtils.copyProperties(logApi,logApiVo);
      //设置日志类型名称
      logApiVo.setTypeName(DictCache.getValue(DictConstant.LOG_TYPE, Integer.valueOf(logApiVo.getType())));
      return logApiVo;
    }
}
