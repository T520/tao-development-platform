package com.tang.module.system.wrapper;

import com.tang.module.system.entity.Post;
import com.tang.module.system.vo.PostVo;
import org.springframework.beans.BeanUtils;

/**
 * 岗位表(Post)数据库转换层
 *
 * @author tang
 * @since 2022-01-14 16:14:56
 */
public class PostWrapper {
    
    public static PostVo wrapper(Post post){
      PostVo postVo = new PostVo();
      //属性赋值
      BeanUtils.copyProperties(post,postVo);
      return postVo;
    }
}
