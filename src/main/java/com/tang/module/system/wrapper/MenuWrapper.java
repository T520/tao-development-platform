package com.tang.module.system.wrapper;




import com.tang.common.constant.DictConstant;
import com.tang.common.resources.BaseEntityWrapper;
import com.tang.common.utils.cache.DictCache;
import com.tang.module.system.entity.Menu;
import com.tang.module.system.vo.MenuVo;
import org.springframework.beans.BeanUtils;

/**
 * 菜单表(Menu)数据库转换层
 *
 * @author tang
 * @since 2022-01-20 17:29:39
 */
public class MenuWrapper extends BaseEntityWrapper<Menu, MenuVo> {

    public static MenuWrapper build(){
        return new MenuWrapper();
    }


    @Override
    public  MenuVo wrapper(Menu menu){
      MenuVo menuVo = new MenuVo();
      //属性赋值
      BeanUtils.copyProperties(menu,menuVo);
      menuVo.setActionName(menuVo.getAction() == 0 ? "" : DictCache.getValue(DictConstant.BUTTON_TYPE,menuVo.getAction()));
      menuVo.setCategoryName(DictCache.getValue(DictConstant.MENU_TYPE,menu.getCategory()));
      return menuVo;
    }
}
