package com.tang.module.system.wrapper;



import com.tang.module.system.entity.RoleInterface;
import com.tang.common.resources.BaseEntityWrapper;
import com.tang.module.system.vo.RoleInterfaceVo;
import org.springframework.beans.BeanUtils;

/**
 * 角色数据权限关联表(RoleInterface)数据库转换层
 *
 * @author tang
 * @since 2022-03-18 12:11:56
 */
public class RoleInterfaceWrapper extends BaseEntityWrapper<RoleInterface,RoleInterfaceVo> {
    
    public static  RoleInterfaceWrapper build(){
        return new RoleInterfaceWrapper();
    }
    
    @Override
    public  RoleInterfaceVo wrapper(RoleInterface roleInterface){
      RoleInterfaceVo roleInterfaceVo = new RoleInterfaceVo();
      //属性赋值
      BeanUtils.copyProperties(roleInterface,roleInterfaceVo);
      return roleInterfaceVo;
    }
}
