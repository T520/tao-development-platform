package com.tang.module.system.controller;



import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tang.common.resources.Result;
import com.tang.common.resources.TaoPage;
import com.tang.module.system.entity.LogInterface;
import com.tang.module.system.service.LogInterfaceService;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import javax.validation.Valid;
import io.swagger.annotations.ApiOperation;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * 业务接口日志表(LogInterface)控制层
 *
 * @author tang
 * @since 2022-01-21 14:04:09
 */
@RestController
@RequestMapping("logInterface")
@Api(tags = "业务接口日志控制层")
public class LogInterfaceController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private LogInterfaceService logInterfaceService;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @param logInterface 查询实体
     * @return 所有数据
     */
    @GetMapping()
    @ApiOperation(value = "分页查询")
    public Result<Page<LogInterface>> selectAll(@Valid TaoPage page, LogInterface logInterface) {
        return Result.data(this.logInterfaceService.page(new Page<>(page.getCurrent(), page.getSize()), new QueryWrapper<>(logInterface)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("one")
    @ApiOperation(value = "主键查询")
    public Result<LogInterface> selectOne(Serializable id) {
        return Result.data(this.logInterfaceService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param logInterface 实体对象
     * @return 新增结果
     */
    @PostMapping
    @ApiOperation(value = "新增")
    public Result<Boolean> insert(@RequestBody LogInterface logInterface) {
        return Result.status(this.logInterfaceService.save(logInterface));
    }

    /**
     * 修改数据
     *
     * @param logInterface 实体对象
     * @return 修改结果
     */
    @PostMapping("updata")
    @ApiOperation(value = "修改")
    public Result<Boolean> update(@RequestBody LogInterface logInterface) {
        return Result.status(this.logInterfaceService.updateById(logInterface));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @GetMapping("delete")
    @ApiOperation(value = "删除")
    public Result<Boolean> delete(@RequestParam("idList") List<Long> idList) {
        return Result.status(this.logInterfaceService.removeByIds(idList));
    }
}
