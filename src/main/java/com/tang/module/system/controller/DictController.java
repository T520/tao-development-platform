package com.tang.module.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tang.common.annotation.ApiLog;
import com.tang.common.utils.CommonUtils;
import com.tang.common.utils.ExportExcelUtils;
import com.tang.common.resources.Result;
import com.tang.common.resources.TaoPage;
import com.tang.module.system.entity.Dict;
import com.tang.module.system.service.DictService;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import io.swagger.annotations.ApiOperation;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 字典表(Dict)控制层
 *
 * @author tang
 * @since 2022-01-20 11:49:22
 */
@RestController
@RequestMapping("dict")
@Api(tags = "系统字典控制层")
public class DictController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private DictService dictService;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @param dict 查询实体
     * @return 所有数据
     */
    @Secured({"ROLE_superAdmin"})
    @GetMapping()
    @ApiLog(type = 5,title = "分页查询")
    @ApiOperation(value = "分页查询")
    public Result<Page<Dict>> selectAll(@Valid TaoPage page, Dict dict) {
        return Result.data(this.dictService.page(new Page<>(page.getCurrent(), page.getSize()), new QueryWrapper<>(dict).orderByAsc("sort")));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @ApiLog(type = 5,title = "主键查询")
    @GetMapping("one")
    @ApiOperation(value = "主键查询")
    public Result<Dict> selectOne(Serializable id) {
        return Result.data(this.dictService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param dict 实体对象
     * @return 新增结果
     */
    @ApiLog(type = 5,title = "新增")
    @PostMapping
    @ApiOperation(value = "新增")
    public Result<Boolean> insert(@RequestBody Dict dict) {
        return Result.status(this.dictService.save(dict));
    }

    /**
     * 修改数据
     *
     * @param dict 实体对象
     * @return 修改结果
     */
    @ApiLog(type = 5,title = "修改")
    @PostMapping("update")
    @ApiOperation(value = "修改")
    public Result<Boolean> update(@RequestBody Dict dict) {
        return Result.status(this.dictService.updateById(dict));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @ApiLog(type = 5,title = "删除")
    @DeleteMapping("delete")
    @ApiOperation(value = "删除")
    public Result<Boolean> delete(@RequestParam("idList") List<Long> idList) {
        return Result.status(this.dictService.removeByIds(idList));
    }

    /**
     * 获取全部顶级字典
     * @return 顶级字典列表
     */
    @ApiLog(type = 5,title = "顶级字典")
    @GetMapping("top")
    @ApiOperation(value = "顶级字典")
    public Result<List<Dict>> selectTop(){
        return Result.data(this.dictService.list(Wrappers.<Dict>lambdaQuery().eq(Dict::getParentId,0).orderByAsc(Dict::getSort)));
    }

    /**
     * 获取字典树
     * @return 顶级字典列表
     */
    @ApiLog(type = 5,title = "顶级字典")
    @GetMapping("tree")
    @ApiOperation(value = "顶级字典")
    public Result<Page<Dict>> tree(@Valid TaoPage taoPage){
        return Result.data(CommonUtils.pageUtli(this.dictService.tree(), taoPage.getCurrent(), taoPage.getSize()));
    }

    /**
     * 获取字典类别
     * @param codeList 字典编码,格式：1,2,3
     * @return 字典列表（去除key为0的值）
     */
    @ApiLog(type = 5,title = "字典列表")
    @GetMapping("list")
    @ApiOperation(value = "字典列表")
    public Result<Map<String, List<Dict>>> list(@RequestParam(value = "codeList") List<String> codeList){
        return Result.data(this.dictService.list(codeList));
    }

    /**
     * excel导出
     */
    @ApiLog(type = 2,title = "excel导出")
    @ApiOperation("excel导出")
    @GetMapping("/importExcel")
    public void excel(HttpServletResponse response) throws IOException {
        ExportExcelUtils.entityExcel(dictService.list(),"系统字典参数.xlsx",response);
    }

}
