package com.tang.module.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tang.common.annotation.ApiLog;
import com.tang.common.utils.ExportExcelUtils;
import com.tang.common.resources.Result;
import com.tang.common.resources.TaoPage;
import com.tang.module.system.entity.Post;
import com.tang.module.system.service.PostService;
import org.springframework.web.bind.annotation.*;;
import io.swagger.annotations.Api;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import io.swagger.annotations.ApiOperation;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * 岗位表(Post)控制层
 *
 * @author tang
 * @since 2022-01-14 16:14:56
 */
@RestController
@RequestMapping("post")
@Api(tags = "岗位控制层")
public class PostController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private PostService postService;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @param post 查询实体
     * @return 所有数据
     */
    @ApiLog(type = 3,title = "分页查询")
    @GetMapping()
    @ApiOperation(value = "分页查询")
    public Result<Page<Post>> selectAll(@Valid TaoPage page, Post post) {
        return Result.data(this.postService.page(new Page<>(page.getCurrent(), page.getSize()), new QueryWrapper<>(post)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @ApiLog(type = 3,title = "主键查询")
    @GetMapping("one")
    @ApiOperation(value = "主键查询")
    public Result<Post> selectOne(Serializable id) {
        return Result.data(this.postService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param post 实体对象
     * @return 新增结果
     */
    @ApiLog(type = 3,title = "新增")
    @PostMapping
    @ApiOperation(value = "新增")
    public Result<Boolean> insert(@RequestBody Post post) {
       //CommonUtils.baseEntityField(post,)
        return Result.status(this.postService.save(post));
    }

    /**
     * 修改数据
     *
     * @param post 实体对象
     * @return 修改结果
     */
    @ApiLog(type = 3,title = "修改")
    @PostMapping("update")
    @ApiOperation(value = "修改")
    public Result<Boolean> update(@RequestBody Post post) {
        return Result.status(this.postService.updateById(post));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @ApiLog(type = 3,title = "删除")
    @DeleteMapping("delete")
    @ApiOperation(value = "删除")
    public Result<Boolean> delete(@RequestParam("idList") List<Long> idList) {
        return Result.status(this.postService.removeByIds(idList));
    }

    /**
     * 查询全部岗位名称和id
     * @return 所有数据
     */
    @ApiLog(type = 3,title = "岗位名称和id")
    @GetMapping("nameOrId")
    @ApiOperation(value = "岗位名称和id")
    public Result<List<Post>> selectAllId() {
        return Result.data(postService.list(Wrappers.<Post>lambdaQuery().select(Post::getPostName,Post::getId)));
    }

    /**
     * excel导出
     */
    @ApiLog(type = 3,title = "excel导出")
    @ApiOperation("excel导出")
    @GetMapping("/importExcel")
    public void excel(HttpServletResponse response) throws IOException {
        ExportExcelUtils.entityExcel(postService.list(),"部门.xlsx",response);
    }
}
