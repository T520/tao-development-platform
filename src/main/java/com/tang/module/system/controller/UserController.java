package com.tang.module.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tang.common.annotation.ApiLog;
import com.tang.common.utils.ExportExcelUtils;
import com.tang.common.resources.Result;
import com.tang.common.resources.TaoPage;
import com.tang.module.system.entity.User;
import com.tang.module.system.search.UserSearch;
import com.tang.module.system.service.UserService;
import com.tang.module.system.vo.UserVo;
import com.tang.module.system.wrapper.UserWrapper;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * 用户表(User)控制层
 *
 * @author tang
 * @since 2022-01-12 18:04:54
 */
@RestController
@Api(tags = "用户控制层")
@RequestMapping("user")
@AllArgsConstructor
public class UserController extends ApiController {

    /**
     * 服务对象
     */
    private  final  UserService userService;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @param user 查询实体
     * @return 分页数据
     */
    @ApiLog(type = 7,title = "分页查询")
    @GetMapping()
    @ApiOperation(value = "分页查询")
    public Result<IPage<UserVo>> selectAll(@Valid TaoPage page, UserSearch user) {
        Page<User> userPage = this.userService.page(new Page<>(page.getCurrent(), page.getSize()), new QueryWrapper<>(user));
        return Result.data(UserWrapper.build().pageWrapper(userPage));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @ApiLog(type = 7,title = "主键查询")
    @GetMapping("one")
    @ApiOperation(value = "主键查询")
    public Result<UserVo> selectOne(@RequestParam("id") Serializable id) {
        return Result.data(UserWrapper.build().wrapper(this.userService.getById(id)));
    }

    /**
     * 新增数据
     *
     * @param user 实体对象
     * @return 新增结果
     */
    @ApiLog(type = 7,title = "新增")
    @PostMapping
    @ApiOperation(value = "新增")
    public Result<Boolean> insert(@RequestBody User user) {
        return Result.status(this.userService.save(user));
    }

    /**
     * 修改数据
     *
     * @param user 实体对象
     * @return 修改结果
     */
    @ApiLog(type = 7,title = "修改")
    @PostMapping("update")
    @ApiOperation(value = "修改")
    public Result<Boolean> update(@RequestBody User user) {
        return Result.status(this.userService.updateById(user));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @ApiLog(type = 7,title = "删除")
    @DeleteMapping("delete")
    @ApiOperation(value = "删除")
    public Result<Boolean> delete(@RequestParam("idList") List<Long> idList) {
        return Result.status(this.userService.removeByIds(idList));
    }

    /**
     * 用户修改密码
     * @param oldPassword 老密码
     * @param newPassword 新密码
     * @return
     */
    @ApiLog(type = 7,title = "修改密码")
    @ApiOperation("修改密码")
    @PostMapping("/changePassword")
    public Result<Boolean> changePassword(@RequestParam(value = "oldPassword") String oldPassword,
                                           @RequestParam(value ="newPassword") String newPassword){
        return Result.status(this.userService.updatePassword(oldPassword,newPassword));
    }



    /**
     * 重置密码
     * @param id 用户id
     * @param newPassword 新密码
     * @return 是否成功
     */
    @ApiLog(type = 7,title = "重置密码")
    @ApiOperation("重置密码")
    @PostMapping("/resetPassword")
    public Result<Boolean> resetPassword(@RequestParam(value = "id") String id,
                                          @RequestParam(value ="newPassword") String newPassword){
        return Result.status((this.userService.resetPassword(id,newPassword)));
    }

    /**
     * 头像上传
     * @param file 头像文件
     * @param id 用户id
     * @return 新头像链接
     */
    @ApiLog(type = 7,title = "头像上传")
    @ApiOperation("头像上传")
    @PostMapping("/avatarUpdate")
    public Result<String> avatarUpdate(@RequestParam("file") MultipartFile file,@RequestParam("id") Long id){
        return Result.data(userService.avatarUpdate(file, id));
    }

    /**
     * excel导出
     */
    @ApiLog(type = 7,title = "excel导出")
    @ApiOperation("excel导出")
    @GetMapping("/importExcel")
    public void excel(HttpServletResponse response) throws IOException {
        //过滤密码字段
        List<User> userList = userService.list(Wrappers.<User>lambdaQuery().
                select(User.class,i -> ! "password".equals(i.getProperty())));
        ExportExcelUtils.entityExcel(userList,"用户.xlsx",response);
    }

    /**
     * 用户名和id集合
     */
    @ApiLog(type = 7,title = "用户名和id集合")
    @ApiOperation("用户名和id集合")
    @GetMapping("/idAndName")
    public Result<List<User>> idAndName(){
       return Result.data(userService.list(Wrappers.<User>lambdaQuery().select(User::getId,User::getName).ne(User::getStatus,0)));
    }

}
