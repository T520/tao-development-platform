package com.tang.module.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tang.common.annotation.ApiLog;
import com.tang.common.utils.ExportExcelUtils;
import com.tang.common.resources.Result;
import com.tang.common.resources.TaoPage;
import com.tang.module.system.entity.Dept;
import com.tang.module.system.service.DeptService;
import com.tang.module.system.vo.DeptVo;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import io.swagger.annotations.ApiOperation;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * 部门表(Dept)控制层
 *
 * @author tang
 * @since 2022-01-14 16:14:56
 */
@RestController
@RequestMapping("dept")
@Api(tags = "部门字典控制层")
public class DeptController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private DeptService deptService;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @param dept 查询实体
     * @return 所有数据
     */
    @ApiLog(type = 3,title = "分页查询")
    @GetMapping()
    @ApiOperation(value = "分页查询")
    public Result<Page<Dept>> selectAll(@Valid TaoPage page, Dept dept) {
        return Result.data(this.deptService.page(new Page<>(page.getCurrent(), page.getSize()), new QueryWrapper<>(dept)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @ApiLog(type = 3,title = "主键查询")
    @GetMapping("one")
    @ApiOperation(value = "主键查询")
    public Result<Dept> selectOne(Serializable id) {
        return Result.data(this.deptService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param dept 实体对象
     * @return 新增结果
     */
    @ApiLog(type = 3,title = "新增")
    @PostMapping
    @ApiOperation(value = "新增")
    public Result<Boolean> insert(@RequestBody Dept dept) {
        return Result.status(this.deptService.save(dept));
    }

    /**
     * 修改数据
     *
     * @param dept 实体对象
     * @return 修改结果
     */
    @ApiLog(type = 3,title = "修改")
    @PostMapping("update")
    @ApiOperation(value = "修改")
    public Result<Boolean> update(@RequestBody Dept dept) {
        return Result.status(this.deptService.updateById(dept));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @ApiLog(type = 3,title = "删除")
    @DeleteMapping("delete")
    @ApiOperation(value = "删除")
    public Result<Boolean> delete(@RequestParam("idList") List<Long> idList) {
        return Result.status(this.deptService.removeByIds(idList));
    }

    /**
     * 查询全部部门名称和id
     * @return 所有数据
     */
    @ApiLog(type = 3,title = "部门名称和id")
    @GetMapping("nameOrId")
    @ApiOperation(value = "部门名称和id")
    public Result<List<Dept>> selectAllId() {
        return Result.data(deptService.list(Wrappers.<Dept>lambdaQuery().select(Dept::getDeptName,Dept::getId)));
    }

    /**
     * 获得部门树形结构
     * @param deptCategory 部门类型
     * @return 树形数据
     */
    @ApiLog(type = 3,title = "树形")
    @GetMapping("tree")
    @ApiOperation(value = "树形")
    public Result<List<DeptVo>> tree(@RequestParam(name = "deptCategory",required = false) Integer deptCategory) {
        return Result.data(deptService.tree(deptCategory));
    }

    /**
     * excel导出
     */
    @ApiLog(type = 3,title = "excel导出")
    @ApiOperation("excel导出")
    @GetMapping("/importExcel")
    public void excel(HttpServletResponse response) throws IOException {
        ExportExcelUtils.entityExcel(deptService.list(),"部门.xlsx",response);
    }
}
