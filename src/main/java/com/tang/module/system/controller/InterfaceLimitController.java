package com.tang.module.system.controller;



import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tang.common.annotation.ApiLog;
import com.tang.common.utils.ExportExcelUtils;
import com.tang.common.resources.Result;
import com.tang.common.resources.TaoPage;
import com.tang.module.system.entity.InterfaceLimit;
import com.tang.module.system.service.InterfaceLimitService;
import com.tang.module.system.vo.InterfaceLimitVo;
import com.tang.module.system.wrapper.InterfaceLimitWrapper;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import io.swagger.annotations.ApiOperation;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * 接口访问次数
(InterfaceLimit)控制层
 *
 * @author tang
 * @since 2022-01-21 16:28:34
 */
@RestController
@RequestMapping("interfaceLimit")
@Api(tags = "接口访问次数控制层")
public class InterfaceLimitController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private InterfaceLimitService interfaceLimitService;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @param interfaceLimit 查询实体
     * @return 所有数据
     */
    @ApiLog(type = 13,title = "分页查询")
    @GetMapping()
    @ApiOperation(value = "分页查询")
    public Result<IPage<InterfaceLimitVo>> selectAll(@Valid TaoPage page, InterfaceLimit interfaceLimit) {
        return Result.data(InterfaceLimitWrapper.build().pageWrapper(
                this.interfaceLimitService.page(new Page<>(page.getCurrent(), page.getSize()), new QueryWrapper<>(interfaceLimit))
        ));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @ApiLog(type = 13,title = "主键查询")
    @GetMapping("one")
    @ApiOperation(value = "主键查询")
    public Result<InterfaceLimit> selectOne(Serializable id) {
        return Result.data(this.interfaceLimitService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param interfaceLimit 实体对象
     * @return 新增结果
     */
    @ApiLog(type = 13,title = "新增")
    @PostMapping
    @ApiOperation(value = "新增")
    public Result<Boolean> insert(@RequestBody InterfaceLimit interfaceLimit) {
        return Result.status(this.interfaceLimitService.save(interfaceLimit));
    }

    /**
     * 修改数据
     *
     * @param interfaceLimit 实体对象
     * @return 修改结果
     */
    @ApiLog(type = 13,title = "修改")
    @PostMapping("update")
    @ApiOperation(value = "修改")
    public Result<Boolean> update(@RequestBody InterfaceLimit interfaceLimit) {
        return Result.status(this.interfaceLimitService.updateById(interfaceLimit));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @ApiLog(type = 13,title = "删除")
    @GetMapping("delete")
    @ApiOperation(value = "删除")
    public Result<Boolean> delete(@RequestParam("idList") List<Long> idList) {
        return Result.status(this.interfaceLimitService.removeByIds(idList));
    }

    /**
     *  清空访问次数
     * @param userAccount 用户账号
     * @param interfaceType 接口类型
     * @return
     */
    @ApiLog(type = 13,title = "清空访问次数")
    @PostMapping("empty")
    @ApiOperation(value = "清空访问次数")
    public Result<Boolean> empty(@RequestParam String userAccount,@RequestParam String interfaceType) {
        return Result.status(this.interfaceLimitService.empty(userAccount, interfaceType));
    }

    /**
     * 新用户初始化
     * @return
     */
    @ApiLog(type = 13,title = "新用户初始化")
    @PostMapping("init")
    @ApiOperation(value = "新用户初始化")
    public Result<Boolean> initNumber() {
        return Result.status(this.interfaceLimitService.initNumber());
    }

    /**
     * 新增访问次数
     * @param userAccount 用户账号
     * @param interfaceType  接口类型
     * @param number 次数
     * @return
     */
    @ApiLog(type = 13,title = "新增用户访问次数")
    @PostMapping("add")
    @ApiOperation(value = "新增用户访问次数")
    public Result<Boolean> addNumber(@RequestParam String userAccount,@RequestParam Long number,@RequestParam String interfaceType) {
        return Result.status(this.interfaceLimitService.addNumber(userAccount, number,interfaceType));
    }

    /**
     * excel导出
     */
    @ApiLog(type = 13,title = "excel导出")
    @ApiOperation("excel导出")
    @GetMapping("/importExcel")
    public void excel(HttpServletResponse response) throws IOException {
        ExportExcelUtils.entityExcel(interfaceLimitService.list(),"访问次数.xlsx",response);
    }
}
