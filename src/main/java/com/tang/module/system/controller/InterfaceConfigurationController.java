package com.tang.module.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tang.common.annotation.ApiLog;
import com.tang.common.utils.ExportExcelUtils;
import com.tang.common.resources.Result;
import com.tang.common.resources.TaoPage;
import com.tang.module.system.entity.InterfaceConfiguration;
import com.tang.module.system.entity.Role;
import com.tang.module.system.service.InterfaceConfigurationService;
import com.tang.module.system.vo.InterfaceConfigurationVo;
import com.tang.module.system.wrapper.InterfaceConfigurationWrapper;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import io.swagger.annotations.ApiOperation;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * 业务接口配置表(InterfaceConfiguration)控制层
 *
 * @author tang
 * @since 2022-01-21 12:59:21
 */
@RestController
@RequestMapping("interfaceConfiguration")
@Api(tags = "业务接口配置控制层")
public class InterfaceConfigurationController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private InterfaceConfigurationService interfaceConfigurationService;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @param interfaceConfiguration 查询实体
     * @return 所有数据
     */
    @ApiLog(type = 12,title = "分页查询")
    @GetMapping()
    @ApiOperation(value = "分页查询")
    public Result<IPage<InterfaceConfigurationVo>> selectAll(@Valid TaoPage page, InterfaceConfiguration interfaceConfiguration) {
        return Result.data(InterfaceConfigurationWrapper.build().pageWrapper(
                this.interfaceConfigurationService.page(new Page<>(page.getCurrent(), page.getSize()), new QueryWrapper<>(interfaceConfiguration))
        ));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @ApiLog(type = 12,title = "主键查询")
    @GetMapping("one")
    @ApiOperation(value = "主键查询")
    public Result<InterfaceConfiguration> selectOne(Serializable id) {
        return Result.data(this.interfaceConfigurationService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param interfaceConfiguration 实体对象
     * @return 新增结果
     */
    @ApiLog(type = 12,title = "新增")
    @PostMapping
    @ApiOperation(value = "新增")
    public Result<Boolean> insert(@RequestBody InterfaceConfiguration interfaceConfiguration) {
        return Result.status(this.interfaceConfigurationService.save(interfaceConfiguration));
    }

    /**
     * 修改数据
     *
     * @param interfaceConfiguration 实体对象
     * @return 修改结果
     */
    @ApiLog(type = 12,title = "修改")
    @PostMapping("update")
    @ApiOperation(value = "修改")
    public Result<Boolean> update(@RequestBody InterfaceConfiguration interfaceConfiguration) {
        return Result.status(this.interfaceConfigurationService.updateById(interfaceConfiguration));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @ApiLog(type = 12,title = "删除")
    @DeleteMapping("delete")
    @ApiOperation(value = "删除")
    public Result<Boolean> delete(@RequestParam("idList") List<Long> idList) {
        return Result.status(this.interfaceConfigurationService.removeByIds(idList));
    }

    /**
     * 获得接口所需角色
     *
     * @param id 主键
     * @return 删除结果
     */
    @ApiLog(type = 12,title = "接口角色")
    @GetMapping("role")
    @ApiOperation(value = "接口角色")
    public Result<List<Role>> getRole(@RequestParam("id") Long id) {
        return Result.data(this.interfaceConfigurationService.getRole(id));
    }

    /**
     * 更新接口所需的角色
     *
     * @param id 接口ID
     * @param rid 角色ID集合
     * @return 删除结果
     */
    @ApiLog(type = 12,title = "更新角色")
    @PostMapping("role")
    @ApiOperation(value = "更新角色")
    public Result<Boolean> role(@RequestParam("id") Long id,@RequestParam("rid") List<Long> rid) {
        return Result.data(this.interfaceConfigurationService.updateRole(id, rid));
    }


    /**
     * 刷新缓存
     *
     * @return 刷新缓存结果
     */
    @ApiLog(type = 12,title = "刷新缓存")
    @GetMapping("cacheRefresh")
    @ApiOperation(value = "刷新缓存")
    public Result<Boolean> cacheRefresh() {
        return Result.data(this.interfaceConfigurationService.cacheRefresh());
    }

    /**
     * excel导出
     */
    @ApiLog(type = 12,title = "excel导出")
    @ApiOperation("excel导出")
    @GetMapping("/importExcel")
    public void excel(HttpServletResponse response) throws IOException {
        ExportExcelUtils.entityExcel(interfaceConfigurationService.list(),"接口配置.xlsx",response);
    }
}
