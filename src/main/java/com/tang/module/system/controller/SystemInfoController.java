package com.tang.module.system.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.tang.common.resources.Result;
import com.tang.common.resources.TaoPage;
import com.tang.module.system.entity.NameAndValue;
import com.tang.module.system.service.StatisticsService;
import com.tang.module.system.service.SystemInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 系统状态信息
 * @author tang
 * @date 2022/1/21 18:13
 */
@RestController
@Api(tags = "系统信息")
@RequestMapping("/system")
public class SystemInfoController {

    @Autowired
    private SystemInfoService systemInfoService;

    @Autowired
    private StatisticsService statisticsService;

    @ApiOperation(value = "获取在线用户信息")
    @ApiOperationSupport(order = 1)
    @GetMapping("/onLine")
    public Result<Page<Object>> onLine(TaoPage taoPage){
        return Result.data(systemInfoService.onlineUsers(taoPage));
    }

    /**
     * 强制下线
     * @param account 账户
     * @return
     */
    @ApiOperation(value = "强制下线")
    @ApiOperationSupport(order = 2)
    @PostMapping("/offline")
    public Result<Boolean> offline(@RequestParam String account){
        return Result.data(systemInfoService.offline(account));
    }

    /**
     * 首页日志统计折线图数据
     *
     * @return 统计数据
     */
    @GetMapping("statistics")
    @ApiOperation(value = "首页日志统计")
    public Result<Map<String,Map<String, Object>> > statistics() {
        return Result.data(this.statisticsService.byDay());
    }

    /**
     * 首页统计的记录数
     *
     * @return 统计数据
     */
    @GetMapping("record")
    @ApiOperation(value = "首页日志统计")
    public Result<Map<String,String> > recordStatistics() {
        return Result.data(this.statisticsService.recordStatistics());
    }

    /**
     * 访问来源统计
     *
     * @return 集合数据
     */
    @GetMapping("accessSource")
    @ApiOperation(value = "首页日志统计")
    public Result<List<NameAndValue>> accessSource() {
        return Result.data(this.statisticsService.accessSource());
    }

}
