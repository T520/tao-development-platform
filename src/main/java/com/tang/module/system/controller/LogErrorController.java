package com.tang.module.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tang.common.resources.Result;
import com.tang.common.resources.TaoPage;
import com.tang.module.system.entity.LogError;
import com.tang.module.system.service.LogErrorService;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import javax.validation.Valid;
import io.swagger.annotations.ApiOperation;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * 错误日志表(LogError)控制层
 *
 * @author tang
 * @since 2022-01-20 10:34:00
 */
@RestController
@RequestMapping("logError")
@Api(tags = "错误日志控制层")
public class LogErrorController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private LogErrorService logErrorService;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @param logError 查询实体
     * @return 所有数据
     */
    @GetMapping()
    @ApiOperation(value = "分页查询")
    public Result selectAll(@Valid TaoPage page, LogError logError) {
        return Result.data(this.logErrorService.page(new Page<>(page.getCurrent(), page.getSize()), new QueryWrapper<>(logError)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    @ApiOperation(value = "主键查询")
    public Result selectOne(@PathVariable Serializable id) {
        return Result.data(this.logErrorService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param logError 实体对象
     * @return 新增结果
     */
    @PostMapping
    @ApiOperation(value = "新增")
    public Result insert(@RequestBody LogError logError) {
        return Result.status(this.logErrorService.save(logError));
    }

    /**
     * 修改数据
     *
     * @param logError 实体对象
     * @return 修改结果
     */
    @PutMapping
    @ApiOperation(value = "修改")
    public Result update(@RequestBody LogError logError) {
        return Result.status(this.logErrorService.updateById(logError));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping
    @ApiOperation(value = "删除")
    public Result delete(@RequestParam("idList") List<Long> idList) {
        return Result.status(this.logErrorService.removeByIds(idList));
    }
}
