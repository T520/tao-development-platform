package com.tang.module.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tang.common.annotation.ApiLog;
import com.tang.common.utils.ExportExcelUtils;
import com.tang.common.resources.Result;
import com.tang.common.resources.TaoPage;
import com.tang.module.system.entity.Menu;
import com.tang.module.system.entity.User;
import com.tang.module.system.service.MenuService;
import com.tang.module.system.vo.MenuVo;
import com.tang.module.system.wrapper.MenuWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import io.swagger.annotations.ApiOperation;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * 菜单表(Menu)控制层
 *
 * @author tang
 * @since 2022-01-20 17:29:39
 */
@RestController
@RequestMapping("menu")
@Api(tags = "菜单控制层")
public class MenuController extends ApiController {
    /**
     * 服务对象
     */
    @Autowired
    private MenuService menuService;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @param menu 查询实体
     * @return 所有数据
     */
    @ApiLog(type = 2,title = "分页查询")
    @GetMapping()
    @ApiOperation(value = "分页查询")
    public Result<IPage<MenuVo>> selectAll(@Valid TaoPage page, Menu menu) {
        return Result.data(MenuWrapper.build().pageWrapper(this.menuService.page(new Page<>(page.getCurrent(), page.getSize()), new QueryWrapper<>(menu))));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @ApiLog(type = 2,title = "主键查询")
    @GetMapping("one")
    @ApiOperation(value = "主键查询")
    public Result<Menu> selectOne(Serializable id) {
        return Result.data(this.menuService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param menu 实体对象
     * @return 新增结果
     */
    @ApiLog(type = 2,title = "新增")
    @PostMapping
    @ApiOperation(value = "新增")
    public Result<Boolean> insert(@RequestBody Menu menu) {
        return Result.status(this.menuService.save(menu));
    }

    /**
     * 修改数据
     *
     * @param menu 实体对象
     * @return 修改结果
     */
    @ApiLog(type = 2,title = "修改")
    @PostMapping("update")
    @ApiOperation(value = "修改")
    public Result<Boolean> update(@RequestBody Menu menu) {
        return Result.status(this.menuService.updateById(menu));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @ApiLog(type = 2,title = "删除")
    @DeleteMapping("delete")
    @ApiOperation(value = "删除")
    public Result<Boolean> delete(@RequestParam("idList") List<Long> idList) {
        return Result.status(this.menuService.removeByIds(idList));
    }

    /**
     * 获取按钮树形结构
     * @return 按钮树
     */
    @ApiLog(type = 2,title = "获取按钮树形结构")
    @GetMapping("/tree/button")
    @ApiOperation(value = "获取按钮树形结构")
    public Result<List<Menu>> button(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return Result.data(this.menuService.button((User) authentication.getPrincipal()));
    }

    /**
     * 登录获取菜单
     * @return 菜单树
     */
    @ApiLog(type = 2,title = "登录获取菜单")
    @GetMapping("/loginMenu")
    @ApiOperation(value = "登录获取菜单")
    public Result<List<Menu>> loginMenu(){
        return Result.data(this.menuService.loginTree());
    }

    /**
     * 获取全部部菜单树形结构
     * @param category 菜单类别，默认为1（菜单类型）,2（按钮），3（全部）
     * @return 菜单树
     */
    @ApiLog(type = 2,title = "获取全部菜单树形结构")
    @GetMapping("/tree")
    @ApiOperation(value = "获取全部菜单树形结构")
    public Result<List<Menu>> tree(@RequestParam(value = "category",defaultValue = "1") String category){
        return Result.data(this.menuService.tree(category));
    }


    /**
     * 获取全部菜单按钮树形结构
     * @param status 菜单状态
     * @return 菜单vo树
     */
    @ApiLog(type = 2,title = "获取全部菜单VO树形结构")
    @GetMapping("/tree/all")
    @ApiOperation(value = "获取全部菜单VO树形结构")
    public Result<List<MenuVo>> treeAll(@RequestParam(value = "status",defaultValue = "1") Integer status){
        return Result.data(this.menuService.treeVo(status));
    }

    /**
     * 获取父级菜单
     * @param level 需要获取的菜单层树
     * @return 对应层树的菜单
     */
    @ApiLog(type = 2,title = "获取父级菜单")
    @GetMapping("/parent")
    @ApiOperation(value = "获取父级菜单")
    public Result<List<Menu>> parentMenu(@RequestParam("level") Integer level){
        return Result.data(this.menuService.parentMenuLevel(level));
    }

    /**
     * 获取父级字符串，格式：1，2，3，4
     * @param category 当前菜单类型
     * @param parentId 父ID
     * @param id 菜单ID
     * @return 父级字符串，
     */
    @ApiLog(type = 2,title = "父级字符串")
    @GetMapping("/parentString")
    @ApiOperation(value = "获取父级字符串")
    public Result<String> parentMenuCommaString(Integer category,Long parentId,Long id){
        return Result.data(this.menuService.parentMenuCommaString(category, parentId, id));
    }

    /**
     * 获取一级菜单
     * @return 一级菜单
     */
    @ApiLog(type = 2,title = "一级菜单")
    @GetMapping("/top")
    @ApiOperation(value = "一级菜单")
    public Result<List<Menu>> top(){
        return Result.data(this.menuService.list(Wrappers.<Menu>lambdaQuery().eq(Menu::getParentId,0)));
    }

    /**
     * 角色菜单ID集合
     * @param id 角色ID
     * @return 一级菜单
     */
    @ApiLog(type = 2,title = "角色菜单ID集合")
    @GetMapping("/idList")
    @ApiOperation(value = "角色菜单ID集合")
    public Result<List<String>> idList(Long id){
        return Result.data(this.menuService.roleGetMenu(id));
    }

    /**
     * excel导出
     */
    @ApiLog(type = 2,title = "excel导出")
    @ApiOperation("excel导出")
    @GetMapping("/importExcel")
    public void excel(HttpServletResponse response) throws IOException {
        ExportExcelUtils.entityExcel(menuService.list(),"菜单.xlsx",response);
    }
}
