package com.tang.module.system.controller;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.tang.common.annotation.ApiLog;
import com.tang.common.constant.cahce.BusinessCacheConstant;
import com.tang.common.exception.SecurityException;
import com.tang.common.interceptor.frequency.EntranceFilterHandlerActuator;
import com.tang.common.resources.Result;
import com.tang.module.system.entity.User;
import com.tang.module.system.service.UserService;
import com.wf.captcha.SpecCaptcha;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 登录控制层
 * @author tang
 * @date 2022/1/13 14:01
 */
@Data
@Api(tags = "授权接口",value = "登录和获取验证码")
@RestController
public class LoginController {

    private final UserService userService;

    private final StringRedisTemplate stringRedisTemplate;

    private final EntranceFilterHandlerActuator entranceFilterHandlerActuator;


    @ApiLog(title = "登录",type = 1)
    @ApiOperation("登录接口")
    @PostMapping("/login")
    public Result<String> login(
                        @ApiParam(value = "验证码key", required = true)@RequestParam String codeKey,
                        @ApiParam(value = "验证码", required = true)@RequestParam String code,
                        @ApiParam(value = "账号", required = true) @RequestParam String username,
                        @ApiParam(value = "密码", required = true) @RequestParam String password){
        //获取缓存中验证码
        String s = stringRedisTemplate.opsForValue().get(BusinessCacheConstant.CAPTCHA_HEADER_KEY+codeKey);
        if (StrUtil.isBlank(s) || ! s.equalsIgnoreCase(code)){
            throw new SecurityException("验证码错误或已过期");
        }
        //用户名密码验证
        return Result.data(userService.login(username,password));
    }

    @ApiLog(title = "验证码",type = 1)
    @ApiOperation("验证码")
    @GetMapping("/captcha")
    public Result<Map<String, String>> captcha(){
        SpecCaptcha specCaptcha = new SpecCaptcha(130, 48, 5);
        //小写验证码字符
        String code = specCaptcha.text().toLowerCase(Locale.ROOT);
        //随机的验证码key
        String key = IdUtil.simpleUUID();
        //有效时间5分钟
        stringRedisTemplate.opsForValue().set(BusinessCacheConstant.CAPTCHA_HEADER_KEY+key,code,5, TimeUnit.MINUTES);
        //返回bean64加密的验证码字符串
        return  Result.data(MapUtil.builder("captchaKey",key).
                put("captcha",specCaptcha.toBase64())
                .build());
    }

    @ApiLog(title = "获取当前用户信息",type = 1)
    @ApiOperation("获取用户信息")
    @GetMapping("/userDetails")
    private Result<User> userDetails(){
        //获得安全上下文
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() != null){
            return Result.data((com.tang.module.system.entity.User)authentication.getPrincipal());
        }
        throw new SecurityException("获取登录用户信息失败");
    }

}
