package com.tang.module.system.controller;



import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tang.common.annotation.ApiLog;
import com.tang.common.utils.ExportExcelUtils;
import com.tang.common.resources.Result;
import com.tang.common.resources.TaoPage;
import com.tang.module.system.entity.Param;
import com.tang.module.system.service.ParamService;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import io.swagger.annotations.ApiOperation;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * 参数表(Param)控制层
 *
 * @author tang
 * @since 2022-01-13 12:47:13
 */
@RestController
@RequestMapping("param")
@Api(tags = "系统参数控制层")
public class ParamController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private ParamService paramService;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @param param 查询实体
     * @return 所有数据
     */
    @ApiLog(type = 6,title = "分页查询")
    @GetMapping()
    @ApiOperation(value = "分页查询")
    public Result<Page<Param>> selectAll(@Valid TaoPage page, Param param) {
        return Result.data(this.paramService.page(new Page<>(page.getCurrent(), page.getSize()), new QueryWrapper<>(param)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @ApiLog(type = 6,title = "主键查询")
    @GetMapping("one")
    @ApiOperation(value = "主键查询")
    public Result<Param> selectOne(Serializable id) {
        return Result.data(this.paramService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param param 实体对象
     * @return 新增结果
     */
    @ApiLog(type = 6,title = "新增")
    @PostMapping
    @ApiOperation(value = "新增")
    public Result<Boolean> insert(@RequestBody Param param) {
        return Result.status(this.paramService.save(param));
    }

    /**
     * 修改数据
     *
     * @param param 实体对象
     * @return 修改结果
     */
    @ApiLog(type = 6,title = "修改")
    @PostMapping("update")
    @ApiOperation(value = "修改")
    public Result<Boolean> update(@RequestBody Param param) {
        return Result.status(this.paramService.updateById(param));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @ApiLog(type = 6,title = "删除")
    @DeleteMapping("delete")
    @ApiOperation(value = "删除")
    public Result<Boolean> delete(@RequestParam("idList") List<Long> idList) {
        return Result.status(this.paramService.removeByIds(idList));
    }

    /**
     * excel导出
     */
    @ApiLog(type = 3,title = "excel导出")
    @ApiOperation("excel导出")
    @GetMapping("/importExcel")
    public void excel(HttpServletResponse response) throws IOException {
        ExportExcelUtils.entityExcel(paramService.list(),"系统参数.xlsx",response);
    }

    /**
     * 通过key获得参数值
     */
    @ApiLog(type = 3,title = "获得参数值")
    @ApiOperation("获得参数值")
    @GetMapping("/code")
    public Result<String> code(@RequestParam String code) {
        Param one = paramService.getOne(Wrappers.<Param>lambdaQuery().eq(Param::getParamKey, code));
        return  Result.data(one == null ? null : one.getParamValue());
    }

}
