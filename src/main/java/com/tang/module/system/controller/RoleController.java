package com.tang.module.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tang.common.annotation.ApiLog;
import com.tang.common.utils.ExportExcelUtils;
import com.tang.common.resources.Result;
import com.tang.common.resources.TaoPage;
import com.tang.module.system.entity.Role;
import com.tang.module.system.service.RoleService;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import io.swagger.annotations.ApiOperation;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * 角色表(Role)控制层
 *
 * @author tang
 * @since 2022-01-13 10:31:42
 */
@RestController
@RequestMapping("role")
@Api(tags = "角色控制层")
public class RoleController extends ApiController {


    /**
     * 服务对象
     */
    @Resource
    private RoleService roleService;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @param role 查询实体
     * @return 所有数据
     */
    @ApiLog(type = 4,title = "分页查询")
    @GetMapping()
    @ApiOperation(value = "分页查询")
    public Result<Page<Role>> selectAll(@Valid TaoPage page, Role role) {
        return Result.data(this.roleService.page(new Page<>(page.getCurrent(), page.getSize()), new QueryWrapper<>(role)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @ApiLog(type = 4,title = "主键查询")
    @GetMapping("one")
    @ApiOperation(value = "主键查询")
    public Result<Role> selectOne(Serializable id) {
        return Result.data(this.roleService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param role 实体对象
     * @return 新增结果
     */
    @ApiLog(type = 4,title = "新增")
    @PostMapping
    @ApiOperation(value = "新增")
    public Result<Boolean> insert(@RequestBody Role role) {
        return Result.status(this.roleService.save(role));
    }

    /**
     * 修改数据
     *
     * @param role 实体对象
     * @return 修改结果
     */
    @ApiLog(type = 4,title = "修改")
    @PostMapping("update")
    @ApiOperation(value = "修改")
    public Result<Boolean> update(@RequestBody Role role) {
        return Result.status(this.roleService.updateById(role));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @ApiLog(type = 4,title = "删除")
    @DeleteMapping("delete")
    @ApiOperation(value = "删除")
    public Result<Boolean> delete(@RequestParam("idList") List<Long> idList) {
        return Result.status(this.roleService.removeByIds(idList));
    }

    /**
     * 查询全部角色名称和id
     * @return 所有数据
     */
    @ApiLog(type = 4,title = "查询全部角色名称和id")
    @GetMapping("nameOrId")
    @ApiOperation(value = "查询全部角色名称和id")
    public Result<List<Role>> selectAllId() {
        return Result.data(roleService.list(Wrappers.<Role>lambdaQuery().select(Role::getRoleAlias,Role::getId)));
    }


    /**
     * 更新角色拥有的菜单
     * @param menuIds 菜单Id字符串集合（，分割）
     * @param roleId 角色ID
     * @return 是否成功
     */
    @ApiLog(type = 4,title = "分页查询")
    @PostMapping("saveMenu")
    @ApiOperation(value = "分页查询")
    public Result<Boolean> updateRoleMenu(@RequestParam("menuIds") String menuIds,@RequestParam("roleId") String roleId) {
        return Result.status(roleService.updateRoleMenu(menuIds, roleId));
    }

    /**
     * excel导出
     */
    @ApiLog(type = 4,title = "excel导出")
    @ApiOperation("excel导出")
    @GetMapping("/importExcel")
    public void excel(HttpServletResponse response) throws IOException {
        ExportExcelUtils.entityExcel(roleService.list(),"角色.xlsx",response);
    }


}
