package com.tang.module.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tang.common.resources.Result;
import com.tang.module.system.entity.Role;
import com.tang.module.system.service.MenuService;
import com.tang.module.system.service.SearchService;
import com.tang.module.system.vo.MenuSearchVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * 系统搜索服务
 * @author tang
 * @date 2022/4/20 16:08
 */
@Api(tags = "系统搜索服务")
@AllArgsConstructor
@RestController
@RequestMapping("/search")
public class SearchController {

    private final SearchService searchService;


    /**
     * 菜单搜索
     *
     * @param keyword 搜索关键字
     * @return 命中的数据
     */
    @GetMapping()
    @ApiOperation(value = "菜单搜索")
    public Result<List<MenuSearchVo>> selectAll(@RequestParam String keyword) {
        return Result.data(searchService.menuKeywordSearch(keyword));
    }



}
