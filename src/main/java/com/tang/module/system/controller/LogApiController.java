package com.tang.module.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tang.common.resources.Result;
import com.tang.common.resources.TaoPage;
import com.tang.module.system.entity.LogApi;
import com.tang.module.system.service.LogApiService;
import com.tang.module.system.vo.LogApiVo;
import com.tang.module.system.wrapper.LogApiWrapper;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import javax.validation.Valid;
import io.swagger.annotations.ApiOperation;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * 接口日志表(LogApi)控制层
 *
 * @author tang
 * @since 2022-01-18 18:08:15
 */
@RestController
@RequestMapping("logApi")
@Api(tags = "日志控制层")
public class LogApiController extends ApiController {

    /**
     * 服务对象
     */
    @Resource
    private LogApiService logApiService;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @param logApi 查询实体
     * @return 所有数据
     */
    @GetMapping()
    @ApiOperation(value = "分页查询")
    public Result<IPage<LogApiVo>> selectAll(@Valid TaoPage page, LogApi logApi) {
        return Result.data(LogApiWrapper.build().pageWrapper(
                this.logApiService.page(new Page<>(page.getCurrent(), page.getSize()), new QueryWrapper<>(logApi).lambda().orderByDesc(LogApi::getCreateTime))));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("one")
    @ApiOperation(value = "主键查询")
    public Result<LogApi> selectOne(Serializable id) {
        return Result.data(this.logApiService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param logApi 实体对象
     * @return 新增结果
     */
    @PostMapping
    @ApiOperation(value = "新增")
    public Result<Boolean> insert(@RequestBody LogApi logApi) {
        return Result.status(this.logApiService.save(logApi));
    }

    /**
     * 修改数据
     *
     * @param logApi 实体对象
     * @return 修改结果
     */
    @PostMapping("updata")
    @ApiOperation(value = "修改")
    public Result<Boolean> update(@RequestBody LogApi logApi) {
        return Result.status(this.logApiService.updateById(logApi));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @GetMapping("delete")
    @ApiOperation(value = "删除")
    public Result<Boolean> delete(@RequestParam("idList") List<Long> idList) {
        return Result.status(this.logApiService.removeByIds(idList));
    }
}
