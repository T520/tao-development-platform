package com.tang.module.system.controller;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tang.common.annotation.ApiLog;
import com.tang.common.resources.Result;
import com.tang.common.resources.TaoPage;
import com.tang.module.system.entity.Comment;
import com.tang.module.system.entity.User;
import com.tang.module.system.mapper.UserMapper;
import com.tang.module.system.search.CommentSearch;
import com.tang.module.system.service.CommentService;
import com.tang.module.system.vo.CommentVo;
import com.tang.module.system.wrapper.CommentWrapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import javax.validation.Valid;
import io.swagger.annotations.ApiOperation;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 评论表(Comment)控制层
 *
 * @author tang
 * @since 2022-03-20 21:34:00
 */
@RestController
@RequestMapping("comment")
@Api(tags = "评论表控制层")
public class CommentController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private CommentService commentService;

    @Resource
    private UserMapper userMapper;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @param commentSearch 查询实体
     * @return 所有数据
     */
    @ApiLog(type = 11,title = "分页")
    @GetMapping()
    @ApiOperation(value = "分页查询")
    public Result<IPage<CommentVo>> selectAll(@Valid TaoPage page, CommentSearch commentSearch) {
        Page<Comment> result = this.commentService.
                page(new Page<>(page.getCurrent(), page.getSize()),
                        new QueryWrapper<Comment>(commentSearch).lambda().isNotNull(Comment::getContent).orderByDesc(Comment::getCreateTime));
        IPage<CommentVo> commentVoIpage = CommentWrapper.build().pageWrapper(result);

        commentVoIpage.getRecords().forEach(comment -> {
            //获得发言用户
            User user = userMapper.selectOne(Wrappers.<User>lambdaQuery().select(User::getAvatar).eq(User::getId,comment.getCreateUser()));
            comment.setAvatarLink(user == null ? null : user.getAvatar());
        });
        return Result.data(commentVoIpage);
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @ApiLog(type = 11,title = "主键查询")
    @GetMapping("one")
    @ApiOperation(value = "主键查询")
    public Result<Comment> selectOne(Serializable id) {
        return Result.data(this.commentService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param comment 实体对象
     * @return 新增结果
     */
    @ApiLog(type = 11,title = "新增")
    @PostMapping
    @ApiOperation(value = "新增")
    public Result<Boolean> insert(@RequestBody Comment comment) {
        //填充公共实体
        comment.setCreateTime(new Date());
        User principal = (User) (SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        comment.setCreateUser(principal.getId());
        return Result.status(this.commentService.save(comment));
    }

    /**
     * 修改数据
     *
     * @param comment 实体对象
     * @return 修改结果
     */
    @ApiLog(type = 11,title = "修改")
    @PostMapping("update")
    @ApiOperation(value = "修改")
    public Result<Boolean> update(@RequestBody Comment comment) {
        return Result.status(this.commentService.updateById(comment));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @ApiLog(type = 11,title = "删除")
    @DeleteMapping("delete")
    @ApiOperation(value = "删除")
    public Result<Boolean> delete(@RequestParam("idList") List<Long> idList) {
        return Result.status(this.commentService.removeByIds(idList));
    }
}
