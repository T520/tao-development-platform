package com.tang.module.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tang.common.annotation.ApiLog;
import com.tang.common.utils.CommonUtils;
import com.tang.common.utils.ExportExcelUtils;
import com.tang.common.resources.Result;
import com.tang.common.resources.TaoPage;
import com.tang.module.system.entity.DictBiz;
import com.tang.module.system.service.DictBizService;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import io.swagger.annotations.ApiOperation;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 业务字典表(DictBiz)控制层
 *
 * @author tang
 * @since 2022-01-20 11:49:23
 */
@RestController
@RequestMapping("dictBiz")
@Api(tags = "业务字典控制层")
public class DictBizController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private DictBizService dictBizService;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @param dictBiz 查询实体
     * @return 所有数据
     */
    @ApiLog(type = 5,title = "分页查询")
    @GetMapping()
    @ApiOperation(value = "分页查询")
    public Result<Page<DictBiz>> selectAll(@Valid TaoPage page, DictBiz dictBiz) {
        return Result.data(this.dictBizService.page(new Page<>(page.getCurrent(), page.getSize()), new QueryWrapper<>(dictBiz)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @ApiLog(type = 5,title = "主键查询")
    @GetMapping("one")
    @ApiOperation(value = "主键查询")
    public Result<DictBiz> selectOne(Serializable id) {
        return Result.data(this.dictBizService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param dictBiz 实体对象
     * @return 新增结果
     */
    @ApiLog(type = 5,title = "新增")
    @PostMapping
    @ApiOperation(value = "新增")
    public Result<Boolean> insert(@RequestBody DictBiz dictBiz) {
        return Result.status(this.dictBizService.save(dictBiz));
    }

    /**
     * 修改数据
     *
     * @param dictBiz 实体对象
     * @return 修改结果
     */
    @ApiLog(type = 5,title = "修改")
    @PostMapping("update")
    @ApiOperation(value = "修改")
    public Result<Boolean> update(@RequestBody DictBiz dictBiz) {
        return Result.status(this.dictBizService.updateById(dictBiz));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @ApiLog(type = 5,title = "删除")
    @DeleteMapping("delete")
    @ApiOperation(value = "删除")
    public Result<Boolean> delete(@RequestParam("idList") List<Long> idList) {
        return Result.status(this.dictBizService.removeByIds(idList));
    }


    /**
     * 获取全部顶级字典
     * @return 顶级字典列表
     */
    @ApiLog(type = 5,title = "顶级字典")
    @GetMapping("top")
    @ApiOperation(value = "顶级字典")
    public Result<List<DictBiz>> selectTop(){
        return Result.data(this.dictBizService.list(Wrappers.<DictBiz>lambdaQuery().eq(DictBiz::getParentId,0).orderByAsc(DictBiz::getSort)));
    }

    /**
     * 获取字典类别
     * @param codeList 字典编码,格式：1,2,3
     * @return 字典列表（去除key为0的值）
     */
    @ApiLog(type = 5,title = "字典列表")
    @GetMapping("list")
    @ApiOperation(value = "字典列表")
    public Result<Map<String, List<DictBiz>>> list(@RequestParam(value = "codeList") List<String> codeList){
        return Result.data(this.dictBizService.list(codeList));
    }

    /**
     * 获取字典树
     * @return 顶级字典列表
     */
    @ApiLog(type = 5,title = "顶级字典")
    @GetMapping("tree")
    @ApiOperation(value = "顶级字典")
    public Result<Page<DictBiz>> tree(@Valid TaoPage taoPage){
        return Result.data(CommonUtils.pageUtli(this.dictBizService.tree(), taoPage.getCurrent(), taoPage.getSize()));
    }

    /**
     * excel导出
     */
    @ApiLog(type = 5,title = "excel导出")
    @ApiOperation("excel导出")
    @GetMapping("/importExcel")
    public void excel(HttpServletResponse response) throws IOException {
        ExportExcelUtils.entityExcel(dictBizService.list(),"业务字典.xlsx",response);
    }
}
