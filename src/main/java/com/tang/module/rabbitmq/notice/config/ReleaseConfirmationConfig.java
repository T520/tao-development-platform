package com.tang.module.rabbitmq.notice.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @author 月蚀
 * @since 2022/6/17 14:58
 */
@Slf4j
@Component
public class ReleaseConfirmationConfig implements RabbitTemplate.ConfirmCallback,RabbitTemplate.ReturnCallback{

    @Resource
    private RabbitTemplate rabbitTemplate;

    @PostConstruct
    private void  init(){
        //将创建的接口注入到RabbitTemplate中
        rabbitTemplate.setConfirmCallback(this);
        rabbitTemplate.setReturnCallback(this);
    }


    /**
     * 消息发送到交换机时调用此方法，或者消息发送不到交换机
     * @param correlationData  回调的信息和id，这个对象有发送方发送消息时自己构建指定
     * @param ack             是否成功
     * @param cause           失败的原因
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        if ( ! ack){
            log.info("失败发送到交换机失败，失败原因: " + cause);
        }
    }

    /**
     * 消息发送到队列失败时调用此方法
     * @param message the returned message. 消息
     * @param replyCode the reply code. 回退代码
     * @param replyText the reply text. 回退原因
     * @param exchange the exchange. 交换机
     * @param routingKey the routing key. 路由key
     */
    @Override
    public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
        log.error("消息队列回退消息，回退的消息{{}}，交换机{{}}，回退的原因{{}}，回退的路由key{{}}",message.toString(),exchange,replyText,routingKey);
    }
}
