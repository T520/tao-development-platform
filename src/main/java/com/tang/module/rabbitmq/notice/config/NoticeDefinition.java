package com.tang.module.rabbitmq.notice.config;

import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 系统通知的交换机，队列创建配置类
 * @author 月蚀
 * @since 2022/6/15 18:14
 */
@Configuration
public class NoticeDefinition {


    public static final String SYSTEM_NOTICE_EXCHANGE = "systemNoticeDirectExchange";

    /**
     * 系统通知采用直接交换机
     * 依靠路由key来分发消息
     * @return 直接交换机
     */
    @Bean
    public DirectExchange directExchange(){
        return ExchangeBuilder.directExchange(SYSTEM_NOTICE_EXCHANGE).build();
    }

}
