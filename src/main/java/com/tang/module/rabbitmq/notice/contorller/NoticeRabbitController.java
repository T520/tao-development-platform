package com.tang.module.rabbitmq.notice.contorller;

import com.tang.common.resources.Result;
import com.tang.module.rabbitmq.notice.service.NoticeRabbitService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 系统通知的rabbitMQ控制层，负责消息的发送
 * @author 月蚀
 * @since 2022/6/16 16:08
 */
@RestController
@Api(tags = "系统通知发送")
@RequestMapping("/notice/rabbit")
public class NoticeRabbitController {


    @Resource
    private NoticeRabbitService noticeRabbitService;

    /**
     * 将消息发送给指定的用户
     * @param uid 用户id
     * @param title 消息标题
     * @param message 消息内容
     * @return 是否成功
     */
    @PostMapping("")
    @ApiOperation(value = "将消息发送给指定的用户")
    public Result<Boolean> byUserIdSend(@RequestParam Long uid, @RequestParam String title, @RequestParam String message){
       return Result.data(noticeRabbitService.byUserIdSend(uid,title,message));
    }

    /**
     * 将消息发送给指定的多个用户
     * @param uidList 用户id集合
     * @param title 消息标题
     * @param message 消息内容
     * @return 是否成功
     */
    @PostMapping("/ids")
    @ApiOperation(value = "将消息发送给指定的用户")
    public Result<Boolean> byUsersIdSend(@RequestParam List<Long> uidList, @RequestParam String title, @RequestParam String message){
        return Result.data(noticeRabbitService.byUsersIdSend(uidList,title,message));
    }

    /**
     * 群发消息
     * @param deptList 部门id集合
     * @param title 消息标题
     * @param message 消息内容
     * @return 是否成功
     */
    @PostMapping("bulk")
    @ApiOperation(value = "群发消息")
    public Result<Boolean> bulkSend(@RequestParam List<Long> deptList, @RequestParam String title, @RequestParam String message){
        return Result.data(noticeRabbitService.bulkSend(deptList,title,message));
    }


}
