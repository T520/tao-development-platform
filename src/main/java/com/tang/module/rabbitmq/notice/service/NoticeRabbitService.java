package com.tang.module.rabbitmq.notice.service;


import java.util.List;

/**
 * 系统通知的rabbitMQ业务层
 * @author 月蚀
 * @since 2022/6/16 16:16
 */
public interface NoticeRabbitService {

    /**
     * 将消息发生给指定的用户
     * @param uid 用户id
     * @param title 消息标题
     * @param message 消息内容
     * @return 是否成功
     */
     Boolean byUserIdSend(Long uid, String title, String message);

    /**
     * 将消息发生给指定的用户
     * @param deptList 部门集合
     * @param title 消息标题
     * @param message 消息内容
     * @return 是否成功
     */
     Boolean bulkSend(List<Long> deptList,String title,String message);

    /**
     * 将消息发送给指定的多个用户
     * @param uidList 用户id集合
     * @param title 消息标题
     * @param message 消息内容
     * @return 是否成功
     */
    Boolean byUsersIdSend(List<Long> uidList, String title, String message);
}
