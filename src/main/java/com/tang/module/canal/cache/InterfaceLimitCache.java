package com.tang.module.canal.cache;

import com.alibaba.otter.canal.protocol.CanalEntry;
import com.google.protobuf.InvalidProtocolBufferException;
import com.tang.common.annotation.canal.CanalDeletePoint;
import com.tang.common.annotation.canal.CanalEventListener;
import com.tang.common.annotation.canal.CanalInsertPoint;
import com.tang.common.annotation.canal.CanalUpdatePoint;
import com.tang.common.constant.BusinessConstant;
import com.tang.common.resources.CanalCacheEventHandling;
import com.tang.module.system.entity.InterfaceConfiguration;
import com.tang.module.system.entity.InterfaceLimit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.annotation.Resource;

/**
 * @author tang
 * @date 2022/3/10 15:13
 */
@Slf4j
@CanalEventListener
public class InterfaceLimitCache implements CanalCacheEventHandling {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    @CanalInsertPoint(database = "tao",tableName = "tao_interface_limit")
    public void insertDealWith(CanalEntry.Entry entry) {
        try {
            CanalEntry.RowChange rowChange = CanalEntry.RowChange.parseFrom(entry.getStoreValue());
            for (CanalEntry.RowData rowData :  rowChange.getRowDatasList()) {
                //获取实体
                InterfaceLimit interfaceLimit = parameterInverseSequence(rowData.getAfterColumnsList(), InterfaceLimit.class);
                //新增系统参数缓存,ZSet结构
                stringRedisTemplate.opsForZSet().add(BusinessConstant.INTERFACE_RESTRICTION_FREQUENCY,interfaceLimit.getUserId() + "-"+ interfaceLimit.getInterfaceType(),interfaceLimit.getRemainingTimes());
            }
        } catch (Exception e) {
            log.error("接口访问次数缓存新增失败",e);
            e.printStackTrace();
        }
    }

    @Override
    @CanalUpdatePoint(database = "tao",tableName = "tao_interface_limit")
    public void updateDealWith(CanalEntry.Entry entry) {
        try {
            CanalEntry.RowChange rowChange = CanalEntry.RowChange.parseFrom(entry.getStoreValue());
            for (CanalEntry.RowData rowData :  rowChange.getRowDatasList()) {
                //转换成实体对象
                InterfaceLimit interfaceLimitBefore = parameterInverseSequence(rowData.getBeforeColumnsList(),InterfaceLimit.class);
                InterfaceLimit interfaceLimitAfter = parameterInverseSequence(rowData.getAfterColumnsList(),InterfaceLimit.class);
                //是否修改了剩余访问次数
                String key = interfaceLimitBefore.getUserId() + "-" + interfaceLimitBefore.getInterfaceType();
                if (! interfaceLimitAfter.getRemainingTimes().equals(interfaceLimitBefore.getRemainingTimes())){
                    //更新分数
                    stringRedisTemplate.opsForZSet().add(BusinessConstant.INTERFACE_RESTRICTION_FREQUENCY, key
                    ,interfaceLimitAfter.getRemainingTimes());
                }
                //是否（停用/逻辑）删除
                if (interfaceLimitAfter.getIsDeleted() == 1 ){
                    stringRedisTemplate.opsForZSet().remove(BusinessConstant.INTERFACE_RESTRICTION_FREQUENCY,
                            key);
                }
            }
        } catch (Exception e) {
            log.error("接口访问次数缓存更新失败",e);
            e.printStackTrace();
        }
    }

    @Override
    @CanalDeletePoint(database = "tao",tableName = "tao_interface_limit")
    public void deleteDealWith(CanalEntry.Entry entry) {
        try {
            CanalEntry.RowChange rowChange = CanalEntry.RowChange.parseFrom(entry.getStoreValue());
            for (CanalEntry.RowData rowData :  rowChange.getRowDatasList()) {
                //转换成实体对象
                InterfaceLimit interfaceLimit = parameterInverseSequence(rowData.getBeforeColumnsList(),InterfaceLimit.class);
                //删除系统参数缓存
                stringRedisTemplate.opsForZSet().remove(BusinessConstant.INTERFACE_RESTRICTION_FREQUENCY,
                        interfaceLimit.getUserId() + "-" + interfaceLimit.getInterfaceType());
            }
        } catch (Exception e) {
            log.error("接口访问次数缓存删除失败",e);
            e.printStackTrace();
        }
    }
}
