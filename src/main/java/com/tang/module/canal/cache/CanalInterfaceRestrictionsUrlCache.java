package com.tang.module.canal.cache;

import com.alibaba.otter.canal.protocol.CanalEntry;
import com.google.protobuf.InvalidProtocolBufferException;
import com.tang.common.annotation.canal.CanalDeletePoint;
import com.tang.common.annotation.canal.CanalEventListener;
import com.tang.common.annotation.canal.CanalInsertPoint;
import com.tang.common.annotation.canal.CanalUpdatePoint;
import com.tang.common.constant.BusinessConstant;
import com.tang.common.constant.DictConstant;
import com.tang.common.resources.CanalCacheEventHandling;
import com.tang.common.utils.cache.DictCache;
import com.tang.module.system.entity.InterfaceConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.annotation.Resource;

/**
 * 接口url限制缓存
 * @author tang
 * @date 2022/1/24 13:45
 */
@Slf4j
@CanalEventListener
public class CanalInterfaceRestrictionsUrlCache implements CanalCacheEventHandling {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    @CanalInsertPoint(database = "tao",tableName = "tao_interface_configuration")
    public void insertDealWith(CanalEntry.Entry entry) {
        try {
            CanalEntry.RowChange rowChange = CanalEntry.RowChange.parseFrom(entry.getStoreValue());
            for (CanalEntry.RowData rowData :  rowChange.getRowDatasList()) {
                //获取实体
                InterfaceConfiguration interfaceConfiguration = parameterInverseSequence(rowData.getAfterColumnsList(), InterfaceConfiguration.class);
                //新增系统参数缓存,哈希结构
                stringRedisTemplate.opsForHash().put(
                        BusinessConstant.INTERFACE_RESTRICTION_HASH,
                        DictCache.getValue(DictConstant.HTTP_TYPE, Math.toIntExact(interfaceConfiguration.getMethod())) + interfaceConfiguration.getSign(),
                        String.valueOf(interfaceConfiguration.getInterfaceType()));
            }
        } catch (InvalidProtocolBufferException e) {
            log.error("接口限制url名单缓存新增失败",e);
            e.printStackTrace();
        }
    }

    @Override
    @CanalUpdatePoint(database = "tao",tableName = "tao_interface_configuration")
    public void updateDealWith(CanalEntry.Entry entry) {
        try {
            CanalEntry.RowChange rowChange = CanalEntry.RowChange.parseFrom(entry.getStoreValue());
            for (CanalEntry.RowData rowData :  rowChange.getRowDatasList()) {
                //转换成实体对象
                InterfaceConfiguration interfaceBefore = parameterInverseSequence(rowData.getBeforeColumnsList(),InterfaceConfiguration.class);
                InterfaceConfiguration interfaceAfter = parameterInverseSequence(rowData.getAfterColumnsList(),InterfaceConfiguration.class);
                //是否修改了类型或者标记
                if (! interfaceAfter.getInterfaceType().equals(interfaceBefore.getInterfaceType()) ||
                        ! interfaceAfter.getSign().equals(interfaceBefore.getSign()) ||
                        ! interfaceAfter.getMethod().equals(interfaceBefore.getMethod())){
                    //删除之前的
                    stringRedisTemplate.opsForHash().delete(
                            BusinessConstant.INTERFACE_RESTRICTION_HASH
                            ,DictCache.getValue(DictConstant.HTTP_TYPE, Math.toIntExact(interfaceBefore.getMethod())) + interfaceBefore.getSign());
                    //新增现在的
                    stringRedisTemplate.opsForHash().put(
                            BusinessConstant.INTERFACE_RESTRICTION_HASH,
                            DictCache.getValue(DictConstant.HTTP_TYPE, Math.toIntExact(interfaceBefore.getMethod()))+ interfaceAfter.getSign(),
                            String.valueOf(interfaceAfter.getInterfaceType()));
                }
                //是否（停用/逻辑）删除
                if (interfaceAfter.getIsDeleted() == 1 || ! interfaceAfter.getOpenLimit()){
                    stringRedisTemplate.opsForHash().delete(BusinessConstant.INTERFACE_RESTRICTION_HASH,interfaceAfter.getMethod() + interfaceAfter.getSign());
                }
            }
        } catch (InvalidProtocolBufferException e) {
            log.error("接口限制url名单缓存更新失败",e);
            e.printStackTrace();
        }
    }

    @Override
    @CanalDeletePoint(database = "tao",tableName = "tao_interface_configuration")
    public void deleteDealWith(CanalEntry.Entry entry) {
        try {
            CanalEntry.RowChange rowChange = CanalEntry.RowChange.parseFrom(entry.getStoreValue());
            for (CanalEntry.RowData rowData :  rowChange.getRowDatasList()) {
                //转换成实体对象
                InterfaceConfiguration interfaceConfiguration = parameterInverseSequence(rowData.getBeforeColumnsList(),InterfaceConfiguration.class);
                //删除系统参数缓存
                stringRedisTemplate.opsForHash().delete(BusinessConstant.INTERFACE_RESTRICTION_HASH,interfaceConfiguration.getSign());
            }
        } catch (InvalidProtocolBufferException e) {
            log.error("接口限制url名单缓存删除失败",e);
            e.printStackTrace();
        }
    }
}
