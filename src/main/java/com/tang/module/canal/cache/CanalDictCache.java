package com.tang.module.canal.cache;

import com.alibaba.otter.canal.protocol.CanalEntry;
import com.google.protobuf.InvalidProtocolBufferException;
import com.tang.common.annotation.canal.CanalDeletePoint;
import com.tang.common.annotation.canal.CanalEventListener;
import com.tang.common.annotation.canal.CanalInsertPoint;
import com.tang.common.annotation.canal.CanalUpdatePoint;
import com.tang.common.constant.cahce.BusinessCacheConstant;
import com.tang.common.resources.CanalCacheEventHandling;
import com.tang.module.system.entity.Dict;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;

/**
 * 系统字典缓存
 * @author tang
 * @date 2022/1/20 11:44
 */
@Slf4j
@CanalEventListener
public class CanalDictCache implements CanalCacheEventHandling {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;


    @Override
    @CanalInsertPoint(database = "tao",tableName = "tao_dict")
    public void insertDealWith(CanalEntry.Entry entry) {
        try {
            CanalEntry.RowChange rowChange = CanalEntry.RowChange.parseFrom(entry.getStoreValue());
            for (CanalEntry.RowData rowData :  rowChange.getRowDatasList()) {
                //获取实体
                Dict dict = parameterInverseSequence(rowData.getAfterColumnsList(), Dict.class);
                //新增系统参数缓存,哈希结构
                redisTemplate.opsForHash().put(BusinessCacheConstant.SYSTEM_DICTIONARY_PREFIX+ dict.getCode(),dict.getDictKey(),dict);
            }
        } catch (InvalidProtocolBufferException e) {
            log.error("系统字典缓存新增失败",e);
            e.printStackTrace();
        }
    }

    @Override
    @CanalUpdatePoint(database = "tao",tableName = "tao_dict")
    public void updateDealWith(CanalEntry.Entry entry) {
        try {
            CanalEntry.RowChange rowChange = CanalEntry.RowChange.parseFrom(entry.getStoreValue());
            for (CanalEntry.RowData rowData :  rowChange.getRowDatasList()) {
                //转换成实体对象
                Dict dictBefore = parameterInverseSequence(rowData.getBeforeColumnsList(),Dict.class);
                Dict dictAfter = parameterInverseSequence(rowData.getAfterColumnsList(),Dict.class);
                //首先判断是否修改了code
                if (! dictBefore.getCode().equals(dictAfter.getCode())){
                    //改名
                    redisTemplate.rename(BusinessCacheConstant.SYSTEM_DICTIONARY_PREFIX+ dictBefore.getCode(), BusinessCacheConstant.SYSTEM_DICTIONARY_PREFIX+ dictAfter.getCode());
                }
                //是否修改了key
                if (! dictAfter.getDictKey().equals(dictBefore.getDictKey())){
                    //删除之前的再hash内存储的key
                    redisTemplate.opsForHash().delete(BusinessCacheConstant.SYSTEM_DICTIONARY_PREFIX+ dictAfter.getCode(),dictBefore.getDictKey());
                }
                //直接更新
                redisTemplate.opsForHash().put(BusinessCacheConstant.SYSTEM_DICTIONARY_PREFIX+ dictAfter.getCode(),dictAfter.getDictKey(),dictAfter);
            }
        } catch (InvalidProtocolBufferException e) {
            log.error("系统字典缓存更新失败",e);
            e.printStackTrace();
        }
    }

    @Override
    @CanalDeletePoint(database = "tao", tableName = "tao_dict")
    public void deleteDealWith(CanalEntry.Entry entry) {
        try {
            CanalEntry.RowChange rowChange = CanalEntry.RowChange.parseFrom(entry.getStoreValue());
            for (CanalEntry.RowData rowData :  rowChange.getRowDatasList()) {
                //转换成实体对象
                Dict dict = parameterInverseSequence(rowData.getBeforeColumnsList(),Dict.class);
                //删除系统参数缓存
                redisTemplate.opsForHash().delete(BusinessCacheConstant.SYSTEM_DICTIONARY_PREFIX+ dict.getCode(),dict.getDictKey());
            }
        } catch (InvalidProtocolBufferException e) {
            log.error("系统字典缓存删除失败",e);
            e.printStackTrace();
        }
    }
}
