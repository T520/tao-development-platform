package com.tang.module.canal.cache;

import com.alibaba.otter.canal.protocol.CanalEntry;
import com.google.protobuf.InvalidProtocolBufferException;
import com.tang.common.annotation.canal.CanalDeletePoint;
import com.tang.common.annotation.canal.CanalEventListener;
import com.tang.common.annotation.canal.CanalInsertPoint;
import com.tang.common.annotation.canal.CanalUpdatePoint;
import com.tang.common.constant.cahce.BusinessCacheConstant;
import com.tang.common.resources.CanalCacheEventHandling;
import com.tang.module.system.entity.Param;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;


/**
 * 处理参数表，同步缓存
 * @author tang
 * @date 2022/1/19 13:54
 */
@Slf4j
@CanalEventListener
public class CanalParamCache implements CanalCacheEventHandling {


    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    @CanalInsertPoint(database = "tao",tableName = "tao_param")
    public void insertDealWith(CanalEntry.Entry entry) {
        try {
            CanalEntry.RowChange rowChange = CanalEntry.RowChange.parseFrom(entry.getStoreValue());
            for (CanalEntry.RowData rowData :  rowChange.getRowDatasList()) {
                //获取实体
                Param param = parameterInverseSequence(rowData.getAfterColumnsList(),Param.class);
                //新增系统参数缓存
                redisTemplate.opsForValue().set(BusinessCacheConstant.PARAM_PREFIX_PARAM + param.getParamKey(), param);
            }
        } catch (InvalidProtocolBufferException e) {
            log.error("系统参数表缓存新增失败",e);
            e.printStackTrace();
        }
    }

    @Override
    @CanalUpdatePoint(database = "tao",tableName = "tao_param")
    public void updateDealWith(CanalEntry.Entry entry) {
        try {
            CanalEntry.RowChange rowChange = CanalEntry.RowChange.parseFrom(entry.getStoreValue());
            for (CanalEntry.RowData rowData :  rowChange.getRowDatasList()) {
                //转换成实体对象
                Param paramBefore = parameterInverseSequence(rowData.getBeforeColumnsList(),Param.class);
                Param paramAfter = parameterInverseSequence(rowData.getAfterColumnsList(),Param.class);
                //首先判断是否修改了key
                if (! paramAfter.getParamKey().equals(paramBefore.getParamKey())){
                    //删除原有数据,再新增
                    redisTemplate.delete(BusinessCacheConstant.PARAM_PREFIX_PARAM + paramBefore.getParamKey());
                    redisTemplate.opsForValue().set(BusinessCacheConstant.PARAM_PREFIX_PARAM + paramAfter.getParamKey(),paramAfter);
                    return;
                }
                //是否逻辑删除
                if (paramAfter.getIsDeleted() == 1){
                    redisTemplate.delete(BusinessCacheConstant.PARAM_PREFIX_PARAM + paramBefore.getParamKey());
                    return;
                }
                //直接更新
                redisTemplate.opsForValue().set(BusinessCacheConstant.PARAM_PREFIX_PARAM + paramBefore.getParamKey(),paramAfter);
            }
        } catch (InvalidProtocolBufferException e) {
            log.error("系统参数表缓存更新失败",e);
            e.printStackTrace();
        }
    }

    @Override
    @CanalDeletePoint(database = "tao",tableName = "tao_param")
    public void deleteDealWith(CanalEntry.Entry entry) {
        try {
            CanalEntry.RowChange rowChange = CanalEntry.RowChange.parseFrom(entry.getStoreValue());
            for (CanalEntry.RowData rowData :  rowChange.getRowDatasList()) {
                //转换成实体对象
                Param param = parameterInverseSequence(rowData.getBeforeColumnsList(),Param.class);
                //删除系统参数缓存
                redisTemplate.delete(BusinessCacheConstant.PARAM_PREFIX_PARAM + param.getParamKey());
            }
        } catch (InvalidProtocolBufferException e) {
            log.error("系统参数表缓存删除失败",e);
            e.printStackTrace();
        }
    }
}
