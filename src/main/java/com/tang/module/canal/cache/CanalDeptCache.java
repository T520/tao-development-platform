package com.tang.module.canal.cache;

import com.alibaba.otter.canal.protocol.CanalEntry;
import com.google.protobuf.InvalidProtocolBufferException;
import com.tang.common.annotation.canal.CanalDeletePoint;
import com.tang.common.annotation.canal.CanalEventListener;
import com.tang.common.annotation.canal.CanalInsertPoint;
import com.tang.common.annotation.canal.CanalUpdatePoint;
import com.tang.common.constant.cahce.BusinessCacheConstant;
import com.tang.common.resources.CanalCacheEventHandling;
import com.tang.module.system.entity.Dept;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;

/**
 * 部门缓存
 * @author tang
 * @date 2022/1/20 11:44
 */
@Slf4j
@CanalEventListener
public class CanalDeptCache implements CanalCacheEventHandling {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;


    @Override
    @CanalInsertPoint(database = "tao",tableName = "tao_dept")
    public void insertDealWith(CanalEntry.Entry entry) {
        try {
            CanalEntry.RowChange rowChange = CanalEntry.RowChange.parseFrom(entry.getStoreValue());
            for (CanalEntry.RowData rowData :  rowChange.getRowDatasList()) {
                //获取实体
                Dept dept = parameterInverseSequence(rowData.getAfterColumnsList(), Dept.class);
                //新增系统参数缓存,哈希结构
                redisTemplate.opsForHash().put(BusinessCacheConstant.DEPT_PREFIX,dept.getId().toString(),dept.getDeptName());
            }
        } catch (InvalidProtocolBufferException e) {
            log.error("部门字典缓存新增失败",e);
            e.printStackTrace();
        }
    }

    @Override
    @CanalUpdatePoint(database = "tao",tableName = "tao_dept")
    public void updateDealWith(CanalEntry.Entry entry) {
        try {
            CanalEntry.RowChange rowChange = CanalEntry.RowChange.parseFrom(entry.getStoreValue());
            for (CanalEntry.RowData rowData :  rowChange.getRowDatasList()) {
                //转换成实体对象
                //Dept deptBefore = parameterInverseSequence(rowData.getBeforeColumnsList(),Dept.class);
                Dept deptAfter = parameterInverseSequence(rowData.getAfterColumnsList(),Dept.class);
                //是否逻辑删除
                if (deptAfter.getIsDeleted() == 1){
                    redisTemplate.opsForHash().delete(BusinessCacheConstant.DEPT_PREFIX,deptAfter.getId());
                    return;
                }
                //直接更新
                redisTemplate.opsForHash().put(BusinessCacheConstant.DEPT_PREFIX,deptAfter.getId().toString(),deptAfter.getDeptName());
            }
        } catch (InvalidProtocolBufferException e) {
            log.error("部门字典缓存更新失败",e);
            e.printStackTrace();
        }
    }

    @Override
    @CanalDeletePoint(database = "tao", tableName = "tao_dept")
    public void deleteDealWith(CanalEntry.Entry entry) {
        try {
            CanalEntry.RowChange rowChange = CanalEntry.RowChange.parseFrom(entry.getStoreValue());
            for (CanalEntry.RowData rowData :  rowChange.getRowDatasList()) {
                //转换成实体对象
                Dept dict = parameterInverseSequence(rowData.getBeforeColumnsList(),Dept.class);
                //删除系统参数缓存
                redisTemplate.opsForHash().delete(BusinessCacheConstant.DEPT_PREFIX,dict.getId().toString());
            }
        } catch (InvalidProtocolBufferException e) {
            log.error("部门缓存删除失败",e);
            e.printStackTrace();
        }
    }
}
