package com.tang.module.canal.cache;

import com.alibaba.otter.canal.protocol.CanalEntry;
import com.google.protobuf.InvalidProtocolBufferException;
import com.tang.common.annotation.canal.CanalEventListener;
import com.tang.common.annotation.canal.CanalInsertPoint;
import com.tang.common.annotation.canal.CanalUpdatePoint;
import com.tang.common.constant.cahce.BusinessCacheConstant;
import com.tang.common.resources.CanalCacheEventHandling;
import com.tang.module.system.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;

/**
 * 只存储用户id和用户名对应关系
 * @author tang
 * @date 2022/2/28 13:49
 */
@Slf4j
@CanalEventListener
public class UserCache implements CanalCacheEventHandling {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    @CanalInsertPoint(database = "tao",tableName = "tao_user")
    public void insertDealWith(CanalEntry.Entry entry) {
        try {
            CanalEntry.RowChange rowChange = CanalEntry.RowChange.parseFrom(entry.getStoreValue());
            for (CanalEntry.RowData rowData :  rowChange.getRowDatasList()) {
                //获取实体
                User user = parameterInverseSequence(rowData.getAfterColumnsList(), User.class);
                user.setPassword("");
                //新增系统参数缓存,哈希结构
                redisTemplate.opsForHash().put(BusinessCacheConstant.USER_PREFIX,user.getId(),user.getName());
            }
        } catch (InvalidProtocolBufferException e) {
            log.error("用户名称缓存新增失败",e);
            e.printStackTrace();
        }
    }

    @Override
    @CanalUpdatePoint(database = "tao",tableName = "tao_user")
    public void updateDealWith(CanalEntry.Entry entry) {
        try {
            CanalEntry.RowChange rowChange = CanalEntry.RowChange.parseFrom(entry.getStoreValue());
            for (CanalEntry.RowData rowData :  rowChange.getRowDatasList()) {
                //转换成实体对象
                User userBefore = parameterInverseSequence(rowData.getBeforeColumnsList(),User.class);
                User userAfter = parameterInverseSequence(rowData.getAfterColumnsList(),User.class);
                //首先判断是否修改了用户名
                if (! userAfter.getName().equals(userBefore.getName())){
                    redisTemplate.opsForValue().set(BusinessCacheConstant.USER_PREFIX + userBefore.getId(),userAfter.getName());
                    return;
                }
                //判断是否为逻辑删除
                if (userAfter.getIsDeleted() == 1){
                    redisTemplate.opsForHash().delete(BusinessCacheConstant.USER_PREFIX,userAfter.getId());
                }
            }
        } catch (InvalidProtocolBufferException e) {
            log.error("用户名称缓存更新失败",e);
            e.printStackTrace();
        }
    }

    @Override
    public void deleteDealWith(CanalEntry.Entry entry) {

    }
}
