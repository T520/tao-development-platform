package com.tang.module.canal.handle;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author tang
 * @date 2021/9/10 13:50
 */
@Component
public class JdbcService {


	@Value("${spring.datasource.url}")
	private String url;

	@Value("${spring.datasource.username}")
	private String username;

	@Value("${spring.datasource.password}")
	private String password;

	public void implement(String sql) throws SQLException {
		Connection conn = DriverManager.getConnection(url,username,password);
		Statement statement = conn.createStatement();
		statement.executeUpdate(sql);
		statement.close();
		conn.close();
	}

}
