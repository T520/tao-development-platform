package com.tang.module.useapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tang.module.useapi.search.UseApiParamSearch;
import com.tang.module.useapi.vo.UseApiParamVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.tang.module.useapi.entity.UseApiParam;

/**
 * 第三方api参数(UseApiParam)数据库访问层
 *
 * @author tang
 * @since 2022-04-02 14:06:36
 */
@Repository
public interface UseApiParamMapper extends BaseMapper<UseApiParam> {

    /**
     * 自定义分页
     * @param page 分页参数
     * @param useApiParamSearch 查询参数
     * @return 分页结果
     */
    IPage<UseApiParamVo> list(IPage<UseApiParamVo> page, UseApiParamSearch useApiParamSearch);

}
