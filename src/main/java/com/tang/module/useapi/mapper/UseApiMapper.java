package com.tang.module.useapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tang.module.useapi.search.UseApiSearch;
import com.tang.module.useapi.vo.UseApiVo;
import org.springframework.stereotype.Repository;
import com.tang.module.useapi.entity.UseApi;

/**
 * 系统使用的外部接口(UseApi)数据库访问层
 *
 * @author tang
 * @since 2022-04-02 16:49:13
 */
@Repository
public interface UseApiMapper extends BaseMapper<UseApi> {

    /**
     * 自定义分页
     * @param page 分页参数
     * @param useApiSearch 搜索实体
     * @return 结果
     */
    IPage<UseApiVo> list(IPage<UseApiVo> page, UseApiSearch useApiSearch);

}
