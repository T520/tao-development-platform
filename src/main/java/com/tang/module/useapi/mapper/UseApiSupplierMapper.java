package com.tang.module.useapi.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;
import com.tang.module.useapi.entity.UseApiSupplier;

/**
 * 第三方厂商(UseApiSupplier)数据库访问层
 *
 * @author tang
 * @since 2022-04-02 14:06:36
 */
@Repository
public interface UseApiSupplierMapper extends BaseMapper<UseApiSupplier> {

}
