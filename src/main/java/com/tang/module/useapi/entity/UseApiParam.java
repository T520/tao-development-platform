package com.tang.module.useapi.entity;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.tang.common.resources.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.EqualsAndHashCode;

/**
 * 第三方api参数(UseApiParam)实体类
 *
 * @author tang
 * @since 2022-04-02 14:06:36
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "tao_use_api_param 实体类",description = "tao_use_api_param 实体类")
@TableName("tao_use_api_param")
public class UseApiParam extends BaseEntity {

    
    /**
    * 供应商id
    */    
    @TableField("supplier_id")
    @ApiModelProperty(value = "供应商id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long supplierId;
    
    /**
    * 参数名
    */    
    @TableField("param_name")
    @ApiModelProperty(value = "参数名")
    private String paramName;
    
    /**
    * 参数键
    */    
    @TableField("param_key")
    @ApiModelProperty(value = "参数键")
    private String paramKey;
    
    /**
    * 参数值
    */    
    @TableField("param_value")
    @ApiModelProperty(value = "参数值")
    private String paramValue;
    
    /**
    * 备注
    */    
    @TableField("remark")
    @ApiModelProperty(value = "备注")
    private String remark;

     
}
