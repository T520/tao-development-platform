package com.tang.module.useapi.entity;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.tang.common.resources.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import lombok.EqualsAndHashCode;

/**
 * 系统使用的外部接口(UseApi)实体类
 *
 * @author tang
 * @since 2022-04-02 16:49:13
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "tao_use_api 实体类",description = "tao_use_api 实体类")
@TableName("tao_use_api")
public class UseApi extends BaseEntity {

    
    /**
    * 接口名称
    */    
    @TableField("name")
    @ApiModelProperty(value = "接口名称")
    private String name;
    
    /**
    * 父ID
    */    
    @TableField("parent_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "父ID")
    private Long parentId;

    /**
     * 供应商id
     */
    @TableField("supplier_id")
    @ApiModelProperty(value = "供应商id")
    private String supplierId;
    
    /**
    * 启用
    */    
    @TableField("enable")
    @ApiModelProperty(value = "启用")
    private Integer enable;

    /**
     * 接口bean名称
     */
    @TableField("bean_name")
    @ApiModelProperty(value = "接口bean名称")
    private String beanName;
    
    /**
    * 请求方法类型
    */    
    @TableField("method_type")
    @ApiModelProperty(value = "请求方法类型")
    private Long methodType;
    
    /**
    * 后端url
    */    
    @TableField("url")
    @ApiModelProperty(value = "后端url")
    private String url;
    
    /**
    * 备注
    */    
    @TableField("remark")
    @ApiModelProperty(value = "备注")
    private String remark;

    

     
}
