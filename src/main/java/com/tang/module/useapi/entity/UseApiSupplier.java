package com.tang.module.useapi.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.*;
import com.tang.common.resources.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 第三方厂商(UseApiSupplier)实体类
 *
 * @author tang
 * @since 2022-04-02 14:06:37
 */
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "tao_use_api_supplier 实体类",description = "tao_use_api_supplier 实体类")
@TableName("tao_use_api_supplier")
public class UseApiSupplier extends BaseEntity {
    

    
    /**
    * 供应商名称
    */    
    @TableField(value = "supplier_name",condition = SqlCondition.LIKE)
    @ApiModelProperty(value = "供应商名称")
    private String supplierName;
    
    /**
    * 统一社会信用代码
    */    
    @TableField("credit_code")
    @ApiModelProperty(value = "统一社会信用代码")
    private String creditCode;
    
    /**
    * 备注
    */    
    @TableField("remark")
    @ApiModelProperty(value = "备注")
    private String remark;

}
