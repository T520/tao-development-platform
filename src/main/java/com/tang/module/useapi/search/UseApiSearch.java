package com.tang.module.useapi.search;

import com.tang.module.useapi.entity.UseApi;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;

/**
 * 系统使用的外部接口(UseApi)实体搜索对象
 *
 * @author tang
 * @since 2022-04-02 16:49:13
 */
@ApiModel(value = "UseApiSearch搜索对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class UseApiSearch extends UseApi {
    
}
