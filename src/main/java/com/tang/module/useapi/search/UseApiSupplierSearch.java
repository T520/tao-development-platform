package com.tang.module.useapi.search;

import com.tang.module.useapi.entity.UseApiSupplier;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;

/**
 * 第三方api厂商(UseApiSupplier)实体搜索对象
 *
 * @author tang
 * @since 2022-04-02 14:06:37
 */
@ApiModel(value = "UseApiSupplierSearch搜索对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class UseApiSupplierSearch extends UseApiSupplier {
    
}
