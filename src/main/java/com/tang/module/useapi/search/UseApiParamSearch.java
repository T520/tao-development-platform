package com.tang.module.useapi.search;

import com.tang.module.useapi.entity.UseApiParam;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;

/**
 * 第三方api参数(UseApiParam)实体搜索对象
 *
 * @author tang
 * @since 2022-04-02 14:06:36
 */
@ApiModel(value = "UseApiParamSearch搜索对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class UseApiParamSearch extends UseApiParam {
    
}
