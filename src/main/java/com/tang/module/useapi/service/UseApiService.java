package com.tang.module.useapi.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tang.module.useapi.entity.UseApi;
import com.tang.module.useapi.search.UseApiSearch;
import com.tang.module.useapi.vo.UseApiVo;

import java.util.List;

/**
 * 系统使用的外部接口(UseApi)表服务接口
 *
 * @author tang
 * @since 2022-04-02 16:49:13
 */
public interface UseApiService extends IService<UseApi> {


    /**
     * 自定义分页
     * @param page 分页参数
     * @param useApiSearch 查询参数
     * @return 分页结果
     */
    public IPage<UseApiVo> list(IPage<UseApiVo> page, UseApiSearch useApiSearch);

    /**
     * 构建树
     * @param useApiSearch 搜索条件
     * @return 结果
     */
    public List<UseApiVo> tree(UseApiSearch useApiSearch);

    /**
     * 创建树节点
     * @param pid 父ID
     * @param list 数据结合
     * @return 节点
     */
    public List<UseApiVo> createTree(Long pid,List<UseApiVo> list);

    /**
     * API启用状态变化
     * @param useApi 变化的api实体
     * @return 变化是否成功
     */
    public boolean status(UseApi useApi);

}
