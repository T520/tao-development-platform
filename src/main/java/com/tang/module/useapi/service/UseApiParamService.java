package com.tang.module.useapi.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.tang.module.useapi.entity.UseApiParam;
import com.tang.module.useapi.search.UseApiParamSearch;
import com.tang.module.useapi.vo.UseApiParamVo;

/**
 * 第三方api参数(UseApiParam)表服务接口
 *
 * @author tang
 * @since 2022-04-02 14:06:36
 */
public interface UseApiParamService extends IService<UseApiParam> {

    /**
     * 自定义分页
     * @param page 分页参数
     * @param useApiParamSearch 查询参数
     * @return 分页结果
     */
    public IPage<UseApiParamVo> list(IPage<UseApiParamVo> page, UseApiParamSearch useApiParamSearch);
}
