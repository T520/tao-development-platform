package com.tang.module.useapi.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tang.module.useapi.entity.UseApiSupplier;

/**
 * 第三方厂商(UseApiSupplier)表服务接口
 *
 * @author tang
 * @since 2022-04-02 14:06:37
 */
public interface UseApiSupplierService extends IService<UseApiSupplier> {

}
