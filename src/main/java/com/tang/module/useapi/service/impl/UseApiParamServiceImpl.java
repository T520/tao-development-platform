package com.tang.module.useapi.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tang.module.useapi.mapper.UseApiParamMapper;
import com.tang.module.useapi.entity.UseApiParam;
import com.tang.module.useapi.search.UseApiParamSearch;
import com.tang.module.useapi.service.UseApiParamService;
import com.tang.module.useapi.vo.UseApiParamVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 第三方api参数(UseApiParam)表服务实现类
 *
 * @author tang
 * @since 2022-04-02 14:06:36
 */
@Service("useApiParamService")
public class UseApiParamServiceImpl extends ServiceImpl<UseApiParamMapper, UseApiParam> implements UseApiParamService {

    @Resource
    private  UseApiParamMapper useApiParamMapper;

    @Override
    public IPage<UseApiParamVo> list(IPage<UseApiParamVo> page, UseApiParamSearch useApiParamSearch) {
        return useApiParamMapper.list(page, useApiParamSearch);
    }
}
