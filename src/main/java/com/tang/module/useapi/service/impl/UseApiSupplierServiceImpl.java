package com.tang.module.useapi.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tang.module.useapi.mapper.UseApiSupplierMapper;
import com.tang.module.useapi.entity.UseApiSupplier;
import com.tang.module.useapi.service.UseApiSupplierService;
import org.springframework.stereotype.Service;

/**
 * 第三方厂商(UseApiSupplier)表服务实现类
 *
 * @author tang
 * @since 2022-04-02 14:06:37
 */
@Service("useApiSupplierService")
public class UseApiSupplierServiceImpl extends ServiceImpl<UseApiSupplierMapper, UseApiSupplier> implements UseApiSupplierService {

}
