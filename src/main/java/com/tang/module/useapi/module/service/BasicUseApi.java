package com.tang.module.useapi.module.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.tang.common.exception.GlobalException;
import com.tang.module.useapi.module.entity.IpLocation;
import com.tang.module.useapi.entity.UseApi;

import java.util.List;
import java.util.Map;

/**
 * 基础API接口,包含各种常用接口
 * 策略模式
 *  说明：定义一系列的算法,把每一个算法封装起来, 并且使它们可相互切换
 *  策略抽象接口: BasicUseApi
 *  策略的实现：GaoDeBasicUseApi(高德)，TenXunBasicUseApi（腾讯）
 *  策略应用类：BasicUseApiResources（提供数据库种的bean名称获取对应的策略实现类）
 * @author tang
 * @date 2022/2/14 14:52
 */
public interface BasicUseApi {

    /**
     * 通过ip获取位置信息
     * @param ip ipv4或者ipv6
     * @return 位置实体
     */
    IpLocation byIpGetLocation(String ip);


    /**
     * 获得当前生效的BasicUseApi的bean
     * @param interfaceName 接口名称
     * @param useApi 当前生效的接口配置列表，原则上只能有一个
     * @return 当前生效的BasicUseApi的bean
     */
    default BasicUseApi getBasicUseApi(String interfaceName, UseApi useApi){
        if (BeanUtil.isEmpty(useApi)) {
            throw new GlobalException("未找到生效的接口,当前接口名称: " + interfaceName);
        }
        //获得生效的接口
        //获得BasicUseApi类型的全部bean
        Map<String, BasicUseApi> basicUseApiMap = SpringUtil.getBeansOfType(BasicUseApi.class);
        //获得当前生效的basicUseApi
        BasicUseApi basicUseApi = basicUseApiMap.get(useApi.getBeanName());
        if (basicUseApi == null){
            throw new GlobalException("通过生效的配置获取bean失败,当前生效bean名称: " + useApi.getBeanName());
        }
        return basicUseApi;
    }
}
