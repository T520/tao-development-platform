package com.tang.module.useapi.module.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * ip定位实体
 * @author tang
 * @date 2022/2/14 14:55
 */
@Data
@ApiModel(value = "ip定位实体")
public class IpLocation implements Serializable {


    @ApiModelProperty(value = "国家")
    private String country;

    @ApiModelProperty(value = "省份")
    private String province;

    @ApiModelProperty(value = "市")
    private String city;

    @ApiModelProperty(value = "区县")
    private String district;

    @ApiModelProperty(value = "运营商")
    private String isp;

    @ApiModelProperty(value = "经纬度")
    private String location;

    @ApiModelProperty("ip地址")
    private String  ip;

}
