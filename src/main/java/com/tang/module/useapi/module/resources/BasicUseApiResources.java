package com.tang.module.useapi.module.resources;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.tang.common.constant.UseApiConstant;
import com.tang.module.useapi.module.entity.IpLocation;
import com.tang.module.useapi.entity.UseApi;
import com.tang.module.useapi.mapper.UseApiMapper;
import com.tang.module.useapi.module.service.BasicUseApi;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 基础API接口资源
 * @author tang
 * @date 2022/4/8 10:23
 */
@Service
public class  BasicUseApiResources implements BasicUseApi{

    @Resource
    private UseApiMapper useApiMapper;


    @Override
    public IpLocation byIpGetLocation(String ip) {
        //获得当前生效的接口配置
        UseApi useApi = useApiMapper.selectOne(Wrappers.<UseApi>lambdaQuery().eq(UseApi::getName, UseApiConstant.IP).eq(UseApi::getEnable, 1));
        BasicUseApi basicUseApi = getBasicUseApi(UseApiConstant.IP,useApi);
        //执行实际方法
        return basicUseApi.byIpGetLocation(ip);
    }

}
