package com.tang.module.useapi.module.service.impl;

import cn.hutool.core.util.URLUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.tang.common.constant.UniversalConstant;
import com.tang.common.exception.GlobalException;
import com.tang.common.utils.HttpRequestBaseUtil;
import com.tang.module.useapi.module.entity.IpLocation;
import com.tang.module.useapi.entity.UseApiParam;
import com.tang.module.useapi.entity.UseApiSupplier;
import com.tang.module.useapi.mapper.UseApiParamMapper;
import com.tang.module.useapi.mapper.UseApiSupplierMapper;
import com.tang.module.useapi.module.service.BasicUseApi;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.tang.common.constant.UseParamConstant.*;

/**
 * 腾讯基本接口
 * @author tang
 * @date 2022/4/7 17:09
 */
@Service
public class TenXunBasicUseApi implements BasicUseApi{

    @Resource
    private UseApiParamMapper useApiParamMapper;

    @Resource
    private UseApiSupplierMapper useApiSupplierMapper;

    @Override
    public IpLocation byIpGetLocation(String ip) {
        //获得API对应的厂商
        UseApiSupplier useApiSupplier = useApiSupplierMapper.selectOne(Wrappers.<UseApiSupplier>lambdaQuery().select(UseApiSupplier::getId).eq(UseApiSupplier::getCreditCode, TEN_XUN_CREDIT_CODE));
        if (useApiSupplier == null){
            throw new GlobalException("无法通过供应商社会信用代码获取供应商数据");
        }
        //获得定义的供应商参数
        List<UseApiParam> paramList = useApiParamMapper.selectList(Wrappers.<UseApiParam>lambdaQuery().eq(UseApiParam::getSupplierId, useApiSupplier.getId()));
        //获得供应商参数map
        Map<String, String> paramMap = paramList.parallelStream().collect(Collectors.toMap(UseApiParam::getParamKey, UseApiParam::getParamValue));
        Map<String, String> requestMap = new HashMap<>();
        requestMap.put("ip",ip);
        requestMap.put("key",paramMap.get(APP_KEY_WEB));
        //开始计算签名
        String s = URLUtil.getPath(paramMap.get(T_IP_ADDRESS_URL)) + "?" + HttpUtil.toParams(requestMap) + paramMap.get(T_WEB_AUTOGRAPH);
        //进行md5加密
        String sig = SecureUtil.md5(s);
        requestMap.put("sig",sig);
        JSONObject map = HttpRequestBaseUtil.get(
                paramMap.get(T_IP_ADDRESS_URL), requestMap, JSONObject.class, null,"IP定位");
        //不为0时，表示请求有错误
        if (map.getInteger(UniversalConstant.STATUS) != 0){
            throw new GlobalException("调用腾讯ip定位失败,原因: " + map.get("message"));
        }
        //获得结果
        Map<String, Object> result =  map.getJSONObject("result").getInnerMap();
        LinkedHashMap<String, String> adInfo = (LinkedHashMap<String, String>) result.get("ad_info");
        IpLocation ipLocation = JSON.parseObject(JSON.toJSONString(adInfo), IpLocation.class);
        //设置国家
        ipLocation.setCountry(adInfo.get("nation"));
        //设置ip
        ipLocation.setIp(result.get("ip").toString());
        return ipLocation;
    }
}
