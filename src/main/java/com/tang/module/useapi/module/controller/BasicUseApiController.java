package com.tang.module.useapi.module.controller;

import com.tang.module.useapi.module.service.impl.GaoDeBasicUseApi;
import com.tang.module.useapi.module.service.impl.TenXunBasicUseApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 基础API接口
 * @author tang
 * @date 2022/4/7 16:31
 */
@RestController
@Api(tags = "基础API接口")
@RequestMapping("/use")
public class BasicUseApiController {

    @Resource
    private GaoDeBasicUseApi gaoDeBasicUseApi;

    @Resource
    private TenXunBasicUseApi tenXunBasicUseApi;

    @GetMapping("/g/ip")
    @ApiOperation(value = "高德ip定位")
    public Object gip(String ip){
        return gaoDeBasicUseApi.byIpGetLocation(ip);
    }

    @GetMapping("/t/ip")
    @ApiOperation(value = "腾讯ip定位")
    public Object tip(String ip){
        return tenXunBasicUseApi.byIpGetLocation(ip);
    }




}
