package com.tang.module.useapi.wrapper;



import com.tang.module.useapi.entity.UseApi;
import com.tang.common.resources.BaseEntityWrapper;
import com.tang.module.useapi.vo.UseApiVo;
import org.springframework.beans.BeanUtils;

/**
 * 系统使用的外部接口(UseApi)数据库转换层
 *
 * @author tang
 * @since 2022-04-02 16:49:13
 */
public class UseApiWrapper extends BaseEntityWrapper<UseApi,UseApiVo> {
    
    public static  UseApiWrapper build(){
        return new UseApiWrapper();
    }
    
    @Override
    public  UseApiVo wrapper(UseApi useApi){
      UseApiVo useApiVo = new UseApiVo();
      //属性赋值
      BeanUtils.copyProperties(useApi,useApiVo);
      return useApiVo;
    }
}
