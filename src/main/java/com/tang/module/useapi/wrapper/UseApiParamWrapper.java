package com.tang.module.useapi.wrapper;



import com.tang.module.useapi.entity.UseApiParam;
import com.tang.common.resources.BaseEntityWrapper;
import com.tang.module.useapi.vo.UseApiParamVo;
import org.springframework.beans.BeanUtils;

/**
 * 第三方api参数(UseApiParam)数据库转换层
 *
 * @author tang
 * @since 2022-04-02 14:06:36
 */
public class UseApiParamWrapper extends BaseEntityWrapper<UseApiParam,UseApiParamVo> {
    
    public static  UseApiParamWrapper build(){
        return new UseApiParamWrapper();
    }
    
    @Override
    public  UseApiParamVo wrapper(UseApiParam useApiParam){
      UseApiParamVo useApiParamVo = new UseApiParamVo();
      //属性赋值
      BeanUtils.copyProperties(useApiParam,useApiParamVo);
      return useApiParamVo;
    }
}
