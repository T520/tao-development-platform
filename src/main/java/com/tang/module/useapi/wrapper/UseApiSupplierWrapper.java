package com.tang.module.useapi.wrapper;



import com.tang.module.useapi.entity.UseApiSupplier;
import com.tang.common.resources.BaseEntityWrapper;
import com.tang.module.useapi.vo.UseApiSupplierVo;
import org.springframework.beans.BeanUtils;

/**
 * 第三方api厂商(UseApiSupplier)数据库转换层
 *
 * @author tang
 * @since 2022-04-02 14:06:37
 */
public class UseApiSupplierWrapper extends BaseEntityWrapper<UseApiSupplier,UseApiSupplierVo> {
    
    public static  UseApiSupplierWrapper build(){
        return new UseApiSupplierWrapper();
    }
    
    @Override
    public  UseApiSupplierVo wrapper(UseApiSupplier useApiSupplier){
      UseApiSupplierVo useApiSupplierVo = new UseApiSupplierVo();
      //属性赋值
      BeanUtils.copyProperties(useApiSupplier,useApiSupplierVo);
      return useApiSupplierVo;
    }
}
