package com.tang.module.useapi.vo;

import com.tang.module.useapi.entity.UseApiParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Data;



/**
 * 第三方api参数(UseApiParam)vo对象实体对象
 *
 * @author tang
 * @since 2022-04-02 14:06:36
 */
@ApiModel(value = "UseApiParamVo对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class UseApiParamVo extends UseApiParam {

    /**
     * 供应商名称
     */
    @ApiModelProperty(value = "供应商名称")
    private String supplierName;
    
}
