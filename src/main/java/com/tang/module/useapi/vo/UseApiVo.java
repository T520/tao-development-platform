package com.tang.module.useapi.vo;

import com.tang.module.useapi.entity.UseApi;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.Data;
import lombok.ToString;

import java.util.List;


/**
 * 系统使用的外部接口(UseApi)vo对象实体对象
 *
 * @author tang
 * @since 2022-04-02 16:49:13
 */
@ApiModel(value = "UseApiVo对象")
@EqualsAndHashCode(callSuper = true)
@Data
@ToString
public class UseApiVo extends UseApi {

    /**
     * 供应商名称
     */
    @ApiModelProperty(value = "供应商名称")
    private String supplierName;

    /**
     *  请求方法类型名称
     */
    private String methodTypeName;

    /**
     * 孩子
     */
    @ApiModelProperty(value = "子节点")
    private List<UseApiVo> children;

}
