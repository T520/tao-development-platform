package com.tang.module.useapi.vo;

import com.tang.module.useapi.entity.UseApiSupplier;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;



/**
 * 第三方api厂商(UseApiSupplier)vo对象实体对象
 *
 * @author tang
 * @since 2022-04-02 14:06:37
 */
@ApiModel(value = "UseApiSupplierVo对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class UseApiSupplierVo extends UseApiSupplier {
    
}
