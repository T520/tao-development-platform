package com.tang.module.useapi.controller;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tang.common.annotation.ApiLog;
import com.tang.common.utils.ExportExcelUtils;
import com.tang.common.resources.Result;
import com.tang.common.resources.TaoPage;
import com.tang.module.useapi.entity.UseApiParam;
import com.tang.module.useapi.search.UseApiParamSearch;
import com.tang.module.useapi.service.UseApiParamService;
import com.tang.module.useapi.vo.UseApiParamVo;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import io.swagger.annotations.ApiOperation;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * 第三方api参数(UseApiParam)控制层
 *
 * @author tang
 * @since 2022-04-02 14:06:36
 */
@RestController
@RequestMapping("useApiParam")
@Api(tags = "useApiParam控制层")
public class UseApiParamController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private UseApiParamService useApiParamService;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @param  useApiParamSearch 查询实体
     * @return 所有数据
     */
    @ApiLog(type = 9,title = "分页查询")
    @GetMapping()
    @ApiOperation(value = "分页查询")
    public Result<Page<UseApiParam>> selectAll(@Valid TaoPage page, UseApiParamSearch useApiParamSearch) {
        return Result.data(this.useApiParamService.page(new Page<>(page.getCurrent(), page.getSize()), new QueryWrapper<>(useApiParamSearch)));
    }

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @param  useApiParamSearch 查询实体
     * @return 所有数据
     */
    @ApiLog(type = 9,title = "自定义分页查询")
    @GetMapping("/list")
    @ApiOperation(value = "分页查询")
    public Result<IPage<UseApiParamVo>> list(@Valid TaoPage page, UseApiParamSearch useApiParamSearch) {
        return Result.data(this.useApiParamService.list(new Page<>(page.getCurrent(), page.getSize()), useApiParamSearch));
    }



    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @ApiLog(type = 9,title = "主键查询")
    @GetMapping("one")
    @ApiOperation(value = "主键查询")
    public Result<UseApiParam> selectOne(Serializable id) {
        return Result.data(this.useApiParamService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param useApiParam 实体对象
     * @return 新增结果
     */
    @ApiLog(type = 9,title = "新增")
    @PostMapping
    @ApiOperation(value = "新增")
    public Result<Boolean> insert(@RequestBody UseApiParam useApiParam) {
        return Result.status(this.useApiParamService.save(useApiParam));
    }

    /**
     * 修改数据
     *
     * @param useApiParam 实体对象
     * @return 修改结果
     */
    @ApiLog(type = 9,title = "修改")
    @PostMapping("update")
    @ApiOperation(value = "修改")
    public Result<Boolean> update(@RequestBody UseApiParam useApiParam) {
        return Result.status(this.useApiParamService.updateById(useApiParam));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @ApiLog(type = 9,title = "删除")
    @DeleteMapping("delete")
    @ApiOperation(value = "删除")
    public Result<Boolean> delete(@RequestParam("idList") List<Long> idList) {
        return Result.status(this.useApiParamService.removeByIds(idList));
    }

    /**
     * excel导出
     */
    @ApiLog(type = 9,title = "excel导出")
    @ApiOperation("excel导出")
    @GetMapping("/importExcel")
    public void excel(HttpServletResponse response) throws IOException {
        ExportExcelUtils.entityExcel(useApiParamService.list(),"API请求常量.xlsx",response);
    }
}
