package com.tang.module.useapi.controller;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tang.common.annotation.ApiLog;
import com.tang.common.utils.ExportExcelUtils;
import com.tang.common.resources.Result;
import com.tang.common.resources.TaoPage;
import com.tang.module.useapi.entity.UseApiSupplier;
import com.tang.module.useapi.search.UseApiSupplierSearch;
import com.tang.module.useapi.service.UseApiSupplierService;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import io.swagger.annotations.ApiOperation;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * 第三方厂商(UseApiSupplier)控制层
 *
 * @author tang
 * @since 2022-04-02 14:06:36
 */
@RestController
@RequestMapping("useApiSupplier")
@Api(tags = "useApiSupplier控制层")
public class UseApiSupplierController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private UseApiSupplierService useApiSupplierService;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @param  useApiSupplierSearch 查询实体
     * @return 所有数据
     */
    @ApiLog(type = 10,title = "分页查询")
    @GetMapping()
    @ApiOperation(value = "分页查询")
    public Result<Page<UseApiSupplier>> selectAll(@Valid TaoPage page, UseApiSupplierSearch useApiSupplierSearch) {
        return Result.data(this.useApiSupplierService.page(new Page<>(page.getCurrent(), page.getSize()), new QueryWrapper<>(useApiSupplierSearch)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @ApiLog(type = 10,title = "主键查询")
    @GetMapping("one")
    @ApiOperation(value = "主键查询")
    public Result<UseApiSupplier> selectOne(Serializable id) {
        return Result.data(this.useApiSupplierService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param useApiSupplier 实体对象
     * @return 新增结果
     */
    @ApiLog(type = 10,title = "新增")
    @PostMapping
    @ApiOperation(value = "新增")
    public Result<Boolean> insert(@RequestBody UseApiSupplier useApiSupplier) {
        return Result.status(this.useApiSupplierService.save(useApiSupplier));
    }

    /**
     * 修改数据
     *
     * @param useApiSupplier 实体对象
     * @return 修改结果
     */
    @ApiLog(type = 10,title = "修改")
    @PostMapping("update")
    @ApiOperation(value = "修改")
    public Result<Boolean> update(@RequestBody UseApiSupplier useApiSupplier) {
        return Result.status(this.useApiSupplierService.updateById(useApiSupplier));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @ApiLog(type = 10,title = "删除")
    @DeleteMapping("delete")
    @ApiOperation(value = "删除")
    public Result<Boolean> delete(@RequestParam("idList") List<Long> idList) {
        return Result.status(this.useApiSupplierService.removeByIds(idList));
    }

    /**
     * excel导出
     */
    @ApiLog(type = 10,title = "excel导出")
    @ApiOperation("excel导出")
    @GetMapping("/importExcel")
    public void excel(HttpServletResponse response) throws IOException {
        ExportExcelUtils.entityExcel(useApiSupplierService.list(),"API供应商.xlsx",response);
    }

    /**
     * 获得供应商名称和id的集合
     * @return 数据
     */
    @ApiLog(type = 10,title = "供应商名称和id")
    @GetMapping("/nameAndId")
    @ApiOperation(value = "供应商名称和id")
    public Result<List<UseApiSupplier>> nameAndId() {
        return Result.data(this.useApiSupplierService.list(Wrappers.<UseApiSupplier>lambdaQuery().
                select(UseApiSupplier::getId,UseApiSupplier::getSupplierName).eq(UseApiSupplier::getStatus,1)));
    }
}
