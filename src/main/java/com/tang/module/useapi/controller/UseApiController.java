package com.tang.module.useapi.controller;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tang.common.annotation.ApiLog;
import com.tang.common.utils.ExportExcelUtils;
import com.tang.common.resources.Result;
import com.tang.common.resources.TaoPage;
import com.tang.module.useapi.entity.UseApi;
import com.tang.module.useapi.search.UseApiSearch;
import com.tang.module.useapi.service.UseApiService;
import com.tang.module.useapi.vo.UseApiVo;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import io.swagger.annotations.ApiOperation;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * 系统使用的外部接口(UseApi)控制层
 *
 * @author tang
 * @since 2022-04-02 16:49:13
 */
@RestController
@RequestMapping("useApi")
@Api(tags = "useApi控制层")
public class UseApiController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private UseApiService useApiService;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @param  useApiSearch 查询实体
     * @return 所有数据
     */
    @ApiLog(type = 8,title = "分页查询")
    @GetMapping()
    @ApiOperation(value = "分页查询")
    public Result<Page<UseApi>> selectAll(@Valid TaoPage page, UseApiSearch useApiSearch) {
        return Result.data(this.useApiService.page(new Page<>(page.getCurrent(), page.getSize()), new QueryWrapper<>(useApiSearch)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @ApiLog(type = 8,title = "主键查询")
    @GetMapping("one")
    @ApiOperation(value = "主键查询")
    public Result<UseApi> selectOne(Serializable id) {
        return Result.data(this.useApiService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param useApi 实体对象
     * @return 新增结果
     */
    @ApiLog(type = 8,title = "新增")
    @PostMapping
    @ApiOperation(value = "新增")
    public Result<Boolean> insert(@RequestBody UseApi useApi) {
        return Result.status(this.useApiService.save(useApi));
    }

    /**
     * 修改数据
     *
     * @param useApi 实体对象
     * @return 修改结果
     */
    @ApiLog(type = 8,title = "修改")
    @PostMapping("update")
    @ApiOperation(value = "修改")
    public Result<Boolean> update(@RequestBody UseApi useApi) {
        return Result.status(this.useApiService.updateById(useApi));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @ApiLog(type = 8,title = "删除")
    @DeleteMapping("delete")
    @ApiOperation(value = "删除")
    public Result<Boolean> delete(@RequestParam("idList") List<Long> idList) {
        return Result.status(this.useApiService.removeByIds(idList));
    }

    /**
     * 树形结构
     *
     * @return 删除结果
     */
    @ApiLog(type = 8,title = "树形结构")
    @GetMapping("tree")
    @ApiOperation(value = "树形结构")
    public Result<List<UseApiVo>> tree(UseApiSearch useApiSearch) {
        return Result.data(this.useApiService.tree(useApiSearch));
    }

    /**
     * excel导出
     */
    @ApiLog(type = 8,title = "excel导出")
    @ApiOperation("excel导出")
    @GetMapping("/importExcel")
    public void excel(HttpServletResponse response) throws IOException {
        ExportExcelUtils.entityExcel(useApiService.list(),"API.xlsx",response);
    }

    /**
     * API启用状态变化
     *
     * @param useApi 实体对象
     * @return 修改结果
     */
    @ApiLog(type = 8,title = "状态变化")
    @PostMapping("status")
    @ApiOperation(value = "状态变化")
    public Result<Boolean> status(@RequestBody UseApi useApi) {
        return Result.status(this.useApiService.status(useApi));
    }
}
