package com.tang.module.notice.vo;

import com.tang.module.notice.entity.NoticeLog;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;



/**
 * 系统消息通知日志(NoticeLog)vo对象实体对象
 *
 * @author 月蚀
 * @since 2022-06-16 17:12:32
 */
@ApiModel(value = "NoticeLogVo对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class NoticeLogVo extends NoticeLog {

    /**
     * 创建人姓名
     */
    private String createUserName;

    /**
     * 消息类型名称
     */
    private String typeName;

    /**
     * 接收用户姓名
     */
    private String receiveUserName;

    /**
     * 接收部门名称
     */
    private String receiveGroupName;

    
}
