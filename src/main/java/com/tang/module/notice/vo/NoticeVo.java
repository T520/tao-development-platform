package com.tang.module.notice.vo;

import com.tang.module.notice.entity.Notice;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;



/**
 * 系统消息通知(Notice)vo对象实体对象
 *
 * @author 月蚀
 * @since 2022-06-16 17:12:32
 */
@ApiModel(value = "NoticeVo对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class NoticeVo extends Notice {

    /**
     * 创建人姓名
     */
    private String createUserName;

    /**
     * 消息类型名称
     */
    private String typeName;

}
