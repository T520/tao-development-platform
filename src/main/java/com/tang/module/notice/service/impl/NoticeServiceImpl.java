package com.tang.module.notice.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tang.module.notice.mapper.NoticeMapper;
import com.tang.module.notice.entity.Notice;
import com.tang.module.notice.service.NoticeService;
import org.springframework.stereotype.Service;

/**
 * 系统消息通知(Notice)表服务实现类
 *
 * @author 月蚀
 * @since 2022-06-16 17:12:32
 */
@Service("noticeService")
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements NoticeService {

}
