package com.tang.module.notice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tang.module.notice.entity.NoticeLog;

/**
 * 系统消息通知日志(NoticeLog)表服务接口
 *
 * @author 月蚀
 * @since 2022-06-16 17:12:32
 */
public interface NoticeLogService extends IService<NoticeLog> {
    
}
