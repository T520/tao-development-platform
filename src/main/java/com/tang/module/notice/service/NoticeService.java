package com.tang.module.notice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tang.module.notice.entity.Notice;

/**
 * 系统消息通知(Notice)表服务接口
 *
 * @author 月蚀
 * @since 2022-06-16 17:12:32
 */
public interface NoticeService extends IService<Notice> {
    
}
