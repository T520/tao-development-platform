package com.tang.module.notice.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tang.module.notice.mapper.NoticeLogMapper;
import com.tang.module.notice.entity.NoticeLog;
import com.tang.module.notice.service.NoticeLogService;
import org.springframework.stereotype.Service;

/**
 * 系统消息通知日志(NoticeLog)表服务实现类
 *
 * @author 月蚀
 * @since 2022-06-16 17:12:32
 */
@Service("noticeLogService")
public class NoticeLogServiceImpl extends ServiceImpl<NoticeLogMapper, NoticeLog> implements NoticeLogService {

}
