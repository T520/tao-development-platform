package com.tang.module.notice.controller;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tang.common.resources.Result;
import com.tang.common.resources.TaoPage;
import com.tang.module.notice.entity.NoticeLog;
import com.tang.module.notice.search.NoticeLogSearch;
import com.tang.module.notice.service.NoticeLogService;
import com.tang.module.notice.vo.NoticeLogVo;
import com.tang.module.notice.wrapper.NoticeLogWrapper;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import javax.validation.Valid;
import io.swagger.annotations.ApiOperation;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * 系统消息通知日志(NoticeLog)控制层
 *
 * @author 月蚀
 * @since 2022-06-16 17:12:32
 */
@RestController
@RequestMapping("noticeLog")
@Api(tags = "noticeLog控制层")
public class NoticeLogController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private NoticeLogService noticeLogService;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @param  noticeLogSearch 查询实体
     * @return 所有数据
     */
    @GetMapping()
    @ApiOperation(value = "分页查询")
    public Result<IPage<NoticeLogVo>> selectAll(@Valid TaoPage page, NoticeLogSearch noticeLogSearch) {
        return Result.data(
                NoticeLogWrapper.build().pageWrapper(
                        this.noticeLogService.page(new Page<>(page.getCurrent(), page.getSize()), new QueryWrapper<>(noticeLogSearch))));

    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("one")
    @ApiOperation(value = "主键查询")
    public Result<NoticeLog> selectOne(Serializable id) {
        return Result.data(this.noticeLogService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param noticeLog 实体对象
     * @return 新增结果
     */
    @PostMapping
    @ApiOperation(value = "新增")
    public Result<Boolean> insert(@RequestBody NoticeLog noticeLog) {
        return Result.status(this.noticeLogService.save(noticeLog));
    }

    /**
     * 修改数据
     *
     * @param noticeLog 实体对象
     * @return 修改结果
     */
    @PutMapping("update")
    @ApiOperation(value = "修改")
    public Result<Boolean> update(@RequestBody NoticeLog noticeLog) {
        return Result.status(this.noticeLogService.updateById(noticeLog));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping("delete")
    @ApiOperation(value = "删除")
    public Result<Boolean> delete(@RequestParam("idList") List<Long> idList) {
        return Result.status(this.noticeLogService.removeByIds(idList));
    }
}
