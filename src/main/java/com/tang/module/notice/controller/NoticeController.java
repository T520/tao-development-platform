package com.tang.module.notice.controller;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tang.common.resources.Result;
import com.tang.common.resources.TaoPage;
import com.tang.common.utils.CommonUtils;
import com.tang.module.notice.entity.Notice;
import com.tang.module.notice.search.NoticeSearch;
import com.tang.module.notice.service.NoticeService;
import com.tang.module.notice.vo.NoticeVo;
import com.tang.module.notice.wrapper.NoticeWrapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import javax.validation.Valid;
import io.swagger.annotations.ApiOperation;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * 系统消息通知(Notice)控制层
 *
 * @author 月蚀
 * @since 2022-06-16 17:12:32
 */
@RestController
@RequestMapping("notice")
@Api(tags = "notice控制层")
public class NoticeController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private NoticeService noticeService;

    /**
     * 分页查询所有数据，先展示未读消息
     *
     * @param page 分页对象
     * @param  noticeSearch 查询实体
     * @return 所有数据
     */
    @GetMapping()
    @ApiOperation(value = "分页查询")
    public Result<IPage<NoticeVo>> selectAll(@Valid TaoPage page, NoticeSearch noticeSearch) {
        return Result.data(NoticeWrapper.build().pageWrapper(this.noticeService.page(new Page<>(page.getCurrent(), page.getSize()),
                new QueryWrapper<Notice>(noticeSearch).lambda().orderByAsc(Notice::getHaveRead))));
    }

    /**
     * 本人未读消息数量
     *
     * @return 未读数量
     */
    @GetMapping("unread")
    @ApiOperation(value = "本人未读消息数量")
    public Result<Integer> selectAll() {
        return Result.data(this.noticeService.
                count(Wrappers.<Notice>lambdaQuery()
                        .eq(Notice::getHaveRead,false)
                        .eq(Notice::getReceiveUser, CommonUtils.getLoginUser().getId())));
    }

    /**
     * 查看消息详情，消息已读
     *
     * @param id 消息id
     * @return 未读数量
     */
    @PutMapping("haveRead")
    @ApiOperation(value = "消息已读")
    public Result<NoticeVo> haveRead(Long id) {
        //更新
        this.noticeService.update(Wrappers.<Notice>lambdaUpdate().eq(Notice::getId,id).set(Notice::getHaveRead,true));
        //返回数据
        return Result.data(NoticeWrapper.build().wrapper(this.noticeService.getById(id)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("one")
    @ApiOperation(value = "主键查询")
    public Result<Notice> selectOne(Serializable id) {
        return Result.data(this.noticeService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param notice 实体对象
     * @return 新增结果
     */
    @PostMapping
    @ApiOperation(value = "新增")
    public Result<Boolean> insert(@RequestBody Notice notice) {
        return Result.status(this.noticeService.save(notice));
    }

    /**
     * 修改数据
     *
     * @param notice 实体对象
     * @return 修改结果
     */
    @PutMapping("update")
    @ApiOperation(value = "修改")
    public Result<Boolean> update(@RequestBody Notice notice) {
        return Result.status(this.noticeService.updateById(notice));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping("delete")
    @ApiOperation(value = "删除")
    public Result<Boolean> delete(@RequestParam("idList") List<Long> idList) {
        return Result.status(this.noticeService.removeByIds(idList));
    }




}
