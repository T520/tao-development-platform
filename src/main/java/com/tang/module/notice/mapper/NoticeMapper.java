package com.tang.module.notice.mapper;

import org.springframework.stereotype.Repository;
import com.tang.module.notice.vo.NoticeVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tang.module.notice.entity.Notice;
import com.tang.module.notice.search.NoticeSearch;

/**
 * 系统消息通知(Notice)数据库访问层
 *
 * @author 月蚀
 * @since 2022-06-16 17:12:32
 */
@Repository
public interface NoticeMapper extends BaseMapper<Notice> {
    
    /**
     * 自定义分页
     *
     * @param page 自定义分页对象
     * @param noticeSearch 搜索实体
     * @return 分页结果数据            
     */
    IPage<NoticeVo> list(IPage<NoticeVo> page, NoticeSearch noticeSearch);
}
