package com.tang.module.notice.mapper;

import org.springframework.stereotype.Repository;
import com.tang.module.notice.vo.NoticeLogVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tang.module.notice.entity.NoticeLog;
import com.tang.module.notice.search.NoticeLogSearch;

/**
 * 系统消息通知日志(NoticeLog)数据库访问层
 *
 * @author 月蚀
 * @since 2022-06-16 17:12:32
 */
@Repository
public interface NoticeLogMapper extends BaseMapper<NoticeLog> {
    
    /**
     * 自定义分页
     *
     * @param page 自定义分页对象
     * @param noticeLogSearch 搜索实体
     * @return 分页结果数据            
     */
    IPage<NoticeLogVo> list(IPage<NoticeLogVo> page, NoticeLogSearch noticeLogSearch);
}
