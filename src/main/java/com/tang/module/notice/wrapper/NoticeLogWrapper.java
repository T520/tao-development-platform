package com.tang.module.notice.wrapper;



import com.tang.common.utils.cache.DictCache;
import com.tang.common.utils.cache.UserCache;
import com.tang.module.notice.entity.NoticeLog;
import com.tang.common.resources.BaseEntityWrapper;
import com.tang.module.notice.vo.NoticeLogVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.tang.common.constant.ParamName.NOTICE_TYPE;

/**
 * 系统消息通知日志(NoticeLog)数据库转换层
 *
 * @author 月蚀
 * @since 2022-06-16 17:12:32
 */
public class NoticeLogWrapper extends BaseEntityWrapper<NoticeLog,NoticeLogVo> {
    
    public static  NoticeLogWrapper build(){
        return new NoticeLogWrapper();
    }
    
    @Override
    public  NoticeLogVo wrapper(NoticeLog noticeLog){
      NoticeLogVo noticeLogVo = new NoticeLogVo();
      //属性赋值
      BeanUtils.copyProperties(noticeLog,noticeLogVo);
      noticeLogVo.setCreateUserName(UserCache.getUserName(noticeLog.getCreateUser()));
      noticeLogVo.setTypeName(DictCache.getValue(NOTICE_TYPE,noticeLog.getType()));
      //设置接收用户名称，适用消息单发
      if (noticeLog.getReceiveUser() != null){
          noticeLogVo.setReceiveUserName(UserCache.getUserName(noticeLog.getReceiveUser()));
      }
      //是否有群发或者多id发送
      if (StringUtils.isNotBlank(noticeLog.getReceiveGroup())){
          List<String> list = Arrays.stream(noticeLog.getReceiveGroup().split(",")).collect(Collectors.toList());
          StringBuilder deptName = new StringBuilder();
          //循环获取部门名称
          for (String deptId : list) {
              deptName.append(",").append(UserCache.getDeptName(Long.valueOf(deptId)));
          }
          noticeLogVo.setReceiveUserName(deptName.toString());
      }
      return noticeLogVo;
    }
}
