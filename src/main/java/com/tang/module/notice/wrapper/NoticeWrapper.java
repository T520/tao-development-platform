package com.tang.module.notice.wrapper;



import com.tang.common.utils.cache.DictCache;
import com.tang.common.utils.cache.UserCache;
import com.tang.module.notice.entity.Notice;
import com.tang.common.resources.BaseEntityWrapper;
import com.tang.module.notice.vo.NoticeVo;
import org.springframework.beans.BeanUtils;

import static com.tang.common.constant.ParamName.NOTICE_TYPE;

/**
 * 系统消息通知(Notice)数据库转换层
 *
 * @author 月蚀
 * @since 2022-06-16 17:12:32
 */
public class NoticeWrapper extends BaseEntityWrapper<Notice,NoticeVo> {
    
    public static  NoticeWrapper build(){
        return new NoticeWrapper();
    }
    
    @Override
    public  NoticeVo wrapper(Notice notice){
      NoticeVo noticeVo = new NoticeVo();
      //属性赋值
      BeanUtils.copyProperties(notice,noticeVo);
      noticeVo.setCreateUserName(UserCache.getUserName(notice.getCreateUser()));
      noticeVo.setTypeName(DictCache.getValue(NOTICE_TYPE,notice.getType()));
      return noticeVo;
    }
}
