package com.tang.module.notice.entity;

/**
 * 系统消息通知类型枚举
 * @author 月蚀
 * @since 2022/6/16 18:17
 */
public enum NoticeEnum {

    /**
     * 单个用户
     */
    ONE(1),

    /**
     * 多用户
     */
    USERS(2),

    /**
     * 多部门
     */
    DEPT(3);

    final int code;


    NoticeEnum(int i) {
        code = i;
    }

    public int getCode() {
        return code;
    }
}
