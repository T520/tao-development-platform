package com.tang.module.notice.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.*;
import com.tang.common.resources.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 系统消息通知(Notice)实体类
 *
 * @author 月蚀
 * @since 2022-06-16 17:12:32
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "tao_notice 实体类",description = "tao_notice 实体类")
@TableName("tao_notice")
@EqualsAndHashCode(callSuper  = true)
public class Notice extends BaseEntity {

    
    /**
    * 系统消息通知日志id
    */    
    @TableField("log_id")
    @ApiModelProperty(value = "系统消息通知日志id")
    private Long logId;
    
    /**
    * 消息标题
    */    
    @TableField(value = "title",condition = SqlCondition.LIKE)
    @ApiModelProperty(value = "消息标题")
    private String title;
    
    /**
    * 消息
    */    
    @TableField("message")
    @ApiModelProperty(value = "消息")
    private String message;
    
    /**
    * 接收用户id
    */    
    @TableField("receive_user")
    @ApiModelProperty(value = "接收用户id")
    private Long receiveUser;
    
    /**
    * 类型 1单发 2多id发送 3群发
    */    
    @TableField("type")
    @ApiModelProperty(value = "类型 1单发 2多id发送 3群发")
    private Integer type;
    
    /**
    * 是否已读
    */    
    @TableField("have_read")
    @ApiModelProperty(value = "是否已读")
    private Boolean haveRead;

}
