package com.tang.module.notice.entity;


import com.baomidou.mybatisplus.annotation.SqlCondition;
import com.tang.common.resources.BaseEntity;
import com.tang.common.utils.CommonUtils;
import com.tang.module.system.entity.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * 系统消息通知日志(NoticeLog)实体类
 *
 * @author 月蚀
 * @since 2022-06-16 17:12:32
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "tao_notice_log 实体类",description = "tao_notice_log 实体类")
@TableName("tao_notice_log")
@EqualsAndHashCode(callSuper  = true)
public class NoticeLog extends BaseEntity {

    
    /**
    * 消息标题
    */    
    @TableField(value = "title",condition = SqlCondition.LIKE)
    @ApiModelProperty(value = "消息标题")
    private String title;
    
    /**
    * 消息
    */    
    @TableField("message")
    @ApiModelProperty(value = "消息")
    private String message;
    
    /**
    * 接收用户id
    */    
    @TableField("receive_user")
    @ApiModelProperty(value = "接收用户id")
    private Long receiveUser;
    
    /**
    * 接收部门id集合
    */    
    @TableField("receive_group")
    @ApiModelProperty(value = "接收部门id集合")
    private String receiveGroup;
    
    /**
    * 类型 1单发 2多id发送 3群发
    */    
    @TableField("type")
    @ApiModelProperty(value = "1单发 2多id发送 3群发")
    private Integer type;

}
