package com.tang.module.notice.search;

import com.tang.module.notice.entity.Notice;
import io.swagger.annotations.ApiModel;
import lombok.EqualsAndHashCode;
import lombok.Data;

/**
 * 系统消息通知(Notice)实体搜索对象
 *
 * @author 月蚀
 * @since 2022-06-16 17:12:32
 */
@ApiModel(value = "NoticeSearch搜索对象")
@EqualsAndHashCode(callSuper = true)
@Data
public class NoticeSearch extends Notice {
    
}
