package com.tang.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author tang
 * @date 2021/11/10 14:46
 */
@Data
@ConfigurationProperties(prefix = "canal")
public class CanalProperties {

    /**
     * 是否启用
     */
    private String enable = "false";

    /**
     * canal的ip地址
     */
    private String ip = "127.0.0.1";

    /**
     * canal的端口号
     */
    private Integer port = 111111;

    /**
     * canal订阅表的过滤规则
     */
    private String filter = ".*\\..*";

    /**
     * 实例名称
     */
    private String example = "example";
}
