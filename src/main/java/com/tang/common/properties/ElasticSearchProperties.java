package com.tang.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * ES连接配置类
 * @author tang
 * @date 2021/11/02 13:30
 */
@Data
@ConfigurationProperties(prefix = "elasticsearch")
public class ElasticSearchProperties {

    /**
     * 是否启用ES
     */
    private String enable;

    /** 协议 */
    private String schema;

    /** 集群地址，如果有多个用“,”隔开 */
    private String address;

    /** 连接超时时间 */
    private int connectTimeout;

    /** Socket 连接超时时间 */
    private int socketTimeout;

    /** 获取连接的超时时间 */
    private int connectionRequestTimeout;

    /** 最大连接数 */
    private int maxConnectNum;

    /** 最大路由连接数 */
    private int maxConnectPerRoute;

    /**
     * 账号
     */
    private String username;

    /**
     * 密码
     */
    private String password;

}
