package com.tang.common.properties;

import lombok.Data;
import org.hibernate.validator.constraints.URL;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.validation.constraints.NotEmpty;

/**
 * @author tang
 * @date 2021/11/1 16:29
 */
@Data
@ConfigurationProperties(prefix = "minio")
public class MinioProperties {

    /**
     * 是否启用minio
     */
    private String enable;

    /**
     * minio的地址
     */
    private String url;

    /**
     * minio的账户
     */
    private String accessKey;

    /**
     * minio的密码
     */
    private String secretKey;

    /**
     * 默认存储桶名称
     */
    private String name;

}
