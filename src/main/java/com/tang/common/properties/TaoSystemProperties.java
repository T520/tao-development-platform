package com.tang.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * 系统功能设置
 * @author tang
 * @date 2022/1/24 14:14
 */
@Data
@ConfigurationProperties("system")
public class TaoSystemProperties {

    /**
     * 将令牌存储在redis中,启用将使得一个账号只能在一台设备上登录
     */
    private boolean  redisToken = false;

    /**
     * 是否将业务日志存储到elasticsearch中，默认不存储
     * 业务日志 : aop收集的日志、异常处理类中收集的异常日志、系统拦截器链路中的日志
     */
    private boolean logElasticsearch = false;


    /**
     * 开启sql日志
     */
    private Boolean sqlLog = true;

    /**
     * sql日志忽略打印关键字
     */
    private List<String> sqlLogExclude = new ArrayList<>();



}
