package com.tang.common.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 通过过滤器处理异常
 * @author tang
 * @date 2022/1/16 13:26
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GeneralFilterHandlingException extends RuntimeException{

    private String message;

    public GeneralFilterHandlingException(String message) {
        super(message);
        this.message = message;
    }
}
