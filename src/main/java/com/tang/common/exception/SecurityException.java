package com.tang.common.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 安全验证异常
 * @author tang
 * @date 2022/1/14 13:07
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SecurityException extends RuntimeException{

    private String message;


    public SecurityException(String message) {
        super(message);
        this.message = message;
    }
}
