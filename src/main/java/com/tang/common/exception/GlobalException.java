package com.tang.common.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 全局异常
 * @author tang
 * @date 2021/10/23 14:00
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class GlobalException extends RuntimeException{

    private String message;


    public GlobalException(String message) {
        super(message);
        this.message = message;
    }

}
