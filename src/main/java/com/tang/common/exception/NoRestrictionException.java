package com.tang.common.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 接口不受限异常，表示改接口不受访问控制限制
 * @author tang
 * @date 2022/1/21 13:22
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class NoRestrictionException extends RuntimeException {

    private String message;

    public NoRestrictionException(String message) {
        super(message);
        this.message = message;
    }

}
