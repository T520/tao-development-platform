package com.tang.common.constant;

/**
 * 系统安全常量
 * @author tang
 * @date 2021/10/24 13:25
 */
public interface SafetyConstant {

    /**
     * 超级管理员
     */
    String SUPER_ADMIN = "ROLE_superAdmin";

    /**
     * 管理员
     */
    String ADMIN = "ROLE_admin";

    /**
     * 令牌在请求头中的开头格式
     */
    String TOKEN_HEAD = "Bearer";

    /**
     * 盐值
     */
    String SALT = "taotao";

    /**
     * swagger接口文档访问地址
     */
    String SWAGGER_URL = "/doc.html";

    /**
     * 所有
     */
    String ALL = "all";

    /**
     * canal使用的交换机
     */
    String CANAL_EXCHANGE  = "canalDirectExchange";

    /**
     * canal的使用的交换机的路由key
     */
    String CANAL_ROUTER_KEY  = "canal";

    /**
     * canal的死信队列最大重试次数
     */
    String CANAL_MAX_RETRY = "3";


    /**
     * 令牌在请求头中的属性名
     */
    String TOKEN_NAME = "Authorization";
}
