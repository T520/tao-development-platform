package com.tang.common.constant.cahce;

/**
 * 业务缓存常量
 * @author tang
 * @date 2022/3/1 12:09
 */
public interface BusinessCacheConstant {

    /**
     * token存储前缀名称
     */
    String TOKEN_PREFIX = "tao:system:token:";
    /**
     *  用户名称缓存前缀
     */
    String USER_PREFIX = "tao:system:user";
    /**
     * 角色名称缓存
     */
    String ROLE_PREFIX = "tao:system:role";
    /**
     * 部门名称缓存
     */
    String DEPT_PREFIX = "tao:system:dept";
    /**
     * 岗位名称缓存
     */
    String POST_PREFIX = "tao:system:post";
    /**
     * 验证码
     */
    String CAPTCHA_HEADER_KEY = "tao:system:captcha-key:";
    /**
     * 字典
     */
    String DICT = "tao:system:dict:";
    /**
     * 接口权限对应缓存
     */
    String API_ROLE = "tao:system:api-role";
    /**
     * 系统参数缓存前缀明
     */
    String PARAM_PREFIX_PARAM = "tao:system:param:";
    /**
     * 系统字典前缀
     */
    String SYSTEM_DICTIONARY_PREFIX = "tao:system:dist:system:";
    /**
     * 业务字典前缀
     */
    String BIZ_DICTIONARY_PREFIX = "tao:system:dist:biz:";
    /**
     * 访问总数
     */
    String LOGIN_TOTAL = "tao:system:total:login";
    /**
     *  API调用总数
     */
    String API_TOTAL = "tao:system:total:api";
    /**
     * 本周新增访问
     */
    String LOGIN_WEEK_TOTAL = "tao:system:total:weekLogin";
    /**
     * 本周信息API调用
     */
    String API_WEEK_TOTAL = "tao:system:total:weekApi";



}
