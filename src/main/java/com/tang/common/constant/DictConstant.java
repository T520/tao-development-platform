package com.tang.common.constant;

/**
 * 字典常量,记录字典key
 * @author tang
 * @date 2022/3/1 12:04
 */
public interface DictConstant {

    /**
     * 部门类型
     */
    String DEPT_TYPE = "dept_type";

    /**
     * 日志类型
     */
    String LOG_TYPE = "log_type";

    /**
     * 接口类型
     */
    String INTERFACE_TYPE = "interface_type";

    /**
     * 请求方式
     */
    String HTTP_TYPE = "http_type";

    /**
     * 按钮类型
     */
    String BUTTON_TYPE = "button_type";

    /**
     *  菜单类型
     */
    String MENU_TYPE = "menu_type";

}
