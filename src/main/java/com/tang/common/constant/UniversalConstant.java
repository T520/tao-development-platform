package com.tang.common.constant;

/**
 * 系统通用常量
 * @author tang
 * @date 2022/4/19 11:02
 */
public interface UniversalConstant {


    /**
     * 0
     */
    String ZERO = "0";

    /**
     * 1
     */
    String ONE = "1";

    /**
     * 2
     */
    String TWO = "2";

    /**
     * 3
     */
    String THREE = "3";

    /**
     * 4
     */
    String FOUR = "4";

    /**
     * 5
     */
    String FIVE = "5";

    /**
     * 6
     */
    String SIX = "6";

    /**
     * 7
     */
    String SEVEN = "7";

    /**
     * 8
     */
    String EIGHT = "8";

    /**
     * 9
     */
    String NINE = "9";

    /**
     * status
     */
    String STATUS = "status";


    /**
     * 本地ip
     */
    String LOCALHOST_IP = "127.0.0.1";
}
