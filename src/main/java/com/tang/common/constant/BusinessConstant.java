package com.tang.common.constant;


/**
 * 业务常量
 * @author tang
 * @date 2021/11/4 11:30
 */
public interface BusinessConstant {

    /**
     * 降序
     */
    String ORDER_DESCENDING = "descending";

    /**
     * 升序
     */
    String ORDER_ASCENDING = "ascending";

    /**
     * 接口限制名单
     */
    String INTERFACE_RESTRICTION_HASH = "tao:system:interface:urlHash";
    /**
     * 接口权限名单
     */
    String INTERFACE_AUTH_SET = "tao:system:interface:auth";
    /**
     * 接口剩余次数名单
     */
    String INTERFACE_RESTRICTION_FREQUENCY =  "tao:system:interface:frequency";
    /**
     * 在线人数列表
     */
    String LIST_OF_ONLINE_PEOPLE =  "tao:system:on-line:";
}
