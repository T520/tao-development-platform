package com.tang.common.constant;

/**
 * 使用的接口方法名称
 * @author tang
 * @date 2022/4/8 13:14
 */
public interface UseApiConstant {

    /**
     * IP定位接口类别名称
     */
    String IP = "IP定位";
}
