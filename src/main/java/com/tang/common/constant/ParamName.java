package com.tang.common.constant;

/**
 * 系统参数常量
 * @author tang
 * @date 2021/10/26 16:05
 */
public interface ParamName {

    /**
     * 令牌过期时间参数key
     */
    String TOKEN_TIME = "token_time";

    /**
     * 令牌刷新时间
     */
    String TOKEN_REFRESH_TIME = "token_refresh_time";

    /**
     * 默认登录角色
     */
    String DEFAULT_ROLE = "default_role_user";

    /**
     * minio默认存储桶
     */
    String DEFAULT_BUCKET = "default_bucket";

    /**
     * es默认分片数量
     */
    String DEFAULT_SHARDS_NUMBER = "default_shards";

    /**
     * es默认分片数量
     */
    String DEFAULT_REPLICAS_NUMBER = "default_replicas";

    /**
     * 全局IP访问速率
     */
    String GLOBAL_IP_SPEED = "global_ip_speed";

    /**
     * 系统消息通知类型
     */
    String  NOTICE_TYPE = "notice_type";

}
