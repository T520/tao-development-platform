package com.tang.common.constant;


import org.apache.poi.ss.formula.functions.T;

/**
 * 使用的接口参数常量
 * @author tang
 * @date 2021/11/4 11:30
 */
public interface UseParamConstant {


    /**
     * 高德 统一社会信用代码
     */
    String GAO_DE_CREDIT_CODE = "911101147263767522";

    /**
     *  百度 统一社会信用代码
     */
    String BAI_DU_CREDIT_CODE = "91110000802100433B";

    /**
     * 腾讯 统一社会信用代码
     */
    String TEN_XUN_CREDIT_CODE= "91440300708461136T";

    /**
     *  web请求密钥
     */
    String APP_KEY_WEB = "app_key_web";
    /**
     * 腾讯web服务签名密钥
     */
    String T_WEB_AUTOGRAPH = "t_web_autograph";

    /**
     * 高德软件有限公司-ip定位请求url
     */
    String IP_ADDRESS_URL = "ip_address_url";

    /**
     * 腾讯-ip定位请求url
     */
    String T_IP_ADDRESS_URL = "t_ip_addr_url";

}
