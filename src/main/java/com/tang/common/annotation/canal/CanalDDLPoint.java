package com.tang.common.annotation.canal;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 监控数据库DDL语言
 * DDL : 数据定义语言 create、alter、 drop
 * @author tang
 * @date 2021/11/11 9:59
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CanalDDLPoint {

}
