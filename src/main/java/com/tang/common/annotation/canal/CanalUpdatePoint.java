package com.tang.common.annotation.canal;


import com.tang.common.constant.SafetyConstant;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * canal更新事件监听器
 * @author tang
 * @date 2021/11/10 14:38
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CanalUpdatePoint {

    /**
     * 指定监听的表
     */
    String tableName() default SafetyConstant.ALL;

    /**
     * 指定监听的数据库
     */
    String database() default SafetyConstant.ALL;



}
