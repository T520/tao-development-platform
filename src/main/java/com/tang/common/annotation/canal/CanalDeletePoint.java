package com.tang.common.annotation.canal;

import com.tang.common.constant.SafetyConstant;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * canal删除事件监听器
 * @author tang
 * @date 2021/11/10 14:37
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CanalDeletePoint {

    /**
     * 指定监听的表
     */
    String tableName() default SafetyConstant.ALL;

    /**
     * 指定监听的数据库
     */
    String database() default SafetyConstant.ALL;
}
