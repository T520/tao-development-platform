package com.tang.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 系统日志注解
 * @author tang
 * @date 2022/1/18 17:58
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ApiLog {

    /**
     * 接口标题
     */
    String title() default "业务接口";

    /**
     * 日志类型
     */
    int type() default 0;

    /**
     * 接口日志描述,支持SpEL表达式
     */
    String desc() default "";
}
