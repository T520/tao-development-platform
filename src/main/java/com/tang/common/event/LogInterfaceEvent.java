package com.tang.common.event;

import com.tang.module.system.entity.LogInterface;
import org.springframework.context.ApplicationEvent;

/**
 * 接口日志事件
 * @author tang
 * @date 2022/1/19 10:53
 */
public class LogInterfaceEvent extends ApplicationEvent {

    private final LogInterface logInterface;

    public LogInterfaceEvent(LogInterface logInterface) {
        super(logInterface);
        this.logInterface = logInterface;
    }

    public LogInterface getLogInterface() {
        return logInterface;
    }
}
