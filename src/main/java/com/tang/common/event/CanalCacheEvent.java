package com.tang.common.event;

import com.alibaba.otter.canal.protocol.CanalEntry;
import org.springframework.context.ApplicationEvent;

import java.util.List;

/**
 * canal缓存更新事件
 * @author tang
 * @date 2022/4/12 18:07
 */
public class CanalCacheEvent extends ApplicationEvent {

    /**
     * canal消息实体集合
     */
    private final List<CanalEntry.Entry> entries;

    public CanalCacheEvent(List<CanalEntry.Entry> entries) {
        super(entries);
        this.entries = entries;
    }

    public List<CanalEntry.Entry> getEntries() {
        return entries;
    }
}
