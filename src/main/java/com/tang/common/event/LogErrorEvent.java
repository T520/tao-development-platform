package com.tang.common.event;

import com.tang.module.system.entity.LogError;
import org.springframework.context.ApplicationEvent;

/**
 * 错误日志事件
 * @author tang
 * @date 2022/1/20 10:45
 */
public class LogErrorEvent extends ApplicationEvent {

    private final LogError logError;


    public LogErrorEvent(LogError logError) {
        super(logError);
        this.logError = logError;
    }

    public LogError getLogError() {
        return logError;
    }
}
