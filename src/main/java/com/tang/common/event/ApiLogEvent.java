package com.tang.common.event;

import com.tang.module.system.entity.LogApi;
import org.springframework.context.ApplicationEvent;


/**
 * 通用日志事件
 * @author tang
 * @date 2022/1/19 10:53
 */
public class ApiLogEvent extends ApplicationEvent {

    private final LogApi logApi;

    public ApiLogEvent(LogApi logApi) {
        super(logApi);
        this.logApi = logApi;
    }

    public LogApi getLogApi() {
        return logApi;
    }
}
