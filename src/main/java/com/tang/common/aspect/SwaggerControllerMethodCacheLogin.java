package com.tang.common.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 解决日志频繁打印：Generating unique operation named: updateUsingPOST_4
 * 这是因为使用了Swagger后 在加载controller 的时候，发现这些类里面的方法名称有重复的
 * 所以给你自动生成了一些名称，用以区分这些方法
 * @author tang
 * @date 2022/1/26 10:11
 */
@Slf4j
@Aspect
@Component
public class SwaggerControllerMethodCacheLogin {


    private Map<String, Integer> generated = new HashMap();


    @Pointcut("execution(* springfox.documentation.spring.web.readers.operation.CachingOperationNameGenerator.startingWith(String))")
    public void c() {
    }


    @Around("c()")
    public Object a(ProceedingJoinPoint point) {
        //获取方法参数，这个方法只有一个参数,参数名称为控制层方法名称加上Using和方法请求方式，比如：userDetailsUsingGET
        Object[] args = point.getArgs();
        //这不放行该方法，执行执行我们写好的方法就可以了，就不会打印日志
        return startingWith(String.valueOf(args[0]));
    }

    private String startingWith(String prefix) {
        //判断方法名称是否重复
        if (generated.containsKey(prefix)) {
            generated.put(prefix, generated.get(prefix) + 1);
            String nextUniqueOperationName = String.format("%s_%s", prefix, generated.get(prefix));
           // log.debug("组件中存在相同的方法名称，自动生成组件方法唯一名称进行替换: {}", nextUniqueOperationName);
            return nextUniqueOperationName;
        } else {
            generated.put(prefix, 0);
            return prefix;
        }
    }
}
