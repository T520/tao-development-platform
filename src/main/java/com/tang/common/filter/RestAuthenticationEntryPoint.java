package com.tang.common.filter;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.tang.common.resources.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 未登录或者token过期处理程序
 * @author tang
 * @date 2021/4/6 13:00
 */
@Slf4j
@Component
public  class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {


    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        //内容格式为json
        log.error(authException.getMessage());
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        response.setStatus(HttpStatus.FORBIDDEN.value());
        //获得输出流
        PrintWriter writer = response.getWriter();
        writer.write(new ObjectMapper().writeValueAsString(Result.fail(HttpStatus.FORBIDDEN.value(),"未登录或登录失效,请重新登录")));
        writer.flush();
        writer.close();
    }

}
