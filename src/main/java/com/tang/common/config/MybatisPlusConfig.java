package com.tang.common.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.tang.common.interceptor.SqlLogInterceptor;
import com.tang.common.properties.TaoSystemProperties;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * mybatis-plus配置
 * @author tang
 * @date 2021/10/25 11:10
 */
@Configuration
@MapperScan("com.tang.module.**.mapper")
public class MybatisPlusConfig {


    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }

    /**
     * sql 日志
     */
    @Bean
    public SqlLogInterceptor sqlLogInterceptor(TaoSystemProperties taoSystemProperties) {
        return new SqlLogInterceptor(taoSystemProperties);
    }



}
