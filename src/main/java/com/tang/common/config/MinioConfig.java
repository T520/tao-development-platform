package com.tang.common.config;

import com.tang.common.properties.MinioProperties;
import io.minio.MinioClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * minio自动配置类
 * @author tang
 * @date 2021/11/1 16:32
 */
@Slf4j
@Configuration
@EnableConfigurationProperties(MinioProperties.class)
@ConditionalOnProperty(prefix = "minio",name = "enable",havingValue = "true")
public class MinioConfig {

    @Resource
    private MinioProperties minioProperties;


    @Bean
    public MinioClient getMinioClient() {
        log.info("minio的连接地址为 : {}",minioProperties.getUrl());
        return MinioClient.builder()
                .endpoint(minioProperties.getUrl())
                .credentials(minioProperties.getAccessKey(), minioProperties.getSecretKey()).build();
    }

}
