package com.tang.common.config;

import com.github.xiaoymin.knife4j.spring.extension.OpenApiExtensionResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author tang
 * @date 2021/10/24 13:44
 */
@Configuration
@EnableSwagger2WebMvc
public class SwaggerConfig {

    @Lazy
    @Autowired
    private  OpenApiExtensionResolver openApiExtensionResolver;

    @Bean
    public Docket loginApiConfig(){
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("基础模块")
                .apiInfo(webApiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.tang.module.system"))
                .build()
                .extensions(openApiExtensionResolver.buildExtensions("基础模块"))
                .securitySchemes(securitySchemes())
                .securityContexts(securityContexts());
    }


    @Bean
    public Docket rabbitmqApiConfig(){
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("消息队列模块")
                .apiInfo(webApiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.tang.module.rabbitmq"))
                .build()
                .extensions(openApiExtensionResolver.buildExtensions("消息队列模块"))
                .securitySchemes(securitySchemes())
                .securityContexts(securityContexts());
    }

    private ApiInfo webApiInfo(){
        return new ApiInfoBuilder()
                .title("TAO快速开发框架-API文档")
                .description("本文档描述了TAO快速开发框架服务接口定义")
                .version("1.0")
                .contact(new Contact("tao", "my.vom", "3179927046@qq.com"))
                .build();
    }

    private List<ApiKey> securitySchemes() {
        return new ArrayList<ApiKey>(Collections.singleton(new ApiKey("Authorization", "Authorization", "header")));
    }

    private List<SecurityContext> securityContexts() {
        return new ArrayList<SecurityContext>(
                Collections.singleton(SecurityContext.builder()
                        .securityReferences(defaultAuth())
                        .forPaths(PathSelectors.regex("^(?!auth).*$"))
                        .build())
        );
    }
    List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return new ArrayList<SecurityReference>(
                Collections.singleton(new SecurityReference("Authorization", authorizationScopes)));
    }


}
