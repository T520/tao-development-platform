package com.tang.common.resources;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


/**
 * 分页对象
 * @author tang
 * @date 2021/10/26 16:59
 */
@Data
@ApiModel(value = "分页对象")
public class TaoPage {

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页",required = true)
    @NotNull(message = "当前页面数不能为空")
    @Min(value = 0,message = "当前页必须为正数")
    private Integer current;

    /**
     * 每页数量
     */
    @ApiModelProperty(value = "每页数量",required = true)
    @NotNull(message = "当前页面数不能为空")
    @Min(value = 0,message = "每页数量必须为正数")
    private Integer size;

    /**
     * 排序字段
     */
    @ApiModelProperty(value = "排序字段")
    private String sortField;

    /**
     * 排序方式
     */
    @ApiModelProperty(value = "排序方式")
    private String order;


}
