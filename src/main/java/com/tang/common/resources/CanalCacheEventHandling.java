package com.tang.common.resources;

import com.alibaba.fastjson.JSON;
import com.alibaba.otter.canal.protocol.CanalEntry;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * canal缓存更新事件处理
 * @author tang
 * @date 2021/11/10 14:42
 */
public interface CanalCacheEventHandling {


    /**
     * canal新增事件处理入口
     * @param entry 事件
     */
    void insertDealWith(CanalEntry.Entry entry);

    /**
     * canal更新事件处理入口
     * @param entry 事件
     */
    void updateDealWith(CanalEntry.Entry entry);

    /**
     * canal删除事件处理入口
     * @param entry 事件
     */
    void deleteDealWith(CanalEntry.Entry entry);

    /**
     * 将canal的行数据转换成对应的实体类
     * @param columnList 行数据，里面每个元素都是一个实体类的一个属性
     * @param tClass 实体类class
     * @param <T> class
     * @return 实体类对象
     */
    default <T> T parameterInverseSequence(List<CanalEntry.Column> columnList, Class<T> tClass){
        //将行数据转成map
        Map<String, String> map = columnList.parallelStream().collect(Collectors.toMap(CanalEntry.Column::getName, CanalEntry.Column::getValue));
        return JSON.parseObject(JSON.toJSONString(map), tClass);
    }
}
