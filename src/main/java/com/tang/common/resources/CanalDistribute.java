package com.tang.common.resources;

import com.alibaba.otter.canal.protocol.CanalEntry;
import com.google.protobuf.InvalidProtocolBufferException;

import java.util.List;

/**
 * canal分发器
 * 将对应的canal实际分发到实际的bean
 * 被类只定义接口
 * @author tang
 * @date 2022/1/12 9:52
 */
public interface CanalDistribute {


	/**
	 * 分发，将消息实体集合分发到对应的处理类
	 * @param entryList 具体的消息实体集合
	 * @throws InvalidProtocolBufferException 当解析行数据错误时
	 */
	void distributionProcessing(List<CanalEntry.Entry> entryList) throws InvalidProtocolBufferException;

	/**
	 * 筛选符合要求的库和表
	 * 如过滤掉日志表和其他不需要的数据库
	 * @param entry canal实体
	 * @return 是否需要
	 */
	Boolean screen(CanalEntry.Entry entry);


	/**
	 * canal更新处理
	 * @param entry 具体的消息实体
	 */
	void updateDistribution(CanalEntry.Entry entry);

	/**
	 * canal插入处理
	 * @param entry 具体的消息实体
	 */
	void insertDistribution(CanalEntry.Entry entry);

	/**
	 * canal删除处理
	 * @param entry 具体的消息实体
	 */
	void deleteDistribution(CanalEntry.Entry entry);

	/**
	 * canal的ddl处理
	 * @param entry 具体的消息实体
	 */
	void otherDistribution(CanalEntry.Entry entry);

	/**
	 * 数据验证
	 * 验证注解是否符合要求，如数据库是否匹配，表是否匹配
	 * @param entry 消息实体
	 * @param tableName 表名称
	 * @param databaseName 数据库名称
	 * @return 是否匹配
	 */
	Boolean matchChain(CanalEntry.Entry entry, String tableName,String databaseName);





}
