package com.tang.common.resources;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 统一响应封装
 * @author tang
 * @date 2021/10/24 14:59
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "公共返回实体",description = "公共返回实体")
public class Result<T> implements Serializable {
    private static final long serialVersionUID = -7180318500751666084L;

    @NotNull
    @ApiModelProperty(value = "状态码")
    private int code;

    @NotNull
    @ApiModelProperty(value = "是否成功")
    private boolean success;

    @ApiModelProperty(value = "消息")
    private String message;

    @ApiModelProperty("返回数据")
    private T data;

    /**
     * 操作成功，返回数据
     * @param data 数据
     * @param <T> 数据类型
     * @return
     */
    public static <T> Result<T> data(T data){
        return data(data, "操作成功");
    }

    /**
     * 操作成功，返回数据和消息
     * @param data 数据
     * @param message 消息
     * @param <T> 数据类型
     * @return 结果
     */
    public static <T> Result<T> data(T data,String message){
        return new Result<T>(200, true,message,data);
    }

    /**
     * 操作失败，返回提示信息
     * @param message 提示信息
     * @param <T> 数据类型
     * @return 结果
     */
    public static <T> Result<T> fail(String message){
        return new Result<T>(400, false,message, null);
    }

    /**
     * 操作失败，返回提示信息和自定义的状态码
     * @param code 状态码
     * @param message 信息
     * @param <T> 数数据类型
     * @return 结果
     */
    public static <T> Result<T> fail(Integer code,String message){
        return new Result<T>(code, false,message, null);
    }

    /**
     * 操作成功
     * @param <T> 数据类型
     * @return 结果
     */
    public static <T> Result<T> success(){
        return new Result<T>(200, true,"操作成功", null);
    }

    /**
     * 操作成功，返回自定义消息提示
     * @param message 消息提示
     * @param <T> 数据类型
     * @return 结果
     */
    public static <T> Result<T> success(String message){
        return new Result<T>(200, true,message, null);
    }

    /**
     * 根据返回状态确认是否操作成功
     * @param tag 状态
     * @param <T> 返回数据类型
     * @return 结果
     */
    public static <T> Result<T> status(boolean tag){
        if (tag){
            return success();
        }else {
            return fail("操作失败");
        }
    }

}
