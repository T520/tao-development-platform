package com.tang.common.resources;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 实体类顶级接口
 * @author tang
 * @date 2022/2/21 16:02
 */
@Data
@ApiModel(value = "基础实体")
public class BaseEntity implements Serializable {


    /**
     * 主键
     */
    @TableId(
            value = "id",
            type = IdType.ASSIGN_ID
    )
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "主键")
    public Long id;

    /**
     * 创建人
     */
    @TableField(value = "create_user",fill = FieldFill.INSERT)
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "创建人")
    public Long createUser;

    /**
     * 创建部门
     */
    @TableField(value = "create_dept",fill = FieldFill.INSERT)
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "创建部门")
    public Long createDept;

    /**
     * 创建时间
     */
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    @DateTimeFormat(
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            locale = "zh", timezone="GMT+8"
    )
    @ApiModelProperty(value = "创建时间")
    public Date createTime;

    /**
     * 修改人
     */
    @TableField(value = "update_user",fill = FieldFill.UPDATE)
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "修改人")
    public Long updateUser;

    /**
     * 修改时间
     */
    @TableField(value = "update_time",fill = FieldFill.UPDATE)
    @DateTimeFormat(
            pattern = "yyyy-MM-dd HH:mm:ss"
    )
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            locale = "zh", timezone="GMT+8"
    )
    @ApiModelProperty(value = "修改时间")
    public Date updateTime;

    /**
     * 状态
     */
    @TableField("status")
    @ApiModelProperty(value = "状态")
    public Integer status;

    /**
     * 是否已删除
     */
    @TableField("is_deleted")
    @ApiModelProperty(value = "是否已删除")
    public Integer isDeleted;

}
