package com.tang.common.resources;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 包装类基本接口
 * @author tang
 * @date 2022/2/17 16:17
 */
public  abstract class BaseEntityWrapper<T,R> {


    /**
     * 获取分页vo对象
     * @param page 原生分页对象
     * @return vo分页对象
     */
    public IPage<R> pageWrapper(IPage<T> page){
        IPage<R> pageVo = new Page(page.getCurrent(), page.getSize(), page.getTotal());
        List<R> list = page.getRecords().parallelStream().map(this::wrapper).collect(Collectors.toList());
        pageVo.setRecords(list);
        return pageVo;
    }

    /**
     * 将实体对象转成对应的vo类
     * @param t 实体
     * @return vo对象
     */
    public  abstract R  wrapper(T t);

}
