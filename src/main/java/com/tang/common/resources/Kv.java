package com.tang.common.resources;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 只包含两个属性的实体类
 * @author tang
 * @date 2022/3/14 14:47
 */
@Data
@Accessors(chain = true)
public class Kv<T,R> implements Serializable {

    /**
     * key
     */
    private T key;

    /**
     * value
     */
    private R value;

}
