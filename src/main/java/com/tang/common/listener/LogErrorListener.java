package com.tang.common.listener;

import cn.hutool.core.map.MapUtil;
import com.tang.common.event.LogErrorEvent;
import com.tang.common.utils.CommonUtils;
import com.tang.module.system.entity.LogError;
import com.tang.module.system.mapper.LogErrorMapper;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author tang
 * @date 2022/1/20 10:47
 */
@Component
public class LogErrorListener implements ApplicationListener<LogErrorEvent> {

    @Resource
    private LogErrorMapper logErrorMapper;


    @Async
    @Override
    public void onApplicationEvent(LogErrorEvent event) {
       /* LogError logError = event.getLogError();
        //获取用户所在地址和运营商
        Map<String, String> map = CommonUtils.ipGetAddress(logError.getRemoteIp());
        if (MapUtil.isNotEmpty(map)){
            logError.setAddress(map.get("address"));
            logError.setIsp(map.get("isp"));
        }
        logErrorMapper.insert(logError);*/
    }
}
