package com.tang.common.listener;


import cn.hutool.core.map.MapUtil;
import cn.hutool.extra.template.Template;
import cn.hutool.extra.template.TemplateConfig;
import cn.hutool.extra.template.TemplateEngine;
import cn.hutool.extra.template.TemplateUtil;
import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.tang.common.async.LogInterfaceAsync;
import com.tang.common.event.LogInterfaceEvent;
import com.tang.common.utils.CommonUtils;
import com.tang.module.system.entity.InterfaceConfiguration;
import com.tang.module.system.entity.LogInterface;
import com.tang.module.system.mapper.InterfaceConfigurationMapper;
import com.tang.module.system.mapper.LogInterfaceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 接口日志事件监听
 * @author tang
 * @date 2022/1/19 10:53
 */
@Component
public class LogInterfaceListener implements ApplicationListener<LogInterfaceEvent> {

    @Autowired
    private LogInterfaceMapper logInterfaceMapper;

    @Autowired
    private InterfaceConfigurationMapper interfaceConfigurationMapper;




    @Async
    @Override
    public void onApplicationEvent(LogInterfaceEvent event) {
        LogInterface logInterface = event.getLogInterface();
/*        //获取接口配置
        InterfaceConfiguration configuration = interfaceConfigurationMapper.selectOne(Wrappers.<InterfaceConfiguration>lambdaQuery().
                eq(InterfaceConfiguration::getSign, logInterface.getUrlTag()).
                eq(InterfaceConfiguration::getStatus,1).
                eq(InterfaceConfiguration::getOpenLog,true));
        //如果开启日志记录的接口配置不存在，就直接退出
        if (configuration == null){
            return;
        }
        //如果需要模板解析
        if (configuration.getTemplate().contains("${")){
            //模板引擎，设置模板
            TemplateEngine engine = TemplateUtil.createEngine(new TemplateConfig());
            Template template = engine.getTemplate(configuration.getTemplate());
            //没有参数就直接存储模板
            if (logInterface.getParameter().size() == 0){
                logInterface.setLog(configuration.getTemplate());
            }else {
                Map<String, String> params = new LinkedHashMap<>();
                //设置参数值
                for (String key : logInterface.getParameter().keySet()) {
                    params.put(key,logInterface.getParameter().get(key)[0]);
                }
                //设置日志
                logInterface.setLog(template.render(params));
            }
        }else {
            logInterface.setLog(configuration.getTemplate());
        }
        logInterface.setName(configuration.getInterfaceName());
        //设置请求地址
        Map<String, String> map = CommonUtils.ipGetAddress(logInterface.getRemoteIp());
        if (MapUtil.isNotEmpty(map)){
            logInterface.setAddress(map.get("address"));
            logInterface.setIsp(map.get("isp"));
        }*/
        //获取用户代理
        UserAgent ua = UserAgentUtil.parse(logInterface.getUserAgent());
        logInterface.setBrowser(ua.getBrowser().toString());
        logInterface.setSystem(ua.getPlatform().toString());
        logInterface.setMobileTerminal(ua.isMobile());
        logInterfaceMapper.insert(logInterface);
    }
}
