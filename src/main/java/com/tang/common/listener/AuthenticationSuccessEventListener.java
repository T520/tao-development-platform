package com.tang.common.listener;

import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

/**
 * AuthenticationSuccessEvent事件是Spring Security默认认证通过后发布发事件
 *
 * @author tang
 * @date 2022/1/23 14:26
 */
@Component
public class AuthenticationSuccessEventListener implements ApplicationListener<AuthenticationSuccessEvent> {


    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent event) {
        System.out.println("1122");
        Authentication authentication = event.getAuthentication();
        Object principal = authentication.getPrincipal();
    }
}
