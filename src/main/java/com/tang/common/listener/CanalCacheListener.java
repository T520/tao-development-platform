package com.tang.common.listener;

import com.alibaba.otter.canal.protocol.CanalEntry;
import com.tang.common.event.CanalCacheEvent;
import com.tang.module.canal.handle.CanalCoreDispenser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * canal更新事件监听器
 * @author tang
 * @date 2022/4/12 18:12
 */
@Slf4j
@Component
public class CanalCacheListener implements ApplicationListener<CanalCacheEvent> {

    @Autowired
    private CanalCoreDispenser canalCoreDispenser;


    @Async
    @Override
    public void onApplicationEvent(CanalCacheEvent event) {
        List<CanalEntry.Entry> entries = event.getEntries();
        try {
            canalCoreDispenser.distributionProcessing(entries);
        }catch (Exception e){
            log.error("canal数据处理异常",e);
        }
    }
}
