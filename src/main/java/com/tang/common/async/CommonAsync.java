package com.tang.common.async;

import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import com.tang.common.constant.BusinessConstant;
import com.tang.common.constant.UniversalConstant;
import com.tang.module.system.entity.User;
import com.tang.module.useapi.module.entity.IpLocation;
import com.tang.module.useapi.module.resources.BasicUseApiResources;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * 全局异步方法
 * @author tang
 * @date 2022/1/21 17:47
 */
@Component
public class CommonAsync {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Resource
    private BasicUseApiResources basicUseApiResources;

    /**
     * 在线人数更新
     * @param authentication 认证实体
     * @param userAgent 请求代理
     */
    @Async
    public void  numberOfPeopleOnline(Authentication authentication, String ip, String userAgent){
        //访问不需要登录的接口时，不需要更新在线人数
       if (authentication == null || authentication.getPrincipal() == null) {
            return;
        }
        //获取登录用户
        User user = (User) authentication.getPrincipal();
        HashMap<String, Object> hashMap = new HashMap<>(10);
        hashMap.put("id",user.getId());
        hashMap.put("account",user.getAccount());
        hashMap.put("avatar",user.getAvatar());
        hashMap.put("name",user.getName());
        hashMap.put("phone",user.getPhone());
        hashMap.put("roleList",user.getRoleList());
        //获得IP对应地理位置,本地IP不需要请求，直接设置即可
        if (UniversalConstant.LOCALHOST_IP.equals(ip)){
            hashMap.put("ip",ip);
            hashMap.put("address","本地");
            hashMap.put("isp","本地");
        }else {
            IpLocation ipLocation = basicUseApiResources.byIpGetLocation(ip);
            hashMap.put("ip",ip);
            hashMap.put("address",
                    //国家
                  /*  (ipLocation.getCountry() == null? "" : ipLocation.getCountry()) +*/
                    //省份
                    (ipLocation.getProvince() == null? "" : ipLocation.getProvince()) +
                     //市
                    (ipLocation.getCity() == null? "" : "-" + ipLocation.getCity()) +
                     //区
                    (ipLocation.getDistrict() == null? "" : "-" + ipLocation.getDistrict()) +
                     //区县
                    (ipLocation.getDistrict() == null? "" : "-" + ipLocation.getDistrict()));
            hashMap.put("isp",ipLocation.getIsp());
        }
        UserAgent ua = UserAgentUtil.parse(userAgent);
        hashMap.put("browser",ua.getBrowser().toString());
        hashMap.put("system",ua.getPlatform().toString());
        hashMap.put("mobileTerminal",ua.isMobile());
        //当前时间戳
        long timeMillis = System.currentTimeMillis();
        hashMap.put("timeStamp",timeMillis);
        //新增，更新在线人数,这里使用前缀 ＋ 时间戳作为前缀
        redisTemplate.opsForValue().set(BusinessConstant.LIST_OF_ONLINE_PEOPLE + ip, hashMap,5, TimeUnit.MINUTES);
    }

}
