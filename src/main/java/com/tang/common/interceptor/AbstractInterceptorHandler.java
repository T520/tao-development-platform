package com.tang.common.interceptor;

import lombok.Data;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 接口限额过拦截器抽象处理程序
 * 用于定义预定义非法
 * @author tang
 * @date 2022/1/16 12:41
 */
@Data
public abstract class AbstractInterceptorHandler {

    private AbstractInterceptorHandler nextAbstractInterceptorHandler;


    /**
     * 对拦截器进行处理，执行当前过滤，如果存在下一个拦截器，也紧接着执行
     * @param request 请求
     * @param response 响应
     */
    public void filter(HttpServletRequest request, HttpServletResponse response){
        this.doFilter(request,response);
        //如果下一个处理程序不为null,就可以继续执行下一个过滤处理程序
        if (nextAbstractInterceptorHandler != null){
            nextAbstractInterceptorHandler.filter(request, response);
        }
    }

    /**
     * 拦截处理方法，执行实际的处理程序
     * @param request 请求
     * @param response 响应
     */
    public abstract void doFilter(HttpServletRequest request, HttpServletResponse response);
}
