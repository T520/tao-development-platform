package com.tang.common.interceptor;

import com.alibaba.druid.DbType;
import com.alibaba.druid.filter.FilterChain;
import com.alibaba.druid.filter.FilterEventAdapter;
import com.alibaba.druid.proxy.jdbc.JdbcParameter;
import com.alibaba.druid.proxy.jdbc.ResultSetProxy;
import com.alibaba.druid.proxy.jdbc.StatementProxy;
import com.alibaba.druid.sql.SQLUtils;
import com.tang.common.properties.TaoSystemProperties;
import com.tang.common.utils.CommonUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import oshi.util.StringUtil;

import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Druid日志打印类
 * @author tang
 * @date 2022/5/19 15:37
 */
@Slf4j
public class SqlLogInterceptor extends FilterEventAdapter {

    private static final SQLUtils.FormatOption FORMAT_OPTION = new SQLUtils.FormatOption(false, false);

    private static final List<String> SQL_LOG_EXCLUDE = new ArrayList<>(Arrays.asList("ACT_RU_JOB", "ACT_RU_TIMER_JOB"));

    private final TaoSystemProperties properties;

    public SqlLogInterceptor(TaoSystemProperties properties) {
        this.properties = properties;
        if (properties.getSqlLogExclude().size() > 0) {
            SQL_LOG_EXCLUDE.addAll(properties.getSqlLogExclude());
        }
    }

    @Override
    protected void statementExecuteBefore(StatementProxy statement, String sql) {
        statement.setLastExecuteStartNano();
    }

    @Override
    protected void statementExecuteBatchBefore(StatementProxy statement) {
        statement.setLastExecuteStartNano();
    }

    @Override
    protected void statementExecuteUpdateBefore(StatementProxy statement, String sql) {
        statement.setLastExecuteStartNano();
    }

    @Override
    protected void statementExecuteQueryBefore(StatementProxy statement, String sql) {
        statement.setLastExecuteStartNano();
    }

    @Override
    protected void statementExecuteAfter(StatementProxy statement, String sql, boolean firstResult) {
        statement.setLastExecuteTimeNano();
    }

    @Override
    protected void statementExecuteBatchAfter(StatementProxy statement, int[] result) {
        statement.setLastExecuteTimeNano();
    }

    @Override
    protected void statementExecuteQueryAfter(StatementProxy statement, String sql, ResultSetProxy resultSet) {
        statement.setLastExecuteTimeNano();
    }

    @Override
    protected void statementExecuteUpdateAfter(StatementProxy statement, String sql, int updateCount) {
        statement.setLastExecuteTimeNano();
    }


    @Override
    @SneakyThrows
    @SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
    public void statement_close(FilterChain chain, StatementProxy statement) {
        // 是否开启日志
        if (!properties.getSqlLog()) {
            chain.statement_close(statement);
            return;
        }
        // 是否开启调试
        if (!log.isInfoEnabled()) {
            chain.statement_close(statement);
            return;
        }
        // 打印可执行的 sql,这里的sql是没有设置参数的，以?来代替
        String sql = statement.getBatchSql();
        // sql 为空直接返回
        if (StringUtils.isEmpty(sql)) {
            chain.statement_close(statement);
            return;
        }
        // sql 包含排除的关键字直接返回
        if (excludeSql(sql)) {
            chain.statement_close(statement);
            return;
        }
        int parametersSize = statement.getParametersSize();
        List<Object> parameters = new ArrayList<>(parametersSize);
        for (int i = 0; i < parametersSize; ++i) {
            // 转换参数，处理 java8 时间
            parameters.add(getJdbcParameter(statement.getParameter(i)));
        }
        String dbType = statement.getConnectionProxy().getDirectDataSource().getDbType();
        //填充参数
        String formattedSql = SQLUtils.format(sql, DbType.of(dbType), parameters, FORMAT_OPTION);
        printSql(formattedSql, statement);
        chain.statement_close(statement);
    }

    private static boolean excludeSql(String sql) {
        // 判断关键字
        for (String exclude : SQL_LOG_EXCLUDE) {
            if (sql.contains(exclude)) {
                return true;
            }
        }
        return false;
    }

    private static Object getJdbcParameter(JdbcParameter jdbcParam) {
        if (jdbcParam == null) {
            return null;
        }
        Object value = jdbcParam.getValue();
        // 处理 java8 时间
        if (value instanceof TemporalAccessor) {
            return value.toString();
        }
        return value;
    }

    private static void printSql(String sql, StatementProxy statement) {
        // 打印 sql
        String sqlLogger = "\n\n==============  Sql Start  ==============" +
                "\nExecute SQL : {}" +
                "\nExecute Time: {}" +
                "\n==============  Sql  End   ==============\n";
        log.info(sqlLogger, sql.trim(), CommonUtils.format(statement.getLastExecuteTimeNano()));
    }


}
