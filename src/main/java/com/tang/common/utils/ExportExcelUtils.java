package com.tang.common.utils;

import cn.hutool.core.io.IoUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import cn.hutool.poi.excel.StyleSet;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

/**
 * 封装hutool工具包实现excel快速导出
 * @author tang
 * @date 2021/10/29 14:20
 */
public class ExportExcelUtils {


    /**
     * 通过实体类集合创建excel表格文件导出到输出流
     * @param dataList 数据集合
     * @param fileName 文件名字
     * @param response 输出流
     * @throws IOException
     */
    public static  void  entityExcel(List<? extends Object> dataList,String fileName, HttpServletResponse response) throws IOException {
        ExcelWriter writer = null;
        ServletOutputStream out = null;
        try {
            // 通过工具类创建writer，默认创建xls格式
            writer = ExcelUtil.getWriter(true);
            StyleSet style = writer.getStyleSet();
            CellStyle cellStyle = style.getHeadCellStyle();
            //白色背景
            cellStyle.setFillBackgroundColor(IndexedColors.WHITE1.getIndex());
            // 合并单元格后的标题行，使用默认标题样式
            writer.write(dataList, true);
            response.setContentType("application/octet-stream;charset=utf-8");
            //Content-Disposition的作用：告知浏览器以何种方式显示响应返回的文件，用浏览器打开还是以附件的形式下载到本地保存
            //attachment表示以附件方式下载   inline表示在线打开   "Content-Disposition: inline; filename=文件名.mp3"
            //filename表示文件的默认名称，因为网络传输只支持URL编码的相关支付，因此需要将文件名URL编码后进行传输,前端收到后需要反编码才能获取到真正的名称
            response.setHeader("Content-Disposition","attachment;filename="+ URLEncoder.encode(fileName,"UTF-8"));
            out = response.getOutputStream();
            writer.flush(out, true);
        }  finally {
            //关闭writer，释放内存
            IoUtil.close(writer);
            //关闭输出Servlet流
            IoUtil.close(out);
        }
    }

    /**
     * 通过实体类集合创建excel表格文件导出到输出流
     * @param dataList 数据集合
     * @param fileName 文件名字
     * @param mapAlias 属性别名
     * @param response  输出流
     * @throws IOException
     */
    public static  void  entityExcel(List<Object> dataList, String fileName, Map<String,String> mapAlias, HttpServletResponse response) throws IOException {
        ExcelWriter writer = null;
        ServletOutputStream out = null;
        try {
            // 通过工具类创建writer，默认创建xls格式
            writer = ExcelUtil.getWriter();
            // 合并单元格后的标题行，使用默认标题样式
           // writer.merge(dataList.size(), title);
            //设置别名
            for (String s : mapAlias.keySet()) {
                writer.addHeaderAlias(s,mapAlias.get(s));
            }
            // 一次性写出内容，使用默认样式，强制输出标题
            writer.write(dataList, true);
            //response为HttpServletResponse对象
            response.setContentType("application/vnd.ms-excel;charset=utf-8");
            //Content-Disposition的作用：告知浏览器以何种方式显示响应返回的文件，用浏览器打开还是以附件的形式下载到本地保存
            //attachment表示以附件方式下载   inline表示在线打开   "Content-Disposition: inline; filename=文件名.mp3"
            //filename表示文件的默认名称，因为网络传输只支持URL编码的相关支付，因此需要将文件名URL编码后进行传输,前端收到后需要反编码才能获取到真正的名称
            response.setHeader("Content-Disposition","attachment;filename="+ URLEncoder.encode(fileName,"UTF-8"));
            out=response.getOutputStream();
            writer.flush(out, true);
        }  finally {
            //关闭writer，释放内存
            IoUtil.close(writer);
            //关闭输出Servlet流
            IoUtil.close(out);
        }
    }


}
