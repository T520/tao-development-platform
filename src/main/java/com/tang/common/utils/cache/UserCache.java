package com.tang.common.utils.cache;

import cn.hutool.extra.spring.SpringUtil;
import com.tang.common.constant.cahce.BusinessCacheConstant;
import com.tang.common.exception.GlobalException;
import com.tang.module.system.entity.Dept;
import com.tang.module.system.entity.Post;
import com.tang.module.system.entity.Role;
import com.tang.module.system.entity.User;
import com.tang.module.system.mapper.*;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * 用户,角色，部门，岗位缓存
 * @author tang
 * @date 2022/2/27 18:36
 */
public class UserCache {

    private static final RedisTemplate<String, Object> REDIS_TEMPLATE;

    private static final UserMapper USER_MAPPER;

    private static final RoleMapper ROLE_MAPPER;

    private static final PostMapper POST_MAPPER;

    private static final DeptMapper DEPT_MAPPER;

    static {
        REDIS_TEMPLATE = SpringUtil.getBean("redisTemplate");
        USER_MAPPER = SpringUtil.getBean("userMapper");
        ROLE_MAPPER = SpringUtil.getBean("roleMapper");
        POST_MAPPER = SpringUtil.getBean("postMapper");
        DEPT_MAPPER = SpringUtil.getBean("deptMapper");
    }

    /**
     * 获得用户名称缓存
     * @param id 用户id
     * @return
     */
    public static String getUserName(Long id){
        Object o = REDIS_TEMPLATE.opsForHash().get(BusinessCacheConstant.USER_PREFIX, String.valueOf(id));
        if (o == null){
            o = USER_MAPPER.selectById(id);
            if (o == null){
                throw new GlobalException("用户缓存查询不存在,id = "+id+"不存在");
            }
            REDIS_TEMPLATE.opsForHash().put(BusinessCacheConstant.DEPT_PREFIX, String.valueOf(id),((User) o).getName());
            return ((User) o).getName();
        }
        return String.valueOf(o);
    }


    /**
     * 获得角色名称缓存
     * @param id 角色id
     * @return
     */
    public static String getRoleName(Long id){
        Object o = REDIS_TEMPLATE.opsForHash().get(BusinessCacheConstant.ROLE_PREFIX, String.valueOf(id));
        if (o == null){
            o = ROLE_MAPPER.selectById(id);
            if (o == null){
                throw new GlobalException("角色缓存查询不存在,id = "+id+"不存在");
            }
            REDIS_TEMPLATE.opsForHash().put(BusinessCacheConstant.ROLE_PREFIX, String.valueOf(id),((Role) o).getRoleAlias());
            return ((Role) o).getRoleAlias();
        }
        return String.valueOf(o);
    }

    /**
     * 获得部门名称缓存
     * @param id 部门id
     * @return
     */
    public static String getDeptName(Long id){
        Object o = REDIS_TEMPLATE.opsForHash().get(BusinessCacheConstant.DEPT_PREFIX, String.valueOf(id));
        if (o == null){
            o = DEPT_MAPPER.selectById(id);
            if (o == null){
                throw new GlobalException("部门缓存查询不存在,id = "+id+"不存在");
            }
            REDIS_TEMPLATE.opsForHash().put(BusinessCacheConstant.DEPT_PREFIX, String.valueOf(id),((Dept) o).getDeptName());
            return  ((Dept) o).getDeptName();
        }
        return String.valueOf(o);
    }

    /**
     * 获得岗位名称缓存
     * @param id 岗位id
     * @return
     */
    public static String getPostName(Long id){
        Object o = REDIS_TEMPLATE.opsForHash().get(BusinessCacheConstant.POST_PREFIX, String.valueOf(id));
        if (o == null){
            o =POST_MAPPER.selectById(id);
            if (o == null){
                throw new GlobalException("岗位缓存查询不存在,id = "+id+"不存在");
            }
            REDIS_TEMPLATE.opsForHash().put(BusinessCacheConstant.POST_PREFIX, String.valueOf(id),((Post) o).getPostName());
            return  ((Post) o).getPostName();
        }
        return String.valueOf(o);
    }


}
