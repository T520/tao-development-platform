package com.tang.common.utils.cache;

import cn.hutool.extra.spring.SpringUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.tang.common.constant.cahce.BusinessCacheConstant;
import com.tang.common.exception.GlobalException;
import com.tang.module.system.entity.DictBiz;
import com.tang.module.system.mapper.DictBizMapper;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author tang
 * @date 2022/1/20 14:21
 */
public class DictBizCache {

    private static final RedisTemplate<String, Object> REDIS_TEMPLATE;

    private static  final DictBizMapper DICT_BIZ_MAPPER;


    static {
        REDIS_TEMPLATE = SpringUtil.getBean("redisTemplate");
        DICT_BIZ_MAPPER = SpringUtil.getBean("dictBizMapper");
    }

    /**
     * 获取字典实体
     * @param code 字典字典code
     * @param key  字典实际key
     * @return 实体
     */
    public static DictBiz getDict(String code, String key){
        Object o = REDIS_TEMPLATE.opsForHash().get(BusinessCacheConstant.BIZ_DICTIONARY_PREFIX + code, key);
        if (o == null){
            o = DICT_BIZ_MAPPER.selectOne(Wrappers.<DictBiz>lambdaQuery().eq(DictBiz::getCode,code).eq(DictBiz::getDictKey, key));
            if (o == null){
                throw new GlobalException("系统字典获取失败,code = " +code +",key = "+ key);
            }
        }
        return (DictBiz) o;
    }

    /**
     * 获取字典value
     * @param code 字典字典code
     * @param key  字典实际key
     * @return value
     */
    public static String getValue(String code, String key){
        return getDict(code, key).getDictValue();
    }

}
