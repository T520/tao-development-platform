package com.tang.common.utils.cache;

import cn.hutool.extra.spring.SpringUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.tang.common.constant.cahce.BusinessCacheConstant;
import com.tang.common.exception.GlobalException;
import com.tang.module.system.entity.Dict;
import com.tang.module.system.mapper.DictMapper;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author tang
 * @date 2022/1/20 14:11
 */
public class DictCache {

    private static final RedisTemplate<String, Object> REDIS_TEMPLATE;

    private static  final DictMapper DICT_MAPPER;


    static {
        REDIS_TEMPLATE = SpringUtil.getBean("redisTemplate");
        DICT_MAPPER = SpringUtil.getBean("dictMapper");
    }

    /**
     * 获取字典实体
     * @param code 字典字典code
     * @param key  字典实际key
     * @return 实体
     */
    public static Dict getDict(String code, Integer key){
        Object o = REDIS_TEMPLATE.opsForHash().get(BusinessCacheConstant.SYSTEM_DICTIONARY_PREFIX + code, key.toString());
        if (o == null){
            o = DICT_MAPPER.selectOne(Wrappers.<Dict>lambdaQuery().eq(Dict::getCode,code).eq(Dict::getDictKey,key));
            if (o == null){
                throw new GlobalException("系统字典获取失败,code = " +code +",key = "+ key);
            }
        }
        return (Dict) o;
    }

    /**
     * 获取字典value
     * @param code 字典字典code
     * @param key  字典实际key
     * @return value
     */
    public static String getValue(String code, Integer key){
        return getDict(code, key).getDictValue();
    }

}
