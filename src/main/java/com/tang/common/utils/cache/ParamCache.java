package com.tang.common.utils.cache;

import cn.hutool.extra.spring.SpringUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.tang.common.constant.cahce.BusinessCacheConstant;
import com.tang.common.exception.GlobalException;
import com.tang.module.system.entity.Param;
import com.tang.module.system.mapper.ParamMapper;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * 参数缓存
 * @author tang
 * @date 2021/10/26 15:27
 */
public class ParamCache {

    private  static final RedisTemplate<String, Object> REDIS_TEMPLATE;

    private  static final ParamMapper PARAM_MAPPER;

    static {
        REDIS_TEMPLATE = SpringUtil.getBean("redisTemplate");
        PARAM_MAPPER = SpringUtil.getBean("paramMapper");
    }

    /**
     * 获取参数实体
     * @param key 参数key
     * @return 实体
     */
    public static  Param getParam(String key){
        //从缓存中获取 
        Object param = REDIS_TEMPLATE.opsForValue().get(BusinessCacheConstant.PARAM_PREFIX_PARAM + key);
        if (param == null){
            //缓存中不存在就从数据库获取
            param = PARAM_MAPPER.selectOne(Wrappers.<Param>lambdaQuery().eq(Param::getParamKey, key));
            if (param == null){
                throw new GlobalException("系统参数"+key+"不存在");
            }
        }
        return (Param) param;
    }


    /**
     * 获取参数值
     * @param key 参数key
     * @return 参数值
     */
    public static String getValue(String key){
        return getParam(key).getParamValue();
    }

}
